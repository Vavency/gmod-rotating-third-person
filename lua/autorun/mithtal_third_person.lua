local script_init_time = SysTime()

if SERVER then

	AddCSLuaFile()
	AddCSLuaFile( "includes/mithral_third_person_config.lua" )
	AddCSLuaFile( "includes/mithtal_third_person_createmove.lua" )
	AddCSLuaFile( "includes/mithtal_third_person_debug.lua" )
	AddCSLuaFile( "includes/mithtal_third_person_renderer.lua" )
	AddCSLuaFile( "includes/mithtal_third_person_think.lua" )
	AddCSLuaFile( "includes/mithtal_third_person_revision.lua" )
	AddCSLuaFile( "includes/mithtal_third_person_editor.lua" )

end

if CLIENT then
	-- Places a global variable table used by the mod at large.
	include( "includes/mithral_third_person_config.lua" )

	local experimental = GetConVar( MithralThirdPerson_Config.var_name.EXPERIMENTAL ):GetBool() --  It shouldn't get enabled.

	local MithralThirdPerson_editor = include( "includes/mithtal_third_person_editor.lua" )
	local MithralThirdPerson_think = include( "includes/mithtal_third_person_think.lua" )
	local MithralThirdPerson_CreateMove = include( "includes/mithtal_third_person_createmove.lua" )
	local MithralThirdPerson_renderer = include( "includes/mithtal_third_person_renderer.lua" )
	local MithralThirdPerson_revision = include( "includes/mithtal_third_person_revision.lua" )


	local MithralThirdPerson_debug = include( "includes/mithtal_third_person_debug.lua" )



	local MithralThirdPersonHooks = {}
	ThirdPerson = {}
	ThirdPerson.Behaviour = {}

	local cameraAngles
	local cameraOrigin
	local cameraTargetPos
	local cameraAngles_viewpunched = Angle()
	local viewpunch_angle = Angle()
	local camera_drift = Angle()
	local input_angle_compensation = Angle()
	local cameraFOV = 100
	local playerAngles
	local framedelta = 0.1
	local fakePlayerAngle
	local current_camera_offset = 64
	local camera_pos_desired = Vector()
	local camera_current_pos = Vector()
	local changeSpeedVector = Vector()
	local move_vector = Vector()
	local trace_hull_size
	local camera_collision_hull = {}
	local ticks_per_second = math.ceil( 1 / engine.TickInterval() )

	local publicRequests = {
		isSet = {
			cameraOrigin = false,
			cameraAimingOrigin = false,
			cameraOriginModifier = false,
			cameraAngle = false,
			cameraAngleModifier = false,
			cameraFOV = false,
			cameraAimFOV = false,
		},
		cameraOrigin = Vector(),
		cameraAimingOrigin = Vector(),
		cameraOriginModifier = Vector(),
		cameraAngle = Angle(),
		cameraAngleModifier = Angle(),
		cameraFOV = cameraFOV,
		cameraAimFOV = 0,
	}

	-- Set in the editor UI and primary use is ease of use when setting up camera parameters.
	local Editor_Helpers = {}
	Editor_Helpers.ShouldHoldAim = false
	Editor_Helpers.SupressAimingKey = false
	Editor_Helpers.OpenInContextMenu = false
	Editor_Helpers.ContextMenuOpen = false

	local isAimingWasPressed = false
	local val_needToAim = false
	local val_isAddonActive = false
	local isFirstPersonAngleNeeded = false
	local isAimingKeyDown = false

	-- if the camera misses things then add more 'masks' or 'content' separating them with a comma.
	local traceMask = bit.bor( MASK_SHOT )
	-- This is set in 'UpdateCameraTrace()'
	local cameraMaskContent
	local cameraCollisionMask = MASK_VISIBLE

	local traceData = {}
	traceData.collisiongroup = COLLISION_GROUP_PROJECTILE
	traceData.maxs = Vector( 5, 5, 5 )
	traceData.mins = Vector( -5, -5, -5 )

	local cameraTraceResult = {}

	local CameraObstruction = {}
	--CameraObstruction.PlayerCompensatedAngle = false -- Unused
	CameraObstruction.UpdateCameraOrigin = false
	CameraObstruction.ContextMenuOpen = false
	CameraObstruction.SpawnMenuOpen = false


	-- Used for input comparisons, 'Player:KeyDownLast' is bugged in MP on sub 60 fps
	local OnLastTick = {}
	OnLastTick.in_mvRight = false
	OnLastTick.in_mvLeft = false
	OnLastTick.in_mvForward = false
	OnLastTick.in_mvBackward = false
	OnLastTick.needToAim = false
	OnLastTick.isAiming = false
	OnLastTick.isAimingHold = false
	OnLastTick.ply_ycord = 0
	OnLastTick.cameraTargetPos = Vector()
	OnLastTick.ca_viewpunched = Angle()
	OnLastTick.playerAngles = Angle()
	OnLastTick.UpdateCameraAngles_FrameNumber = 0
	OnLastTick.InputMouseApply = {
		x = 0,
		y = 0,
		camera_angle_add = Angle(),
		FrameNumber = 0
	}
	OnLastTick.UpdatePlayerAngles = {
		playerAngles = Angle(),
		cameraAngles = Angle(),
		framenumber = 0,
		NotCameraDependant = true,
		forceUpdate = false
	}
	OnLastTick.RotatePlayer = {
		current_angle = Angle()
	}
	OnLastTick.ViewPunchAngle = {
		last = Angle(),
		tick = 0,
	}
	OnLastTick.CreateMove = {
		keytap = 0,
		keyUnTap = 0,
		keytap_currentTick = 0,
		keyUnTap_CurrentTick = 0,
		impulse100 = false,
		flashlight_check = false,
	}

	-- Used to cache results
	local ResultCache = {}
	ResultCache.tick_IsPlayerInAimMode = 0
	ResultCache.IsPlayerInAimMode = false
	ResultCache.tick_IsAddonActive = 0
	ResultCache.IsAddonActive = false
	ResultCache.tick_PostProcess = 0
	ResultCache.tick_IsAiming = 0
	ResultCache.IsAiming = false

	local weaponVars = {
		current = nil,
		ang = Angle(),
		vec = Vector(),
		fov = 100,
	}

	local HoldTypeWhiteList = {
		["ar2"] = true,
		["smg"] = true,
		["rpg"] = true,
		["duel"] = true,
		["pistol"] = true,
		["shotgun"] = true,
		["crossbow"] = true,
		["revolver"] = true,
	}

	local weaponHoldType = {
		current_hold_type = "",
		gotten_hold_type = "",
		current_wep = NULL,
		last_weapon = NULL,
		HoldTypePassive = "passive",
	}

	-- Used for keeping the aim for a bit longer.
	local AimLock = {
		TimerFire = 0,
		TimerAim = 0,
		AnimLockFire = 0,
		AnimLockAim = 0,
		AnimLockReload = 0,
	}

	local TESTING_VALUES = {}



	function ThirdPerson.isAimingKeyDown()
		return isAimingKeyDown
	end

	function ThirdPerson.SetCameraOriginModifier( vec1 )
		publicRequests.isSet.cameraOriginModifier = true
		publicRequests.cameraOriginModifier:Set( vec1 or Vector() )
	end

	function ThirdPerson.SetCameraOrigin( vec1 )
		publicRequests.isSet.cameraOrigin = true
		publicRequests.cameraOrigin:Set( vec1 or Vector() )
	end

	function ThirdPerson.SetCameraAimingOrigin( vec1 )
		publicRequests.isSet.cameraAimingOrigin = true
		publicRequests.cameraAimingOrigin:Set( vec1 or Vector() )
	end

	function ThirdPerson.SetCameraFOV( fov )
		publicRequests.isSet.cameraFOV = true
		publicRequests.cameraFOV = fov or cameraFOV
	end

	function ThirdPerson.SetCameraAimingSubtractFOV( fov )
		publicRequests.isSet.cameraAimFOV = true
		publicRequests.cameraAimFOV = fov or 0
	end

	function ThirdPerson.GetCameraOrigin()
		return camera_pos_desired
	end

	function ThirdPerson.GetCameraAbsoluteOrigin()
		return cameraOrigin
	end

	function ThirdPerson.GetCameraFOV()
		return publicRequests.cameraFOV
	end

	function ThirdPerson.GetCameraAimingSubtractFOV()
		return publicRequests.cameraAimFOV
	end

	function ThirdPerson.GetCameraTrace()
		return cameraTraceResult
	end

	function ThirdPerson.GetAddonStatus()
		return val_isAddonActive
	end

	-- Editor will stop working without these
	MithralThirdPerson_editor.Behaviour.GetTable = MithralThirdPerson_think.GetBehaviourTable
	MithralThirdPerson_editor.Behaviour.GetTimeStamp = MithralThirdPerson_think.GetTimeStamp
	MithralThirdPerson_editor.Behaviour.AddRule = MithralThirdPerson_think.AddRule
	MithralThirdPerson_editor.Behaviour.RemoveRule = MithralThirdPerson_think.RemoveRule
	MithralThirdPerson_editor.Behaviour.ModifyRule = MithralThirdPerson_think.ModifyRule
	MithralThirdPerson_editor.Behaviour.SwapRules = MithralThirdPerson_think.SwapRules

	MithralThirdPerson_editor.Main.SetShouldHoldAim = function( bool_value ) Editor_Helpers.ShouldHoldAim = bool_value	end
	MithralThirdPerson_editor.Main.GetShouldHoldAim = function() return Editor_Helpers.ShouldHoldAim end

	MithralThirdPerson_editor.Main.SetSupressAimingKey = function( bool_value ) Editor_Helpers.SupressAimingKey = bool_value end
	MithralThirdPerson_editor.Main.GetSupressAimingKey = function() return Editor_Helpers.SupressAimingKey end

	MithralThirdPerson_editor.Main.SetOpenInContextMenu = function( bool_value ) Editor_Helpers.OpenInContextMenu = bool_value	end
	MithralThirdPerson_editor.Main.GetOpenInContextMenu = function() return Editor_Helpers.OpenInContextMenu end

	MithralThirdPerson_editor.Main.SetCameraObstructionContextMenuOpen = function( bool_value ) CameraObstruction.ContextMenuOpen = bool_value end
	MithralThirdPerson_editor.Main.GetCameraObstructionContextMenuOpen = function() return CameraObstruction.ContextMenuOpen end



	-- Console commands
	local function InvertCameraRightPos()
		GetConVar( MithralThirdPerson_Config.var_name.CAMERA_RIGHT ):SetInt( GetConVar( MithralThirdPerson_Config.var_name.CAMERA_RIGHT ):GetInt() * -1 )
	end

	local function InvertCameraAimRightPos()
		GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_RIGHT ):SetInt( GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_RIGHT ):GetInt() * -1 )
	end

	local function InvertCameraRightPosCombined()
		InvertCameraAimRightPos()
		InvertCameraRightPos()
	end

	local function AddonToggle()
		GetConVar( MithralThirdPerson_Config.var_name.ADDON_ENABLED ):SetBool( not GetConVar( MithralThirdPerson_Config.var_name.ADDON_ENABLED ):GetBool() )
	end

	local function SetCameraAngleCommand( ply, cmd, args )
		if GetConVar( "sv_cheats" ):GetBool() or game.SinglePlayer() then
			args[1] = args[1] or 0
			args[2] = args[2] or 0
			args[3] = args[3] or 0

			cameraAngles = Angle( args[1], args[2], args[3] )
		else
			MsgC( Color( 255,255,255 ), "Can't use cheat command " .. MithralThirdPerson_COM_SET_CAMETA_ANGLE .. " in multiplayer, unless the server has sv_cheats set to 1.\n" )
		end
	end



	local function CheckForNoclip()
		local Player_MoveType = LocalPlayer():GetMoveType()

		return Player_MoveType == MOVETYPE_NOCLIP or Player_MoveType == MOVETYPE_FLY
	end

	local function CheckForMovetype( movet )
		return LocalPlayer():GetMoveType() == movet
	end

	local function IsPlayerViewObstructed()
		return CameraObstruction.UpdateCameraOrigin and not CheckForNoclip()
	end

	local function IsFirstPersonForcedFallback()
		return CameraObstruction.ContextMenuOpen or MithralThirdPerson_think.Behaviour_GetForceFirstPerson()
	end

	local function IsFirstPersonNeeded()
		return IsFirstPersonForcedFallback() or IsPlayerViewObstructed()
	end

	local function IsFirstPersonNeededLastTic()
		return IsFirstPersonForcedFallback() or IsPlayerViewObstructed()
	end

	local function IsKeyDown( enum )
		return LocalPlayer():KeyDown( enum ) or MithralThirdPerson_CreateMove.GetDelayedKey( enum )
	end

	local function SetPlayerPassiveHoldType()
		weaponHoldType.current_weapon = LocalPlayer():GetActiveWeapon()

		if weaponHoldType.current_weapon == NULL or not weaponHoldType.current_weapon:IsValid() then
			return
		end

		weaponHoldType.gotten_hold_type = weaponHoldType.current_weapon:GetHoldType()

		--[[
		MithralThirdPerson_debug.draw_text( "SetPlayerPassiveHoldType:", 3 )
		MithralThirdPerson_debug.draw_text( "  current_weapon: " .. tostring( weaponHoldType.current_weapon ) )

		MithralThirdPerson_debug.draw_text( "  current_weapon:GetHoldType(): " .. tostring( weaponHoldType.current_weapon:GetHoldType() ) )
		MithralThirdPerson_debug.draw_text( "  current_weapon:GetActivity(): " .. tostring( weaponHoldType.current_weapon:GetActivity() ) )
		MithralThirdPerson_debug.draw_text( "  current_weapon:IsScripted(): " .. tostring( weaponHoldType.current_weapon:IsScripted() ) )
		MithralThirdPerson_debug.draw_text( "  weaponHoldType.current_weapon.HoldType: " .. tostring( weaponHoldType.current_weapon.HoldType ) )
		MithralThirdPerson_debug.draw_text( "  weaponHoldType.current_hold_type: " .. tostring( weaponHoldType.current_hold_type ) )
		MithralThirdPerson_debug.draw_text( "  AimLock.AnimLockFire < engine.TickCount(): " .. tostring(AimLock.AnimLockFire < engine.TickCount()) )
		MithralThirdPerson_debug.draw_text( "  AimLock.AnimLockAim < engine.TickCount(): " .. tostring(AimLock.AnimLockAim < engine.TickCount()) )
		MithralThirdPerson_debug.draw_text( "  AimLock.AnimLockReload < engine.TickCount(): " .. tostring(AimLock.AnimLockReload < engine.TickCount()) )
		MithralThirdPerson_debug.draw_text( "  HoldTypeWhiteList[weaponHoldType.gotten_hold_type]: " .. tostring(HoldTypeWhiteList[weaponHoldType.gotten_hold_type]) )
		MithralThirdPerson_debug.draw_text( "  weaponHoldType.HoldTypePassive[weaponHoldType.gotten_hold_type]: " .. tostring(weaponHoldType.HoldTypePassive[weaponHoldType.gotten_hold_type]) )
		--]]

		-- Some logic on how to handle current and last weapon
		if weaponHoldType.last_weapon ~= weaponHoldType.current_weapon then

			if not (weaponHoldType.last_weapon == NULL or not weaponHoldType.last_weapon:IsValid()) then
				-- Most of weapon bases don't bother setting hold type on equip
				weaponHoldType.last_weapon:SetHoldType( weaponHoldType.current_hold_type )
			end

			--print( weaponHoldType.current_weapon:GetHoldType(), tostring( weaponHoldType.current_weapon ) )
			weaponHoldType.current_hold_type = weaponHoldType.gotten_hold_type or weaponHoldType.current_weapon.HoldType

			if weaponHoldType.current_hold_type == "passive" or weaponHoldType.current_hold_type == "" then
				weaponHoldType.current_hold_type = weaponHoldType.current_weapon.HoldType or "ar2"
				weaponHoldType.current_weapon:SetHoldType( weaponHoldType.current_hold_type )
			end

			weaponHoldType.last_weapon = weaponHoldType.current_weapon
		end

		if not GetConVar( MithralThirdPerson_Config.var_name.WEAPON_PASSIVE_STANCE ):GetBool() then
			return
		end

		if 
			AimLock.AnimLockFire < engine.TickCount() 
			and AimLock.AnimLockAim < engine.TickCount() 
			and not IsKeyDown( IN_RELOAD ) 
			and not LocalPlayer():Crouching() 
			and ( HoldTypeWhiteList[weaponHoldType.gotten_hold_type] or weaponHoldType.HoldTypePassive == weaponHoldType.gotten_hold_type )
		then
			if weaponHoldType.HoldTypePassive ~= weaponHoldType.gotten_hold_type then
				weaponHoldType.current_hold_type = weaponHoldType.gotten_hold_type
			end
			weaponHoldType.current_weapon:SetHoldType( "passive" )
		else
			weaponHoldType.current_weapon:SetHoldType( weaponHoldType.current_hold_type )
		end

	end

	local function IsPlayerInAimMode()

		if ResultCache.tick_IsPlayerInAimMode == engine.TickCount() then
			return ResultCache.IsPlayerInAimMode
		end

		local isattacking = false

		if GetConVar( MithralThirdPerson_Config.var_name.AIM_ON_ATTACK ):GetBool() then
			isattacking = IsKeyDown( IN_ATTACK ) or IsKeyDown( IN_ALT1 ) or IsKeyDown( IN_ALT2 ) or IsKeyDown( IN_USE ) or IsKeyDown( IN_ATTACK2 ) or GetConVar( MithralThirdPerson_Config.var_name.ALWAYS_AIM ):GetBool()
		end

		if IsKeyDown( IN_RELOAD ) then
			AimLock.AnimLockReload = engine.TickCount() + ( ticks_per_second * 2 ) + 1
		end

		if isattacking then
			if IsKeyDown( IN_ATTACK ) or IsKeyDown( IN_ALT1 ) or IsKeyDown( IN_ALT2 ) or IsKeyDown( IN_ATTACK2 ) then
				AimLock.AnimLockFire = engine.TickCount() + ( ticks_per_second * 2 ) + 1
			end
			AimLock.TimerFire = engine.TickCount() + ( GetConVar( MithralThirdPerson_Config.var_name.AIMING_TIMER ):GetFloat() * ticks_per_second ) + 4
		end

		val_needToAim = isattacking or IsPlayerViewObstructed()

		ResultCache.IsPlayerInAimMode = val_needToAim
		ResultCache.tick_IsPlayerInAimMode = engine.TickCount()


		--MithralThirdPerson_debug.draw_text( "IsPlayerInAimMode: " .. tostring(val_needToAim), 2 )


		return val_needToAim
	end

	local function UpdateLastTicVariables()

		local ply = LocalPlayer()

		OnLastTick.in_mvRight = ply:KeyDown( IN_MOVERIGHT )
		OnLastTick.in_mvLeft = ply:KeyDown( IN_MOVELEFT )
		OnLastTick.in_mvForward = ply:KeyDown( IN_FORWARD )
		OnLastTick.in_mvBackward = ply:KeyDown( IN_BACK )
		--OnLastTick.needToAim = val_needToAim
		--[[
		if OnLastTick.dupeCall_InputMouseApply then
			OnLastTick.isAddonActive = val_isAddonActive
		end
		]]
		--MithralThirdPerson_debug.draw_text( "UpdateLastTicVariables:", 2)
		--MithralThirdPerson_debug.draw_text( "	OnLastTick.isAddonActive:" .. tostring(OnLastTick.isAddonActive) .. " val_isAddonActive:" .. tostring(val_isAddonActive))
	end

	local function IsPlayerMoves()
		local ply = LocalPlayer()
		return ply:KeyDown( IN_FORWARD ) or ply:KeyDown( IN_BACK ) or ply:KeyDown( IN_MOVERIGHT ) or ply:KeyDown( IN_MOVELEFT )

	end

	local function AimLockTimer()
		return AimLock.TimerFire > engine.TickCount() or AimLock.TimerAim > engine.TickCount()
	end

	local function IsAiming()


		if ResultCache.tick_IsAiming == engine.TickCount() then
			return ResultCache.IsAiming
		end
		ResultCache.tick_IsAiming = engine.TickCount()

		--[[
		MithralThirdPerson_debug.draw_text( "IsAiming: " .. tostring(ResultCache.IsAiming), 2 )
		MithralThirdPerson_debug.draw_text( "  isAimingKeyDown: " .. tostring(isAimingKeyDown) )
		MithralThirdPerson_debug.draw_text( "  Editor_Helpers.SupressAimingKey: " .. tostring(Editor_Helpers.SupressAimingKey) )
		MithralThirdPerson_debug.draw_text( "  Editor_Helpers.OpenInContextMenu: " .. tostring(Editor_Helpers.OpenInContextMenu) )
		MithralThirdPerson_debug.draw_text( "  Editor_Helpers.ContextMenuOpen: " .. tostring(Editor_Helpers.ContextMenuOpen) )
		MithralThirdPerson_debug.draw_text( "  CameraObstruction.ContextMenuOpen: " .. tostring(CameraObstruction.ContextMenuOpen) )
		MithralThirdPerson_debug.draw_text( "  CameraObstruction.SpawnMenuOpen" .. tostring(CameraObstruction.SpawnMenuOpen) )
		--]]

		if Editor_Helpers.ShouldHoldAim or CameraObstruction.SpawnMenuOpen then
			LocalPlayer():SetEyeAngles( cameraAngles )
			ResultCache.tick_IsAiming = true
			return true
		end

		if Editor_Helpers.SupressAimingKey and Editor_Helpers.OpenInContextMenu and CameraObstruction.ContextMenuOpen then
			ResultCache.IsAiming = false
			return false
		end

		local isButtonDown = input.IsButtonDown( GetConVar( MithralThirdPerson_Config.var_name.PLAYER_AIMING_BUTTON ):GetInt() )
		local aimingToggle = GetConVar( MithralThirdPerson_Config.var_name.AIMING_BUTTON_TOGGLE ):GetBool()
		isAimingKeyDown = isButtonDown or Editor_Helpers.ShouldHoldAim or OnLastTick.isAimingHold

		if aimingToggle then
			if isButtonDown and not OnLastTick.isAiming then
				OnLastTick.isAiming = true
				OnLastTick.isAimingHold = not OnLastTick.isAimingHold
			elseif not isButtonDown and OnLastTick.isAiming then
				OnLastTick.isAiming = false
			end
		end

		if isAimingKeyDown then
			AimLock.AnimLockAim = engine.TickCount() + ticks_per_second + 1
			AimLock.TimerAim = engine.TickCount() + ticks_per_second + 1
		end

		if CameraObstruction.ContextMenuOpen or ( Editor_Helpers.SupressAimingKey and Editor_Helpers.ContextMenuOpen and Editor_Helpers.OpenInContextMenu ) then
			ResultCache.IsAiming = false
			return false
		end

		ResultCache.IsAiming = isAimingKeyDown
		return isAimingKeyDown
	end

	local function NeedToAim()
		return IsAiming() or IsPlayerInAimMode() or AimLockTimer() or IsPlayerViewObstructed()
	end

	local function PlayerEyeTraceStart()
		return LocalPlayer():GetEyeTrace().StartPos
	end

	local function PlayerEyeTraceEnd()
		return LocalPlayer():GetEyeTrace().HitPos
	end

	local function UpdateCameraTrace()

		if MithralThirdPerson_think.Behaviour_GetZeroOutCameraTraceMask() then
			cameraMaskContent = CONTENTS_EMPTY
		else
			-- I don't even know why... CONTENTS_SOLID is only thing that CONTENTS mask and trace relsult CONTENTS agree on... The rest is madness. It supposed to be 'CONTENTS_SOLID + CONTENTS_TRANSLUCENT' acording to CONTENTS bitflags I get from trace result but for some god forsaken reason I need to use 'CONTENTS_SOLID + CONTENTS_MONSTER' otherwise trace won't collide.
			cameraMaskContent = traceMask
		end


		traceData.start = cameraOrigin + cameraAngles:Forward() * ( camera_current_pos.x * camera_collision_hull.Fraction )
		traceData.endpos = cameraOrigin + cameraAngles:Forward() * 32768
		traceData.filter = LocalPlayer() --filter out the player.
		traceData.mask = cameraMaskContent
		--traceData.ignoreworld = CheckForNoclip()

		cameraTraceResult = util.TraceLine( traceData )


		cameraTraceResult.cameraAngles = Angle( cameraAngles_viewpunched )

		--[[
		MithralThirdPerson_debug.draw_text(  "UpdateCameraTrace():", 2 )
		MithralThirdPerson_debug.draw_text(  "	cameraTraceResult.StartPos: " .. table.ToString( cameraTraceResult.StartPos:ToTable() ) )
		MithralThirdPerson_debug.draw_text(  "	cameraTraceResult.HitPos: " .. table.ToString( cameraTraceResult.HitPos:ToTable() ))
		MithralThirdPerson_debug.enum_to_name( cameraTraceResult.Contents, MithralThirdPerson_debug.GetContentStringTable(), {append_string="Camera Trace"} )
		MithralThirdPerson_debug.enum_to_name( LocalPlayer():GetEyeTrace().Contents, MithralThirdPerson_debug.GetContentStringTable(), {append_string=" Player trace contents"} )
		MithralThirdPerson_debug.enum_to_name( cameraMaskContent, MithralThirdPerson_debug.GetContentStringTable(), {append_string="mask"} )
		--debugoverlay.Cross( traceData.start, 5, FrameTime(), Color( 255, 0, 0 ) )
		--debugoverlay.Cross( cameraTraceResult.HitPos, 10, FrameTime(), Color( 0, 255, 0 ) )
		debugoverlay.Box( cameraTraceResult.HitPos, traceData.mins, traceData.maxs, 0, Color(0,0,255,64) )
		debugoverlay.Box( cameraTraceResult.StartPos, traceData.mins, traceData.maxs, 0, Color(255,0,0,64) )
		MithralThirdPerson_debug.draw_line( cameraTraceResult.StartPos, cameraTraceResult.HitPos, 1, 1 )
		MithralThirdPerson_debug.draw_line( LocalPlayer():GetEyeTrace().StartPos, LocalPlayer():GetEyeTrace().HitPos, 2, framedelta )
		--MithralThirdPerson_debug.draw_line( TESTING_VALUES.ScreenToVector, LocalPlayer():GetEyeTrace().StartPos, 3, framedelta )
		--]]
	end

	local function PlayerCompensatedAngle()

		if GetConVar( MithralThirdPerson_Config.var_name.ACCURATE_AIM ):GetBool() then
			return cameraAngles
		end

		--[[
		--TODO: Messure performance impact of both methods (subtraction should be faster)
		--Hacky, appoximate method gives surprisingly good results, no idea if it's faster
		work_val = PlayerEyeTraceStart() - cameraOrigin
		work_val = PlayerEyeTraceEnd() - work_val
		--]]

		return ( cameraTraceResult.HitPos - PlayerEyeTraceStart() ):Angle()
	end

	local function keyTurns()
		if IsKeyDown( IN_RIGHT ) then
			return 40 * ( framedelta * 60 )
		elseif IsKeyDown( IN_LEFT ) then
			return -40 * ( framedelta * 60 )
		end
		return 0
	end

	local function CalcDeltaYawByX( x )

		-- Useful for debugging
		x = x + keyTurns()

		local xDirection
		if x < 0 then
			xDirection = 1
		else
			xDirection = -1
		end

		local deltaYaw = 0
		if x ~= 0 then
			deltaYaw = xDirection * ( math.abs( x ) / ScrW() * cameraFOV )
		end

		return deltaYaw
	end

	local function CalcDeltaPitchByY( y )
		local yDirection
		if y < 0 then
			yDirection = -1
		else
			yDirection = 1
		end

		local deltaPitch = 0
		if y ~= 0 then
			deltaPitch = yDirection * ( math.abs( y ) / ScrH() * cameraFOV )
		end

		return deltaPitch
	end

	local function UpdateCameraAngles( cmd, x, y )

		if MithralThirdPerson_think.Behaviour_GetLockCamera() or cameraAngles == nil then
			return
		end

		-- InputMouseApply hook runs on frame updates plus a engine tick updates, the engine tick updates screw up weapon punch.
		if OnLastTick.UpdateCameraAngles_FrameNumber == FrameNumber() then
			return
		else

			OnLastTick.UpdateCameraAngles_FrameNumber = FrameNumber()
			if game.SinglePlayer() then
				viewpunch_angle = LocalPlayer():GetViewPunchAngles()
			else
				-- Viewpunch needs lerping to look nice in MP, quite awful to look at low tickrates otherwise.
				if OnLastTick.ViewPunchAngle.tick ~= engine.TickCount() then
					--MithralThirdPerson_debug.draw_text( "Angle Update!", 4 )
					OnLastTick.ViewPunchAngle.last = LocalPlayer():GetViewPunchAngles()
					OnLastTick.ViewPunchAngle.tick = engine.TickCount()
				end

				local t = framedelta * 1 / engine.TickInterval()
				if t > 1 then t = 1.1 end
				viewpunch_angle.p = viewpunch_angle.p + t * ( OnLastTick.ViewPunchAngle.last.p - viewpunch_angle.p )
				viewpunch_angle.y = viewpunch_angle.y + t * ( OnLastTick.ViewPunchAngle.last.y - viewpunch_angle.y )
				viewpunch_angle.r = viewpunch_angle.r + t * ( OnLastTick.ViewPunchAngle.last.r - viewpunch_angle.r )
			end


		end
		if experimental and GetConVar( MithralThirdPerson_Config.var_name.CAPTURE_EXTERNAL_INPUT_MODIFIERS ):GetBool() then
			if not IsFirstPersonNeeded() and NeedToAim() and not CheckForNoclip() then
			--if not IsFirstPersonNeeded() and not CheckForNoclip() then
				camera_drift:Set( OnLastTick.InputMouseApply.camera_angle_add )
				camera_drift:Sub( OnLastTick.RotatePlayer.current_angle )
				camera_drift:Normalize()
				if MithralThirdPerson_debug.IsDebugEnabled() then
					if camera_drift.x ~= 0 or camera_drift.y ~= 0 or camera_drift.z ~= 0 then
						print(  "	" .. table.ToString( cameraAngles:ToTable(), "cameraAngles", false ) )
						print(  "	" .. table.ToString( playerAngles:ToTable(), "playerAngles", false ) )
						print(  "	" .. table.ToString( camera_drift:ToTable(), "camera_drift", false ) )
						print(  "	" .. table.ToString( cameraAngles_viewpunched:ToTable(), "cameraAngles_viewpunched", false ) )
						--MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( input_angle_compensation_inter:ToTable(), "input_angle_compensation_inter", true ) )
						print(  "	" .. table.ToString( OnLastTick.RotatePlayer.current_angle:ToTable(), "OnLastTick.RotatePlayer.current_angle", false ) )
						print(  "	" .. table.ToString( OnLastTick.InputMouseApply.camera_angle_add:ToTable(), "OnLastTick.InputMouseApply.camera_angle_add", false ) )
						print(  "	" .. table.ToString( OnLastTick.playerAngles:ToTable(), "OnLastTick.playerAngles", false ) )
					end
					if camera_drift.x ~= 0 then
						print( "camera_drift.x: " ..  camera_drift.x )
					end
					if camera_drift.y ~= 0 then
						print( "camera_drift.y: " ..  camera_drift.y )
					end
					if camera_drift.z ~= 0 then
						print( "camera_drift.z: " ..  camera_drift.z )
					end
				end
			else
				camera_drift:Zero()
			end
			--[
				MithralThirdPerson_debug.draw_text(  "UpdateCameraAngles( " .. x .. " , " .. y .. ")", 2 )
				--MithralThirdPerson_debug.draw_text(  "	CalcDeltaYawByX( x ): " .. CalcDeltaYawByX( x ) )
				--MithralThirdPerson_debug.draw_text(  " CalcDeltaPitchByY( y ): " .. CalcDeltaPitchByY( y ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( cameraAngles:ToTable(), "cameraAngles", true ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( playerAngles:ToTable(), "playerAngles", true ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( camera_drift:ToTable(), "camera_drift", true ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( cameraAngles_viewpunched:ToTable(), "cameraAngles_viewpunched", true ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( input_angle_compensation:ToTable(), "input_angle_compensation", true ) )
				--MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( input_angle_compensation_inter:ToTable(), "input_angle_compensation_inter", true ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( OnLastTick.InputMouseApply.camera_angle_add:ToTable(), "OnLastTick.InputMouseApply.camera_angle_add", true ) )
				MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( OnLastTick.playerAngles:ToTable(), "OnLastTick.playerAngles", true ) )
			--]]
		else
			camera_drift:Zero()
		end

		cameraAngles.y = cameraAngles.y + CalcDeltaYawByX( x )
		cameraAngles.p = cameraAngles.p + CalcDeltaPitchByY( y )
		cameraAngles:Add( camera_drift )
		cameraAngles.p = math.min(cameraAngles.p, 89)
		cameraAngles.p = math.max(cameraAngles.p, -89)

		cameraAngles:Normalize()

		--cameraAngles_viewpunched = viewpunch_angle + cameraAngles
		--cameraAngles_viewpunched:Normalize()



		-- Force set the player view to the camera angle to avoid as much confusion as possible.
		if IsFirstPersonNeeded() then
			cmd:SetViewAngles( cameraAngles )
		end

		--[[
		--if cmd:TickCount() != 0 then
			MithralThirdPerson_debug.draw_text(  "UpdateCameraAngles( " .. x .. " , " .. y .. ")", 2 )
			--MithralThirdPerson_debug.draw_text(  "	CalcDeltaYawByX( x ): " .. CalcDeltaYawByX( x ) )
			--MithralThirdPerson_debug.draw_text(  " CalcDeltaPitchByY( y ): " .. CalcDeltaPitchByY( y ) )
			MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( cameraAngles:ToTable(), "cameraAngles", true ) )
			MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( cameraAngles_viewpunched:ToTable(), "cameraAngles_viewpunched", true ) )
		--end
		--]]
	end

	local function Xor( a, b )

		if ( a or b ) and ( not a or not b ) then
			return true
		else
			return false
		end

	end

	local function CameraDependantMovement()
		return LocalPlayer():WaterLevel() > 1 or CheckForMovetype( MOVETYPE_LADDER ) or CheckForNoclip()
	end

	local function IsDisableRotationWhenMove()
		return GetConVar( MithralThirdPerson_Config.var_name.CAMERA_DISABLE_ROTATION_WHEN_MOVE ):GetBool() and not CameraDependantMovement()
	end

	local function UpdatePlayerAngles( x )


		local ply = LocalPlayer()

		if OnLastTick.UpdatePlayerAngles.framenumber ~= FrameNumber() then
			if IsPlayerMoves() and ( IsDisableRotationWhenMove() and not CameraDependantMovement() ) then
					playerAngles.y = playerAngles.y + CalcDeltaYawByX( x )
			end
		end
		OnLastTick.UpdatePlayerAngles.framenumber = FrameNumber()

		if CameraDependantMovement() then
			playerAngles = Angle( cameraAngles )
			isAimingWasPressed = true
		end

		if NeedToAim() then

			playerAngles = PlayerCompensatedAngle()
			isAimingWasPressed = true

		elseif	( isAimingWasPressed
				or ( Xor( ply:KeyDown( IN_FORWARD ), OnLastTick.in_mvForward ) )
				or ( Xor( ply:KeyDown( IN_BACK ), OnLastTick.in_mvBackward ) )
				or ( Xor( ply:KeyDown( IN_MOVERIGHT ), OnLastTick.in_mvRight ) )
				or ( Xor( ply:KeyDown( IN_MOVELEFT ), OnLastTick.in_mvLeft ) )
				)
				and not CheckForMovetype( MOVETYPE_LADDER )
		then

			isAimingWasPressed = false
			OnLastTick.UpdatePlayerAngles.forceUpdate = false

			if ply:KeyDown( IN_FORWARD ) then

				playerAngles = cameraAngles:Forward():Angle()
				if ply:KeyDown( IN_MOVERIGHT ) then
					playerAngles.y = playerAngles.y - 45
				elseif ply:KeyDown( IN_MOVELEFT ) then
					playerAngles.y = playerAngles.y + 45
				end

			elseif ply:KeyDown( IN_BACK ) then

				playerAngles = ( cameraAngles:Forward() * -1 ):Angle()
				if ply:KeyDown( IN_MOVERIGHT ) then
					playerAngles.y = playerAngles.y + 45
				elseif ply:KeyDown( IN_MOVELEFT ) then
					playerAngles.y = playerAngles.y - 45
				end

			elseif ply:KeyDown( IN_MOVERIGHT ) then
				playerAngles = cameraAngles:Right():Angle()
			elseif ply:KeyDown( IN_MOVELEFT ) then
				playerAngles = ( cameraAngles:Right() * -1 ):Angle()
			end

		end

		--OnLastTick.UpdatePlayerAngles.playerAngles = playerAngles

		--playerAngles:Add(viewpunch_angle)
		--[[
		MithralThirdPerson_debug.draw_text( "UpdatePlayerAngles:", 2 )
		--MithralThirdPerson_debug.draw_text(  "	View Punch p: " .. viewpunch_angle.p .. " y: " ..  viewpunch_angle.y ..  " r: " .. viewpunch_angle.roll )
		--MithralThirdPerson_debug.draw_text(  "	framedelta: " .. framedelta )
		--MithralThirdPerson_debug.draw_text( " IN_FORWARD: " .. tostring( ply:KeyDown( IN_FORWARD ) ) .. " IN_BACK: " .. tostring( ply:KeyDown( IN_BACK ) ) .. " IN_MOVERIGHT: " .. tostring( ply:KeyDown( IN_MOVERIGHT ) ) .. " IN_MOVELEFT: " .. tostring( ply:KeyDown( IN_MOVELEFT ) ))
		MithralThirdPerson_debug.draw_text( " IN_FORWARD: " .. tostring( ply:KeyDown( IN_FORWARD ) ) )
		MithralThirdPerson_debug.draw_text( " IN_BACK: " .. tostring( ply:KeyDown( IN_BACK ) ) )
		MithralThirdPerson_debug.draw_text( " IN_MOVERIGHT: " .. tostring( ply:KeyDown( IN_MOVERIGHT ) ) )
		MithralThirdPerson_debug.draw_text( " IN_MOVELEFT: " .. tostring( ply:KeyDown( IN_MOVELEFT ) ) )
		MithralThirdPerson_debug.draw_text( " playerAngles.p: " .. tostring( playerAngles.p ) )
		MithralThirdPerson_debug.draw_text( " playerAngles.y: " .. tostring( playerAngles.y ) )
		MithralThirdPerson_debug.draw_text( " isAimingWasPressed: " .. tostring( isAimingWasPressed ) )
		MithralThirdPerson_debug.draw_text( " IsPlayerMoves(): " .. tostring( IsPlayerMoves() ) )
		MithralThirdPerson_debug.draw_text( " IsDisableRotationWhenMove(): " .. tostring( IsDisableRotationWhenMove() ) )
		MithralThirdPerson_debug.draw_text( " MOVETYPE_LADDER: " .. tostring( CheckForMovetype( MOVETYPE_LADDER ) ) )
		MithralThirdPerson_debug.draw_text( " CameraDependantMovement(): " .. tostring( CameraDependantMovement() ) )
		--MithralThirdPerson_debug.draw_text( " CheckForNoclip(): " .. tostring( CheckForNoclip() ) )
		--MithralThirdPerson_debug.draw_text( " IsPlayerMoves() and ( IsDisableRotationWhenMove() and not CheckForNoclip() ): " .. tostring( IsPlayerMoves() and ( IsDisableRotationWhenMove() and not CheckForNoclip() ) ) )
		MithralThirdPerson_debug.draw_line( ply:GetEyeTrace().StartPos, ply:GetEyeTrace().HitPos, 1, FrameTime()*2 )
		--]]

	end

	local function UpdatePlayerMove( cmd )

		if
			not IsAiming()
			and not IsPlayerInAimMode()
			and not AimLockTimer()
			and IsPlayerMoves()
			and not IsDisableRotationWhenMove()
			or (
				CameraDependantMovement()
				and IsPlayerMoves()
				and not IsAiming()
				and not AimLockTimer()
				and not IsFirstPersonNeeded()
				)
		then

			cmd:SetForwardMove( LocalPlayer():GetMaxSpeed() )
			--cmd:SetForwardMove( 1073741824 )
			cmd:SetSideMove( 0 )
			return
		end

	end

	local function RotatePlayer( cmd, ang )

		if 	not NeedToAim() and not CameraDependantMovement() then

			local rotationSpeed = GetConVar( MithralThirdPerson_Config.var_name.PLAYER_ROTATION_SPEED ):GetFloat() * ( framedelta * 60 ) -- 60, The magic number at 60fps

			if playerAngles == nil then
				return
			end

			ang.y = math.ApproachAngle(ang.y, playerAngles.y, rotationSpeed)
			ang.p = 0 --math.ApproachAngle(ang.p, 0, rotationSpeed)
			ang.r = 0 --math.ApproachAngle(ang.r, 0, rotationSpeed)

			ang:Normalize()

		elseif not NeedToAim() and not CameraDependantMovement() then
			ang = cameraAngles
		else
			ang = playerAngles
		end

		OnLastTick.RotatePlayer.current_angle = ang
		cmd:SetViewAngles( ang )


		--[[
		MithralThirdPerson_debug.draw_text( "RotatePlayer()", 2 )
		MithralThirdPerson_debug.draw_text( " MithralThirdPerson_Config.var_name.PLAYER_ROTATION_SPEED: " .. GetConVar( MithralThirdPerson_Config.var_name.PLAYER_ROTATION_SPEED ):GetInt() )
		MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( playerAngles:ToTable(), "playerAngles", true ) )
		MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( ang:ToTable(), "ang", true ) )
		MithralThirdPerson_debug.draw_text(  "	" .. table.ToString( OnLastTick.RotatePlayer.current_angle:ToTable(), "current_angle", true ) )
		--]]


	end

	local function IsPlayerFalling()
		return OnLastTick.ply_ycord > math.Truncate( LocalPlayer():GetPos().z )
	end

	local function UpdateCameraOrigin( ply, origin )

		--MithralThirdPerson_debug.bench_measure( "UpdateCameraOrigin" )

		local changeSpeed = GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_POSITION_CHANGE_SPEED ):GetFloat() * ( framedelta * 10 )


		--if GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_POSITION_CHANGE_SPEED ):GetFloat() < 1  then -- Sanity check
		--	changeSpeed = 1 * ( framedelta * 10 )
		--end

		--if not ( CheckForMovetype( MOVETYPE_FLY ) or CheckForMovetype( MOVETYPE_FLYGRAVITY ) or CheckForMovetype( MOVETYPE_NOCLIP ) ) and IsPlayerFalling() then
		--	publicRequests.cameraOriginModifier.z = publicRequests.cameraOriginModifier.z + ( GetConVar( MithralThirdPerson_Config.var_name.CAMERA_FALLING_OFFSET ):GetFloat() * math.abs( ply:GetVelocity().z / 500 ) )
		--end

		--publicRequests.cameraOriginModifier:Rotate( Angle( cameraAngles.p, 0, 0 ) )

		--OnLastTick.ply_ycord = math.Truncate( ply:GetPos().z )


		if	IsAiming() and not IsFirstPersonForcedFallback() then

			camera_pos_desired:SetUnpacked(	GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_FORWARD ):GetInt(),
											GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_RIGHT ):GetInt(),
											GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_UP ):GetInt())
			camera_pos_desired:Add( publicRequests.cameraOriginModifier )

		elseif IsFirstPersonForcedFallback() then

			camera_pos_desired:Zero()

		else
			camera_pos_desired:SetUnpacked(	GetConVar( MithralThirdPerson_Config.var_name.CAMERA_FORWARD ):GetInt(),
											GetConVar( MithralThirdPerson_Config.var_name.CAMERA_RIGHT ):GetInt(),
											GetConVar( MithralThirdPerson_Config.var_name.CAMERA_UP ):GetInt())
			camera_pos_desired:Add( publicRequests.cameraOriginModifier )
		end


		-- Incase the frame rate drops to single digits or even seconds per frame, just jump to desired pos instead of lerping. Else it's madness.
		if changeSpeed < 0.9 then
			camera_current_pos:Set( LerpVector( changeSpeed, camera_current_pos, camera_pos_desired ))
		else
			camera_current_pos:Set( camera_pos_desired )
		end


		if GetConVar( MithralThirdPerson_Config.var_name.CAMERA_ORIGIN_ON_HEAD ):GetBool() and not IsFirstPersonForcedFallback() then
			OnLastTick.cameraTargetPos = Vector(cameraTargetPos)
			cameraTargetPos = ply:GetBonePosition( ply:LookupBone( "ValveBiped.Bip01_Head1" ) )
			if cameraTargetPos == ply:GetPos() then
				cameraTargetPos = ply:GetBoneMatrix( ply:LookupBone( "ValveBiped.Bip01_Head1" ) ):GetTranslation()
			end
		else
			cameraTargetPos = ply:GetPos()
			if ply:Crouching() then
				current_camera_offset = math.Approach( current_camera_offset, ply:GetViewOffsetDucked().z + 16, ( current_camera_offset - 40 ) * changeSpeed )
			else
				current_camera_offset = math.Approach( current_camera_offset, ply:GetViewOffset().z, ( current_camera_offset - 64 ) * changeSpeed )
			end
			cameraTargetPos.z = cameraTargetPos.z + current_camera_offset

		end



		traceData.start = origin

		traceData.endpos = cameraTargetPos + cameraAngles:Forward() * -camera_current_pos.x
		traceData.endpos = traceData.endpos + cameraAngles:Right() * camera_current_pos.y
		traceData.endpos = traceData.endpos + cameraAngles:Up() * camera_current_pos.z
		traceData.mask = cameraCollisionMask
		traceData.ignoreworld = CheckForNoclip()

		-- No noticable increase in cpu times, let's see if anyone complains...
		--traceData.filter = function(ent) return ent:GetOwner() == ply or ent ~= ply end
		traceData.filter = ply

		--[[
		if GetConVar( MithralThirdPerson_Config.var_name.SIMPLER_CAM_TRACE_FILTER ):GetBool() then
			traceData.filter = ply
		else
			traceData.filter = function(ent) return ent:GetOwner() ~= ply or ent == ply end
		end
		--]]

		-- 
		trace_hull_size = 5.1 * ( cameraFOV / 100 )

		traceData.maxs:SetUnpacked( trace_hull_size, trace_hull_size, trace_hull_size )
		trace_hull_size = trace_hull_size * -1
		traceData.mins:SetUnpacked( trace_hull_size, trace_hull_size, trace_hull_size )

		camera_collision_hull = util.TraceHull( traceData )
		cameraOrigin = camera_collision_hull.HitPos

		if camera_collision_hull.HitPos:DistToSqr( cameraTargetPos ) < GetConVar( MithralThirdPerson_Config.var_name.FIRSTPERSON_FRACTION ):GetFloat() then
			cameraOrigin = origin
			CameraObstruction.UpdateCameraOrigin = true
		else
			CameraObstruction.UpdateCameraOrigin = false
		end

		publicRequests.cameraOriginModifier:Zero()

		--MithralThirdPerson_debug.bench_measure( "UpdateCameraOrigin" )
		--[[
		debugoverlay.Box( origin, traceData.mins, traceData.maxs, 0, Color(0,255,255,64) )
		debugoverlay.Box( cameraTargetPos, traceData.mins, traceData.maxs, 0, Color(0,255,0,64) )
		debugoverlay.Box( camera_collision_hull.HitPos, traceData.mins, traceData.maxs, 0, Color(0,0,255,64) )
		debugoverlay.Box( camera_collision_hull.StartPos, traceData.mins, traceData.maxs, 0, Color(255,0,0,64) )
		--]]

		--[[
		if camera_collision_hull.Entity ~= NULL and camera_collision_hull.Entity ~= Entity(0) then
			local colbox1, colbox2 = camera_collision_hull.Entity:GetCollisionBounds()
			debugoverlay.Box( camera_collision_hull.Entity:GetPos(), colbox1, colbox2, 20, Color(255,255,0,32) )
			print( camera_collision_hull.Entity, camera_collision_hull.Entity:GetOwner(), camera_collision_hull.Entity:GetCollisionGroup(), camera_collision_hull.Entity:GetMoveCollide(), colbox1, colbox2 )
			print( camera_collision_hull.Entity:GetOwner() == ply )
		end
		--]]

		--[[
		--local debug_helper = LocalPlayer():GetVelocity()--:Rotate( Angle( 1, 1, 1 ) )
		MithralThirdPerson_debug.draw_text( "UpdateCameraOrigin:", 2 )
		MithralThirdPerson_debug.enum_to_name( camera_collision_hull.Contents, MithralThirdPerson_debug.GetContentStringTable() )
		--MithralThirdPerson_debug.enum_to_name( cameraCollisionMask, MithralThirdPerson_debug.GetContentStringTable(), {append_string="cameraCollisionMask"} )
		--MithralThirdPerson_debug.draw_text( "	IsAiming: " .. tostring( IsAiming() ) .. " FCS: " .. changeSpeedVector.x .. " RCS: " .. changeSpeedVector.y .. " UCS: " .. changeSpeedVector.z )
		MithralThirdPerson_debug.draw_text( "	CCP.x: " .. camera_current_pos.x .. " CCP.y: " .. camera_current_pos.y .. " CCP.z: " .. camera_current_pos.z )
		MithralThirdPerson_debug.draw_text( "	CPD.x: " .. camera_pos_desired.x .. " CPD.y: " .. camera_pos_desired.y .. " CPD.z: " .. camera_pos_desired.z )
		--MithralThirdPerson_debug.draw_text( "	TCS.x: " .. ( OnLastTick.cameraTargetPos.x - cameraTargetPos.x ) * ( framedelta * 60 ) .. " TCS.y: " .. ( OnLastTick.cameraTargetPos.y - cameraTargetPos.y ) * ( framedelta * 60 ) .. " TCS.z: " .. ( OnLastTick.cameraTargetPos.z - cameraTargetPos.z ) * ( framedelta * 60 ) )
		--MithralThirdPerson_debug.draw_text( "	Camera_To_Player_Dist: " .. Camera_To_Player_Dist )
		MithralThirdPerson_debug.draw_text( "	camera_collision_hull.Entity: " .. tostring( camera_collision_hull.Entity ) )
		MithralThirdPerson_debug.draw_text( "	CS: " .. changeSpeed)
		MithralThirdPerson_debug.draw_text( "	PlayerPos: x: " .. math.Truncate( LocalPlayer():GetPos().x, 5 ) .. " y: " .. math.Truncate( LocalPlayer():GetPos().y, 5 ) .. " z: " .. math.Truncate( LocalPlayer():GetPos().z, 5 ) )
		MithralThirdPerson_debug.draw_text( "	publicRequests.cameraOriginModifier: " .. table.ToString( publicRequests.cameraOriginModifier:ToTable() ) )
		--MithralThirdPerson_debug.draw_text( "	origin: " .. table.ToString( origin:ToTable() ) )
		--MithralThirdPerson_debug.draw_text( "	actual rel pos: " .. table.ToString( ( origin - camera_collision_hull.HitPos ):ToTable() ) )
		--MithralThirdPerson_debug.draw_text( "	distance_xyz: " .. table.ToString( distance_xyz:ToTable() ) )
		MithralThirdPerson_debug.draw_text( "	Eye To Pos: " .. LocalPlayer():GetEyeTrace().StartPos.z - LocalPlayer():GetPos().z )
		MithralThirdPerson_debug.draw_text( "	trace_hull_size: " .. -trace_hull_size )
		--MithralThirdPerson_debug.draw_text( "	Player Velocity: " .. table.ToString( debug_helper:ToTable() ) )
		--]]

	end

	local function UpdateCameraFOV()
		local cam_fov = GetConVar( MithralThirdPerson_Config.var_name.CAMERA_FOV ):GetInt()
		local cam_aim_fov = GetConVar( MithralThirdPerson_Config.var_name.CAMERA_AIM_FOV ):GetInt()
		local ply_firstPersonFOV = LocalPlayer():GetFOV()

		if IsAiming() then

			if publicRequests.isSet.cameraAimFOV then
				cam_aim_fov = publicRequests.cameraAimFOV
				publicRequests.isSet.cameraAimFOV = false
			end

			cam_fov = cam_fov - cam_aim_fov
		end

		if publicRequests.isSet.cameraFOV then
			cam_fov = publicRequests.cameraFOV
			publicRequests.isSet.cameraFOV = false
		end

		if cam_fov > ply_firstPersonFOV then
			cam_fov = ply_firstPersonFOV
		end

		local changeSpeed = ( cameraFOV - cam_fov ) * (GetConVar( MithralThirdPerson_Config.var_name.CAMERA_FOV_CHANGE_SPEED ):GetFloat() * ( framedelta * 10 ))


		cameraFOV = math.Approach( cameraFOV, cam_fov, changeSpeed)

		cameraFOV = math.Clamp( cameraFOV, 1, ply_firstPersonFOV )


		if IsPlayerViewObstructed() or IsFirstPersonForcedFallback() then
			cameraFOV = ply_firstPersonFOV
		end

		--[[
		MithralThirdPerson_debug.draw_text( "UpdateCameraFOV:", 2)
		MithralThirdPerson_debug.draw_text( "	 cam_fov: " .. cam_fov .. " cameraFOV: " .. cameraFOV .. " GetFOV: " .. ply_firstPersonFOV )
		MithralThirdPerson_debug.draw_text( "	 changeSpeed: " .. changeSpeed)
		MithralThirdPerson_debug.draw_text( "	 publicRequests.cameraFOV: " .. tostring(publicRequests.cameraFOV) .. " publicRequests.cameraAimFOV: " .. tostring(publicRequests.cameraAimFOV))
		--]]

	end

	local function InitParameters( origin, angles, fov )

		cameraOrigin = cameraOrigin or Vector( origin )
		fakePlayerAngle = fakePlayerAngle or Angle( angles )
		cameraAngles = cameraAngles or Angle( angles )
		cameraFOV = cameraFOV or fov
		playerAngles = playerAngles or Angle( angles )
		viewpunch_angle = viewpunch_angle or Angle()
		framedelta = framedelta or FrameTime()

		InitParameters = function() end

	end

	local function IsAddonActive()

		if ResultCache.tick_IsAddonActive == engine.TickCount() then
			return ResultCache.IsAddonActive
		end

		local ply = LocalPlayer()

		local isEnabled = GetConVar( MithralThirdPerson_Config.var_name.ADDON_ENABLED ):GetBool()
		local alive = true
		local inVehicle = false

		if IsValid( ply ) then
			inVehicle = ply:InVehicle()
			alive = ply:Alive()
		end

		val_isAddonActive = isEnabled and alive and not inVehicle

		ResultCache.tick_IsAddonActive = engine.TickCount()
		ResultCache.IsAddonActive = val_isAddonActive

		return val_isAddonActive

	end

	local function IsNeedDrawCrosshair()
		return not GetConVar( MithralThirdPerson_Config.var_name.CROSSHAIR_HIDDEN_IF_NOT_AIMING ):GetBool() or ( IsAiming() or IsPlayerInAimMode() )
	end

	local function IsCustomCrosshairsEnabled()
		return GetConVar( MithralThirdPerson_Config.var_name.STATIC_CROSSHAIR ):GetBool() or GetConVar( MithralThirdPerson_Config.var_name.TRACE_CROSSHAIR ):GetBool()
	end



	function MithralThirdPersonHooks.HUDShouldDraw( name )

		MithralThirdPerson_debug.bench_measure( "HUDShouldDraw" )

		if ( IsAddonActive() ) and ( ( name == "CHudCrosshair" ) and ( not IsNeedDrawCrosshair() or IsCustomCrosshairsEnabled() ) ) then
			MithralThirdPerson_debug.bench_measure( "HUDShouldDraw" )
			return false
		end

		MithralThirdPerson_debug.bench_measure( "HUDShouldDraw" )

	end

	function MithralThirdPersonHooks.HUDPaint()

		MithralThirdPerson_debug.bench_measure( "HUDPaint" )

		if ( IsAddonActive() and IsNeedDrawCrosshair() ) then


			if GetConVar( MithralThirdPerson_Config.var_name.TRACE_CROSSHAIR ):GetBool() then

				local pos = PlayerEyeTraceEnd():ToScreen()
				pos.x, pos.y = math.Round( pos.x ), math.Round( pos.y )

				surface.SetDrawColor(255, 255, 255, 255)

				surface.DrawLine(pos.x - 6, pos.y, pos.x - 9, pos.y)
				surface.DrawLine(pos.x + 6, pos.y, pos.x + 9, pos.y)

				surface.DrawLine(pos.x, pos.y - 6, pos.x, pos.y - 9)
				surface.DrawLine(pos.x, pos.y + 6, pos.x, pos.y + 9)

				surface.SetDrawColor(0, 0, 0, 255)

				surface.DrawLine(pos.x - 4, pos.y, pos.x - 7, pos.y)
				surface.DrawLine(pos.x + 4, pos.y, pos.x + 7, pos.y)

				surface.DrawLine(pos.x, pos.y - 4, pos.x, pos.y - 7)
				surface.DrawLine(pos.x, pos.y + 4, pos.x, pos.y + 7)

				surface.SetDrawColor( MithralThirdPerson_Config.current_value_color.TRACE_CROSSHAIR_COLOUR:Unpack() )

				surface.DrawLine(pos.x - 5, pos.y, pos.x - 8, pos.y)
				surface.DrawLine(pos.x + 5, pos.y, pos.x + 8, pos.y)

				surface.DrawLine(pos.x, pos.y - 5, pos.x, pos.y - 8)
				surface.DrawLine(pos.x, pos.y + 5, pos.x, pos.y + 8)

			end

			if GetConVar( MithralThirdPerson_Config.var_name.STATIC_CROSSHAIR ):GetBool() then

				local scrn_h = bit.rshift( ScrH(), 1 )
				local scrn_w = bit.rshift( ScrW(), 1 )

				surface.SetDrawColor( MithralThirdPerson_Config.current_value_color.STATIC_CROSSHAIR_COLOUR:Unpack() )

				surface.DrawLine(scrn_w - 5, scrn_h, scrn_w - 8, scrn_h)
				surface.DrawLine(scrn_w + 5, scrn_h, scrn_w + 8, scrn_h)

				surface.DrawLine(scrn_w, scrn_h - 5, scrn_w, scrn_h - 8)
				surface.DrawLine(scrn_w, scrn_h + 5, scrn_w, scrn_h + 8)

			end

		end

		MithralThirdPerson_debug.bench_measure( "HUDPaint" )

	end

	function MithralThirdPersonHooks.InputMouseApply( cmd, x, y, ang )

		MithralThirdPerson_debug.bench_measure( "InputMouseApply" )


		if ( IsAddonActive() ) then

			if OnLastTick.InputMouseApply.FrameNumber == FrameNumber() then
				if not IsFirstPersonNeeded() then
					-- Reaffirms player movement for consistency. Otherwise nolciping will have issues with going backwards. Somtimes.
					UpdatePlayerMove( cmd )
				end
			else
				OnLastTick.InputMouseApply.FrameNumber = FrameNumber()

				CameraObstruction.SpawnMenuOpen = false
				CameraObstruction.ContextMenuOpen = false
				Editor_Helpers.ContextMenuOpen = false

				framedelta = FrameTime()


				UpdateCameraAngles( cmd, x, y )
				UpdatePlayerAngles( x )
				UpdateLastTicVariables()


				if not IsFirstPersonNeeded() then

					RotatePlayer( cmd, ang )
					UpdatePlayerMove( cmd )

				end
				OnLastTick.InputMouseApply.camera_angle_add:Zero()
			end

			if experimental then
				local copy_ang = Angle( ang )
				if not IsFirstPersonNeeded() then

					copy_ang:Sub( OnLastTick.InputMouseApply.camera_angle_add )

					OnLastTick.InputMouseApply.camera_angle_add:Add( copy_ang )

				end

				--[[
						MithralThirdPerson_debug.draw_text( "MithralThirdPersonHooks.InputMouseApply", 3 )
						MithralThirdPerson_debug.draw_text( "  x: " .. x )
						MithralThirdPerson_debug.draw_text( "  y: " .. y )
						MithralThirdPerson_debug.draw_text( "  ang: " .. table.ToString( ang:ToTable() ) )
						MithralThirdPerson_debug.draw_text( "  copy_ang: " .. table.ToString( copy_ang:ToTable() ) )
						MithralThirdPerson_debug.draw_text( "  playerAngles: " .. table.ToString( playerAngles:ToTable() ) )
						MithralThirdPerson_debug.draw_text( "  cameraAngles: " .. table.ToString( cameraAngles:ToTable() ) )
						MithralThirdPerson_debug.draw_text( "  OnLastTick.InputMouseApply.camera_angle_add: " .. table.ToString( OnLastTick.InputMouseApply.camera_angle_add:ToTable() ) )
				--]]
			end

			SetPlayerPassiveHoldType()

			MithralThirdPerson_debug.bench_measure( "InputMouseApply" )

			return true
		end

		MithralThirdPerson_debug.bench_measure( "InputMouseApply" )
	end

	function MithralThirdPersonHooks.CalcView( ply, origin, angles, fov )

		MithralThirdPerson_debug.bench_measure( "CalcView" )

		if ( IsAddonActive() ) then


			InitParameters( origin, angles, fov )

			if IsValid( ply ) then

				local hooked_camAngle, hooked_camFOV = hook.Run( "ThirdPersonCalcView", ply, cameraAngles, cameraFOV )

				weaponVars.current = ply:GetActiveWeapon()

				if weaponVars.current:IsValid() then
					if weaponVars.current.CalcView then
						weaponVars.vec, weaponVars.ang, weaponVars.fov = weaponVars.current:CalcView( ply, Vector( cameraOrigin ), Angle(), cameraFOV )

						weaponVars.vec = weaponVars.vec or Vector()
						weaponVars.ang = weaponVars.ang or Angle()
						weaponVars.fov = weaponVars.fov or 90

						--[[
						MithralThirdPerson_debug.draw_text( "weaponVars.vec: " .. table.ToString( weaponVars.vec:ToTable() ) )
						MithralThirdPerson_debug.draw_text( "weaponVars.ang: " .. table.ToString( weaponVars.ang:ToTable() ) )
						MithralThirdPerson_debug.draw_text( "weaponVars.fov: " .. tostring( weaponVars.fov ) )
						--]]
					end
				end

				cameraAngles = hooked_camAngle or cameraAngles
				if isnumber( hooked_camFOV ) then
					publicRequests.isSet.cameraFOV = true
					publicRequests.cameraFOV = hooked_camFOV
				end

				UpdateCameraOrigin( ply, origin )
				UpdateCameraTrace()
				UpdateCameraFOV()
			end


			if not IsFirstPersonNeeded() or CameraObstruction.ContextMenuOpen then


				cameraAngles_viewpunched = viewpunch_angle + cameraAngles + weaponVars.ang
				weaponVars.ang:Zero()
				cameraAngles_viewpunched:Normalize()

				MithralThirdPerson_debug.bench_measure( "CalcView" )

				return {
					origin = cameraOrigin,
					angles = cameraAngles_viewpunched,
					fov = cameraFOV,
					drawviewer = not IsFirstPersonNeeded()
				}

			end


		else
			cameraAngles = Angle( angles )
			cameraAngles.r = 0
			playerAngles = Angle( angles )
		end


		MithralThirdPerson_debug.bench_measure( "CalcView" )

	end

	function MithralThirdPersonHooks.CreateMove( mv )

		MithralThirdPerson_debug.bench_measure( "CreateMove" )

		if IsAddonActive() then
			if	( isangle( cameraAngles )
				--and ( IsDisableRotationWhenMove() or ( AimLockTimer() or IsPlayerInAimMode() ) )
				and ( not CameraDependantMovement() ) )
				and ( not IsFirstPersonNeeded() )
			then
				-- SetMoveAngles is busted on clients, thanks to that we need this hack.

					move_vector:SetUnpacked(mv:GetForwardMove(), mv:GetSideMove(), mv:GetUpMove())

					if AimLockTimer() or val_needToAim or IsAiming() then
						move_vector:Rotate( LocalPlayer():EyeAngles() - cameraAngles )

					elseif IsDisableRotationWhenMove() then
						move_vector:Rotate( Angle( 0, LocalPlayer():EyeAngles().y - cameraAngles.y, 0 ) )

					else
						move_vector:Rotate( Angle( 0, LocalPlayer():EyeAngles().y - playerAngles.y, 0 ) )
					end

					mv:SetForwardMove(move_vector.x)
					mv:SetSideMove(move_vector.y)
					mv:SetUpMove(move_vector.z)

					--[[
					MithralThirdPerson_debug.draw_text( "  IsDisableRotationWhenMove(): " .. tostring( IsDisableRotationWhenMove() ) )
					MithralThirdPerson_debug.draw_text( "  " .. table.ToString( move_vector:ToTable(), "move_vector", true ) )
					--MithralThirdPerson_debug.draw_text( "  " .. table.ToString( Angle( 0, LocalPlayer():EyeAngles().y - cameraAngles.y, 0 ):ToTable(), "cameraAngles Rotate", true ) )
					--MithralThirdPerson_debug.draw_text( "  " .. table.ToString( Angle( 0, math.NormalizeAngle(LocalPlayer():EyeAngles().y - playerAngles.y), 0 ):ToTable(), "playerAngles Rotate", true ) )
					MithralThirdPerson_debug.draw_text( "  " .. table.ToString( OnLastTick.UpdatePlayerAngles.cameraAngles:ToTable(), "OnLastTick.UpdatePlayerAngles.cameraAngles", true ) )
					MithralThirdPerson_debug.draw_text( "  " .. table.ToString( LocalPlayer():EyeAngles():ToTable(), "LocalPlayer():EyeAngles()", true ) )
					MithralThirdPerson_debug.draw_text( "  " .. table.ToString( LocalPlayer():GetPos():ToTable(), "LocalPlayer():GetPos()", true ) )
					--MithralThirdPerson_debug.draw_line( LocalPlayer():GetPos(), LocalPlayer():GetPos() + Angle( 0, LocalPlayer():EyeAngles().y - cameraAngles.y, 0 ):Forward() * 32, 1, FrameTime() )
					MithralThirdPerson_debug.draw_line( LocalPlayer():GetPos(), LocalPlayer():GetPos() + Angle( 0, LocalPlayer():EyeAngles().y - playerAngles.y, 0, 0 ):Forward() * 96, 2 )
					MithralThirdPerson_debug.draw_line( LocalPlayer():GetPos(), LocalPlayer():GetPos() + LocalPlayer():EyeAngles():Forward() * 64, 3, FrameTime())
					MithralThirdPerson_debug.draw_line( LocalPlayer():GetPos(), LocalPlayer():GetPos() + Vector(mv:GetForwardMove(), mv:GetSideMove(), mv:GetUpMove()) * 128, 4, FrameTime())
					--]]

			end

			if GetConVar( MithralThirdPerson_Config.var_name.AIM_ON_ATTACK ):GetBool() then

				-- It's just easier to call the function 5 times than write a for loop.
				-- Now it's 6
				-- And now it's 12, two sets of 6 for each slightly different code path... ISN'T SOURCE ENGINE FUN?

				if engine.TickInterval() < framedelta then
					MithralThirdPerson_CreateMove.AddKeyDelayFrame( IN_ATTACK, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayFrame( IN_ALT1, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayFrame( IN_ALT2, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayFrame( IN_USE, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayFrame( IN_ATTACK2, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayFrame( IN_RELOAD, mv )
				else
					MithralThirdPerson_CreateMove.AddKeyDelayTick( IN_ATTACK, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayTick( IN_ALT1, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayTick( IN_ALT2, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayTick( IN_USE, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayTick( IN_ATTACK2, mv )
					MithralThirdPerson_CreateMove.AddKeyDelayTick( IN_RELOAD, mv )
				end

			end

			MithralThirdPerson_CreateMove.AddKeysToCMD( MithralThirdPerson_think.Behaviour_GetHoldDownKeys(), mv )

			local tapkey = MithralThirdPerson_think.Behaviour_GetTapKeys()

			if tapkey ~= 0 then
				if OnLastTick.CreateMove.keytap == 0 then
					OnLastTick.CreateMove.keytap_currentTick = engine.TickCount()
					OnLastTick.CreateMove.keytap = tapkey
					OnLastTick.CreateMove.keyUnTap = tapkey
				end
				--[[
				-- Lets see if anyone complains about switching weapons not triggering key taps
				if OnLastTick.CreateMove.keytap ~= tapkey then

					OnLastTick.CreateMove.keytap_currentTick = engine.TickCount()
					OnLastTick.CreateMove.keyUnTap_CurrentTick = engine.TickCount()
					OnLastTick.CreateMove.keyUnTap = OnLastTick.CreateMove.keytap
					OnLastTick.CreateMove.keytap = tapkey

					--MithralThirdPerson_CreateMove.AddKeysToCMD( OnLastTick.CreateMove.keytap, mv )
					--OnLastTick.CreateMove.keytap = tapkey
					--MithralThirdPerson_CreateMove.AddKeysToCMD( OnLastTick.CreateMove.keytap, mv )
				end
				--]]

				if OnLastTick.CreateMove.keytap_currentTick == engine.TickCount() then
					MithralThirdPerson_CreateMove.AddKeysToCMD( OnLastTick.CreateMove.keytap, mv )
				end
			elseif OnLastTick.CreateMove.keytap ~= 0 then
				OnLastTick.CreateMove.keyUnTap_CurrentTick = engine.TickCount()
			end

			if OnLastTick.CreateMove.keyUnTap_CurrentTick == engine.TickCount() then
				MithralThirdPerson_CreateMove.AddKeysToCMD( OnLastTick.CreateMove.keyUnTap, mv )
				OnLastTick.CreateMove.keytap = 0
			end

			-- For later
			-- This is the equivilent of picking up the phone to see if it's ringing.
			if mv:GetImpulse() == 100 then

				mv:SetImpulse( 0 )
				OnLastTick.CreateMove.impulse100 = true
				OnLastTick.CreateMove.flashlight_check = true

			elseif not OnLastTick.CreateMove.flashlight_check then

				OnLastTick.CreateMove.impulse100 = false

			end

			--[[
			MithralThirdPerson_debug.draw_text("MithralThirdPersonHooks.CreateMove:", 3)
			MithralThirdPerson_debug.draw_text("  OnLastTick.CreateMove.impulse100: " .. tostring( OnLastTick.CreateMove.impulse100 ) .. " OnLastTick.flashlight_check: " .. tostring( OnLastTick.CreateMove.flashlight_check ) )
			MithralThirdPerson_debug.draw_text("  OnLastTick.CreateMove.keytap: " .. tostring( OnLastTick.CreateMove.keytap ) )
			MithralThirdPerson_debug.draw_text("  OnLastTick.CreateMove.keyUnTap: " .. tostring( OnLastTick.CreateMove.keyUnTap ) )
			MithralThirdPerson_debug.draw_text("  OnLastTick.CreateMove.keytap_currentTick: " .. tostring( OnLastTick.CreateMove.keytap_currentTick ) )
			MithralThirdPerson_debug.draw_text("  OnLastTick.CreateMove.keyUnTap_CurrentTick: " .. tostring( OnLastTick.CreateMove.keyUnTap_CurrentTick ) )
			--]]

			--[[
			MithralThirdPerson_debug.draw_text( "CreateMove", 4 )
			--MithralThirdPerson_debug.draw_text( "  ang: " .. table.ToString( ang:ToTable() ) )
			--MithralThirdPerson_debug.draw_text( "  copy_ang: " .. table.ToString( copy_ang:ToTable() ) )
			MithralThirdPerson_debug.draw_text( "  playerAngles: " .. table.ToString( playerAngles:ToTable() ) )
			MithralThirdPerson_debug.draw_text( "  cameraAngles: " .. table.ToString( cameraAngles:ToTable() ) )
			MithralThirdPerson_debug.draw_text( "  OnLastTick.InputMouseApply.camera_angle_add: " .. table.ToString( OnLastTick.InputMouseApply.camera_angle_add:ToTable() ) )
			MithralThirdPerson_debug.draw_text( "  TESTING_VALUES.CreateMove: " .. table.ToString( mv:GetViewAngles():ToTable() ) )
			MithralThirdPerson_debug.draw_text( "  TESTING_VALUES.add_angle: " .. table.ToString( TESTING_VALUES.add_angle:ToTable() ) )
			--]]


			--[[
			TESTING_VALUES.CreateMove = mv:GetViewAngles()
			TESTING_VALUES.copy_angle:Zero()

			TESTING_VALUES.copy_angle:Sub( TESTING_VALUES.CreateMove )

			TESTING_VALUES.add_angle:Add( TESTING_VALUES.copy_angle )
			--]]
		end
		

		MithralThirdPerson_debug.bench_measure( "CreateMove" )

	end

	function MithralThirdPersonHooks.RenderScreenspaceEffects()

		MithralThirdPerson_debug.bench_measure( "RenderScreenspaceEffects.Other" )

		if IsAddonActive() then

			MithralThirdPerson_renderer.ThirdPerson_Flashlight( { trace_result = cameraTraceResult, isFirstPersonForced = IsFirstPersonNeeded(), last_tick = OnLastTick.CreateMove.impulse100 } )

			OnLastTick.CreateMove.flashlight_check = MithralThirdPerson_renderer.return_flashlight_check()

		else
			MithralThirdPerson_renderer.Remove()
		end

		MithralThirdPerson_debug.bench_measure( "RenderScreenspaceEffects.Other" )

	end

	function MithralThirdPersonHooks.RenderScreenspaceEffects()

		MithralThirdPerson_debug.bench_measure( "RenderScreenspaceEffects" )

		if IsAddonActive() then

			-- Fuck it, not dealing with stupid rendering issues on windows
			if ( system.IsLinux() and experimental ) or MithralThirdPerson_debug.IsDebugEnabled() then
				MithralThirdPerson_renderer.Render_FakeDOF( IsAiming() )
			end

			MithralThirdPerson_renderer.ThirdPerson_Flashlight( { trace_result = cameraTraceResult, isFirstPersonForced = IsFirstPersonNeeded(), last_tick = OnLastTick.CreateMove.impulse100, campos = cameraOrigin, campos_rel = camera_current_pos } )

			OnLastTick.CreateMove.flashlight_check = MithralThirdPerson_renderer.return_flashlight_check()

		else
			MithralThirdPerson_renderer.Remove()
		end

		MithralThirdPerson_debug.bench_measure( "RenderScreenspaceEffects" )

	end

	function MithralThirdPersonHooks.OnContextMenuOpen()

		if GetConVar( MithralThirdPerson_Config.var_name.FORCED_FIRSTPERSON_CONTEXT_MENU ):GetBool() then
			if not Editor_Helpers.OpenInContextMenu then
				CameraObstruction.ContextMenuOpen = true
			end
			Editor_Helpers.ContextMenuOpen = true
		end

	end

	function MithralThirdPersonHooks.OnContextMenuClose()
		CameraObstruction.ContextMenuOpen = false
		Editor_Helpers.ContextMenuOpen = false
	end

	function MithralThirdPersonHooks.OnSpawnMenuOpen()
		CameraObstruction.SpawnMenuOpen = true
	end

	function MithralThirdPersonHooks.OnSpawnMenuClose()
		CameraObstruction.SpawnMenuOpen = false
	end

	function MithralThirdPersonHooks.Think()

		MithralThirdPerson_debug.bench_measure( "Think" )

		if IsAddonActive() then
			MithralThirdPerson_think.Behaviour_Run()
		end

		MithralThirdPerson_debug.bench_measure( "Think" )

	end



	hook.Add( "DrawOverlay", "MithralThirdPersonHooks.DrawOverlay", MithralThirdPerson_debug.bench_draw_overlay ) -- frame time based
	hook.Add( "HUDShouldDraw", "MithralThirdPersonHooks.HUDShouldDraw", MithralThirdPersonHooks.HUDShouldDraw ) -- runs multiple times per frame.
	hook.Add( "HUDPaint", "MithralThirdPersonHooks.HUDPaint", MithralThirdPersonHooks.HUDPaint )
	hook.Add( "InputMouseApply", "MithralThirdPersonHooks.InputMouseApply", MithralThirdPersonHooks.InputMouseApply ) -- frame time + 66 (tickrate?) based
	hook.Add( "CalcView", "MithralThirdPersonHooks.CalcView", MithralThirdPersonHooks.CalcView ) -- frame time based
	hook.Add( "CreateMove", "MithralThirdPersonHooks.CreateMove", MithralThirdPersonHooks.CreateMove ) -- frame time + 66 (tickrate?) based
	hook.Add( "RenderScreenspaceEffects", "MithralThirdPersonHooks.RenderScreenspaceEffects", MithralThirdPersonHooks.RenderScreenspaceEffects ) -- frame time + (wtf is it) based
	hook.Add( "OnContextMenuOpen", "MithralThirdPersonHooks.OnContextMenuOpen", MithralThirdPersonHooks.OnContextMenuOpen )
	hook.Add( "OnContextMenuClose", "MithralThirdPersonHooks.OnContextMenuClose", MithralThirdPersonHooks.OnContextMenuClose )
	hook.Add( "OnSpawnMenuOpen", "MithralThirdPersonHooks.OnSpawnMenuOpen", MithralThirdPersonHooks.OnSpawnMenuOpen )
	hook.Add( "OnSpawnMenuClose", "MithralThirdPersonHooks.OnSpawnMenuClose", MithralThirdPersonHooks.OnSpawnMenuClose )
	hook.Add( "Think", "MithralThirdPersonHooks.Think", MithralThirdPersonHooks.Think )



	if experimental and MithralThirdPerson_debug.IsDebugEnabled() then
		--TODO

		local dbg_offset = 0
		local dbg_lastframe = 0

		local function GetNewDBOffset()
			if dbg_lastframe ~= FrameNumber() then
				dbg_lastframe = FrameNumber()
				dbg_offset = 30
			end
			dbg_offset = dbg_offset + 1
			return dbg_offset - 1
		end

		--[[

		-- If you're feeling adventurous feel free to try and the whole "context menu doesn't work outside the first person perspective" fix.
		-- I tried to fix it without going to first person mode but there's one huge obstacle: 'Panel:SetWorldClicker()', it has close to no documentation on how it works and it has issue #3467 on garrysmod github issue tracker that mentions 'GM:CalcView' FOV related issues, saddly it's turtles all the way down, even if FOV values would match 'SetWorldClicker' doesn't know where the camera is and assumes that the player is be all and end all.
		-- The following is one part of the fix that works, you can right-click on an object in the world and it will open the context menu for that world object.

		-- Needed for 'util.AimVector' since it has an issue with propper FOV values. This table straight up brute forced pre-computed table of FOV values when added to the current camera FOV would result a propper working world clicker.
		-- Screw it, gaint table it is then.
		local ScreenToWorldFOVAdd = {
			[10] = 3.3071000000006,
			[11] = 3.6318000000007,
			[12] = 3.9548000000014,
			[13] = 4.2758000000008,
			[14] = 4.5949999999986,
			[15] = 4.9119999999979,
			[16] = 5.2266999999971,
			[17] = 5.5389999999964,
			[18] = 5.8486999999957,
			[19] = 6.1557999999973,
			[20] = 6.4599999999966,
			[21] = 6.7612999999959,
			[22] = 7.0594999999952,
			[23] = 7.3543999999992,
			[24] = 7.6461999999985,
			[25] = 7.9342999999978,
			[26] = 8.2189999999972,
			[27] = 8.4999999999965,
			[28] = 8.7770999999959,
			[29] = 9.0504999999903,
			[30] = 9.3197999999993,
			[31] = 9.5850999999986,
			[32] = 9.845999999998,
			[33] = 10.102899999997,
			[34] = 10.355299999997,
			[35] = 10.603199999996,
			[36] = 10.846699999996,
			[37] = 11.085499999995,
			[38] = 11.319599999995,
			[39] = 11.548999999994,
			[40] = 11.773499999998,
			[41] = 11.993299999998,
			[42] = 12.207999999997,
			[43] = 12.417699999997,
			[44] = 12.622399999996,
			[45] = 12.821999999996,
			[46] = 13.016299999995,
			[47] = 13.205499999995,
			[48] = 13.389599999994,
			[49] = 13.568099999994,
			[50] = 13.741399999994,
			[51] = 13.909399999993,
			[52] = 14.072099999993,
			[53] = 14.229499999992,
			[54] = 14.381199999992,
			[55] = 14.527699999992,
			[56] = 14.668599999998,
			[57] = 14.804099999998,
			[58] = 14.934099999998,
			[59] = 15.058599999998,
			[60] = 15.1777,
			[61] = 15.291199999999,
			[62] = 15.399299999999,
			[63] = 15.501799999999,
			[64] = 15.598699999999,
			[65] = 15.690499999998,
			[66] = 15.776499999998,
			[67] = 15.857099999998,
			[68] = 15.932399999998,
			[69] = 16.002099999998,
			[70] = 16.066399999998,
			[71] = 16.125499999997,
			[72] = 16.179099999997,
			[73] = 16.227499999997,
			[74] = 16.270299999997,
			[75] = 16.307899999999,
			[76] = 16.340299999999,
			[77] = 16.367499999999,
			[78] = 16.389299999999,
			[79] = 16.406299999999,
			[80] = 16.417799999999,
			[81] = 16.424199999999,
			[82] = 16.425699999999,
			[83] = 16.422099999999,
			[84] = 16.413399999999,
			[85] = 16.400099999999,
			[86] = 16.381799999999,
			[87] = 16.358299999999,
			[88] = 16.330099999999,
			[89] = 16.297199999999,
			[90] = 16.259799999999,
			[91] = 16.217699999995,
			[92] = 16.170299999995,
			[93] = 16.118799999995,
			[94] = 16.062699999995,
			[95] = 16.002099999995,
			[96] = 15.936799999995,
			[97] = 15.867499999996,
			[98] = 15.793599999996,
			[99] = 15.715699999996,
			[100] = 15.632999999996,
		}

		ThirdPerson.old_ScreenToVector = ThirdPerson.old_ScreenToVector or gui.ScreenToVector
		if IsAddonActive() and not GetConVar( MithralThirdPerson_Config.var_name.FORCED_FIRSTPERSON_CONTEXT_MENU ):GetBool() and not Editor_Helpers.OpenInContextMenu then
			gui.ScreenToVector = function( mx, my )
				return util.AimVector( cameraAngles, cameraFOV + ( ScreenToWorldFOVAdd[math.Truncate( math.abs( math.Clamp( cameraFOV, 10, 100 ) ) )] ), mx, my, ScrW(), ScrH() )
			end
		else
			gui.ScreenToVector = ThirdPerson.old_ScreenToVector
		end
		--]]


	end -- experimental

	if not experimental and not MithralThirdPerson_debug.IsDebugEnabled() then
		hook.Remove( "DrawOverlay", "MithralThirdPersonHooks.DrawOverlay" )
		MithralThirdPerson_debug.bench_measure = function()end
	end



	concommand.Remove( MithralThirdPerson_COM_SET_CAMETA_ANGLE )
	concommand.Add( MithralThirdPerson_COM_SET_CAMETA_ANGLE, SetCameraAngleCommand )

	concommand.Remove( MithralThirdPerson_COM_INVERT_CAMERA_RIGHT )
	concommand.Add( MithralThirdPerson_COM_INVERT_CAMERA_RIGHT, InvertCameraRightPos )

	concommand.Remove( MithralThirdPerson_COM_INVERT_CAMERA_AIM_RIGHT )
	concommand.Add( MithralThirdPerson_COM_INVERT_CAMERA_AIM_RIGHT, InvertCameraAimRightPos )

	concommand.Remove( MithralThirdPerson_COM_INVERT_CAMERA_RIGHT_COMBINED )
	concommand.Add( MithralThirdPerson_COM_INVERT_CAMERA_RIGHT_COMBINED, InvertCameraRightPosCombined )

	concommand.Remove( MithralThirdPerson_COM_TOGGLE_ADDON )
	concommand.Add( MithralThirdPerson_COM_TOGGLE_ADDON, AddonToggle )

	print( "Mithral Third Person revision: " .. MithralThirdPerson_revision )

end

print( "Mithral Third Person script init time: " .. math.Truncate(( SysTime() - script_init_time ) * 1000, 2) .. "ms" )
script_init_time = nil
