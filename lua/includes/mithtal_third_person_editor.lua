-- Debug
local MithralThirdPerson_debug = include( "includes/mithtal_third_person_debug.lua" )
local MithralThirdPerson_revision = include( "includes/mithtal_third_person_revision.lua" )
local experimental = GetConVar( MithralThirdPerson_Config.var_name.EXPERIMENTAL ):GetBool() --  It shouldn't get enabled.

local EditorExclusiveCalls = {
	Behaviour = {},
	Main = {}
}

local Editor = {
	IconLocation = "icon",
	categories = {
		sidePanel = {}
	},
	destroyList = {},
	CurrentlyActivePanel = "",

	debug = {
		drawButtonBounds = false,
		consolePrint = false,
		randomScale = false,
		Scale = math.Rand( 0.75, 2.0 ),
	},

	colours = {
		Base = Color( 255, 255, 255 ),
		BaseAlt = Color( 231, 231, 231),
		Accent = Color( 138, 155, 195),
	--	Accent = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 120 ),
		Error = Color( 219, 82, 72),
		okstatus = Color( 72, 219, 128),
		TextDark = Color( 32, 32, 32),
		TextLight = Color( 255, 255, 255),
		TextGray = Color( 127, 127, 127)
	},

	PanelMinWidth = 512,
	PanelMinHeight = 756,
	wScale = 0,
	hScale = 0,
	Scale = 0,
	tileSize = 12,
	elementSizeCorrection = 0,
	TextSizeCorrectionInProgress = true,
	MaxTextSize = {},
	TextContainerSize = {},

	cookieJar = {
		topName = "org.gitlab.Vavency.mithral_third_person.",
	},

	PanelTitle = "Mithral Third Person Settings",
	window = nil,
	baseContainer = nil,

	MousePosDelta = {
		x = 0,
		y = 0
	},
	PanelStaringPos = {
		x = 0,
		y = 0
	},

	categoryCount = 0,
	radius = 0,
	TitleBarClear = 32,
	DefaultTitleBarHeight = 32,
	Padding = 12,
	borderThickness = 1,

	dragabbleWindow = true,
	scrollBarButtons = false,

	polyDrawData = {
		triangleUp = {},
		triangleDown = {},
		closeSymbol1 = {},
		closeSymbol2 = {},
		Knob = {},
		dropDownArrow = {}
	},

	Font = {
		Name = "",
		HalfName = "",
		DoubleName = "",
		Scale = nil,
		PixelSize = nil,
		Created = false
	}
}

Editor.cookieJar.lastPanel = Editor.cookieJar.topName .. "last_panel"
Editor.PanelWidth = Editor.PanelMinWidth * Editor.wScale
Editor.PanelHeight = Editor.PanelMinHeight * Editor.hScale

-- This is a mess but I hope that it avoids any kind of crashes.
local function InitFont( scale )

	if Editor.Font.Scale == nil then

		if not Editor.Font.Created then

			local FontSize = math.Round( 20 * scale )
			local FontWeight = math.Round( 714 * scale )

			local HalfFontSize = math.Round( FontSize * 0.8, 0 )
			local HalfFontWeight = math.Round( FontWeight * 0.8, 0 )

			local DoubleFontSize = bit.lshift( FontSize, 1 )
			local DoubleFontWeight = bit.lshift( FontWeight, 1 )

			Editor.Font.Name = "rtp settings font"
			Editor.Font.HalfName = "rtp half settings font"
			Editor.Font.DoubleName = "rtp double settings font"

			--[[
			local pc_success, pc_errmsg = pcall( function () draw.GetFontHeight( Editor.Font.Name ) end )


			if pc_success then
				if draw.GetFontHeight( Editor.Font.Name ) == FontSize then
					MsgC( Color(132,180,202), "Map change or script refresh, the font size is still usable. Skipping creation.\n" )
					return true
				end

				Editor.Font.Name = "DermaDefault"
				MsgC( Color(188,132,202), "Was the map restared along with resolution change? Or is it the developer doing things? Falling back to " .. Editor.Font.Name .."\n" )

				return false
			end
			--]]
			if  system.IsOSX() then
				surface.CreateFont( Editor.Font.Name, {
					font = "Helvetica",
					extended = false,
					size = FontSize,
					weight = FontWeight,
					antialias = true,
					italic = false
				} )
				surface.CreateFont( Editor.Font.HalfName, {
					font = "Helvetica",
					extended = false,
					size = HalfFontSize,
					weight = HalfFontWeight,
					antialias = true,
					italic = false
				} )
				surface.CreateFont( Editor.Font.DoubleName, {
					font = "Helvetica",
					extended = false,
					size = DoubleFontSize,
					weight = DoubleFontWeight,
					antialias = true,
					italic = false
				} )
				--MsgC( Color(132,180,202), "Creating 'Helvetica' based font at " .. tostring( draw.GetFontHeight( Editor.Font.Name ) )  .." pixel size and FontSize of " .. tostring( FontSize ) .. " \n" )
			elseif system.IsLinux() then
				surface.CreateFont( Editor.Font.Name, {
					font = "DejaVu Sans",
					extended = false,
					size = FontSize,
					weight = FontWeight,
					antialias = true,
					italic = false
				} )
				surface.CreateFont( Editor.Font.HalfName, {
					font = "DejaVu Sans",
					extended = false,
					size = HalfFontSize,
					weight = HalfFontWeight,
					antialias = true,
					italic = false
				} )
				surface.CreateFont( Editor.Font.DoubleName, {
					font = "DejaVu Sans",
					extended = false,
					size = DoubleFontSize,
					weight = DoubleFontWeight,
					antialias = true,
					italic = false
				} )
				--MsgC( Color(132,180,202), "Creating 'DejaVu Sans' based font at " .. tostring( draw.GetFontHeight( Editor.Font.Name ) )  .." pixel size and FontSize of " .. tostring( FontSize ) .. " \n" )
			else
				surface.CreateFont( Editor.Font.Name, {
					font = "Tahoma",
					extended = false,
					size = FontSize,
					weight = FontWeight,
					antialias = true,
					italic = false
				} )
				surface.CreateFont( Editor.Font.HalfName, {
					font = "Tahoma",
					extended = false,
					size = HalfFontSize,
					weight = HalfFontWeight,
					antialias = true,
					italic = false
				} )
				surface.CreateFont( Editor.Font.DoubleName, {
					font = "Tahoma",
					extended = false,
					size = DoubleFontSize,
					weight = DoubleFontWeight,
					antialias = true,
					italic = false
				} )
				--MsgC( Color(132,180,202), "Creating 'Tahoma' based font at " .. tostring( draw.GetFontHeight( Editor.Font.Name ) )  .." pixel size and FontSize of " .. tostring( FontSize ) .. " \n" )
			end
			Editor.Font.Scale = scale
			Editor.Font.Created = true

			return true
		else

			Editor.Font.Name = "rtp settings font"
			--MsgC( Color(132,180,202), "Font already exists, skipping creating a new one\n" )

			return true
		end

	elseif Editor.Font.Scale == scale then--and not MithralThirdPerson_debug.IsDebugEnabled() then

		Editor.Font.Name = "rtp settings font"
		--MsgC( Color(132,180,202), "Font already exists, skipping creating a new one\n" )

		return true
	else

		Editor.Font.Name = "DermaDefault"
		MsgC( Color(202,169,132), "Font already exists but at the wrong scale, falling back to " .. Editor.Font.Name .. "\n" .. "   Did the game resolution change?\n" )

		return false
	end

	Editor.Font.Name = "DermaDefault"
	MsgC( Color(202,169,132), "How? Falling back to " .. Editor.Font.Name .. "\n" )

	return false
end

local function InitEditorVariables ( init )

	if ScrW() < Editor.PanelMinWidth then
		Editor.PanelWidth = ScrW()
	end

	if ScrH() < Editor.PanelMinHeight then
		Editor.PanelHeight = ScrH()
	end

	Editor.CurrentlyActivePanel = ""

	if init then

		Editor.MaxTextSize = {}
		Editor.TextContainerSize = {}
		Editor.TextSizeCorrectionInProgress = true
		Editor.elementSizeCorrection = 0
		Editor.categories.sidePanelNonPanelTally = 0
	else
		Editor.categories.sidePanel = {}
	end

	EditorExclusiveCalls.Main.SetShouldHoldAim( false )

	Editor.wScale = ScrW()/2560
	Editor.hScale = ScrH()/1440
	if ScrW() < ScrH() then
		Editor.Scale = Editor.wScale
	else
		Editor.Scale = Editor.hScale
	end

	--Editor.Scale = math.Rand( 0.75, 1.4 ) -- Debug

	if Editor.debug.randomScale then
		Editor.Scale = Editor.debug.Scale
		MsgC( Color( 255, 255, 150 ), "Editor.debug.Scale: " .. tostring( Editor.debug.Scale ) .. "\nDisable all debugging stuff before release!\n" )
	end

	Editor.Scale = math.Clamp( Editor.Scale, 0.75, Editor.Scale )

	--Editor.Scale = 1.7 -- Debug
	--Editor.Scale = 0.75 -- Debug
	--Editor.Scale = 1.4 -- Debug
	--Editor.Scale = 1.06 -- Debug
	--Editor.Scale = 1.1986590441675 -- Debug

	InitFont( Editor.Scale )

	Editor.radius = 5 * Editor.Scale

	local playerWeaponColour = LocalPlayer():GetWeaponColor()

	Editor.colours.Accent = Color( 255 * playerWeaponColour.x, 255 * playerWeaponColour.y, 255 * playerWeaponColour.z )

	Editor.categoryCount = 0

	Editor.TitleBarClear = 32 * Editor.Scale
	Editor.Padding = 10 * Editor.Scale

	Editor.tileSize = ( Editor.PanelMinWidth * 0.1 ) * Editor.Scale


	Editor.PanelWidth = Editor.PanelMinWidth * Editor.Scale + Editor.tileSize + ( Editor.tileSize * Editor.elementSizeCorrection )
	Editor.PanelHeight = Editor.PanelMinHeight * Editor.Scale


	if Editor.scrollBarButtons then

		Editor.polyDrawData.triangleUp = {
			{ x = 5 * math.Truncate( Editor.Scale, 0 ), y = 15 * math.Truncate( Editor.Scale, 0 ) },
			{ x = 10 * math.Truncate( Editor.Scale, 0 ), y = 5 * math.Truncate( Editor.Scale, 0 ) },
			{ x = 15 * math.Truncate( Editor.Scale, 0 ), y = 15 * math.Truncate( Editor.Scale, 0 ) }
		}

		Editor.polyDrawData.triangleDown = {
			{ x = 10 * math.Truncate( Editor.Scale, 0 ), y = 15 * math.Truncate( Editor.Scale, 0 ) },
			{ x = 5 * math.Truncate( Editor.Scale, 0 ), y = 5 * math.Truncate( Editor.Scale, 0 ) },
			{ x = 15 * math.Truncate( Editor.Scale, 0 ), y = 5 * math.Truncate( Editor.Scale, 0 ) }
		}

	end

	-- These look better

	Editor.polyDrawData.closeSymbol1 = {
		{ x = 24 * Editor.Scale, y = 22 * Editor.Scale },
		{ x = 22 * Editor.Scale, y = 24 * Editor.Scale },
		{ x = 8 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 10 * Editor.Scale, y = 8 * Editor.Scale }
	}
	Editor.polyDrawData.closeSymbol2 = {
		{ x = 24 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 10 * Editor.Scale, y = 24 * Editor.Scale },
		{ x = 8 * Editor.Scale, y = 22 * Editor.Scale },
		{ x = 22 * Editor.Scale, y = 8 * Editor.Scale }
	}

	-- For the slider knob
	Editor.polyDrawData.Knob[1] = {
		{ x = 15 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 10 * Editor.Scale, y = 15 * Editor.Scale },
		{ x = 5 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 6 * Editor.Scale, y = 1 * Editor.Scale },
		{ x = 14 * Editor.Scale, y = 1 * Editor.Scale }
	}

	Editor.polyDrawData.Knob[2] = {
		{ x = 10 * Editor.Scale, y = 15 * Editor.Scale },
		{ x = 5 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 6 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 10 * Editor.Scale, y = 14 * Editor.Scale },
		{ x = 14 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 15 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 10 * Editor.Scale, y = 15 * Editor.Scale }
	}

	Editor.polyDrawData.Knob[3] = {
		{ x = 6 * Editor.Scale, y = 1 * Editor.Scale },
		{ x = 6 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 5 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 5 * Editor.Scale, y = 0 * Editor.Scale },
		{ x = 15 * Editor.Scale, y = 0 * Editor.Scale },
		{ x = 15 * Editor.Scale, y = 1 * Editor.Scale }
	}

	Editor.polyDrawData.Knob[4] = {
		{ x = 14 * Editor.Scale, y = 0 * Editor.Scale },
		{ x = 15 * Editor.Scale, y = 0 * Editor.Scale },
		{ x = 15 * Editor.Scale, y = 10 * Editor.Scale },
		{ x = 14 * Editor.Scale, y = 10 * Editor.Scale }
	}

	Editor.polyDrawData.dropDownArrow[1] = {
		{ x = 0 * math.Truncate( Editor.Scale, 0 ), y = 0 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 5 * math.Truncate( Editor.Scale, 0 ), y = 10 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 10 * math.Truncate( Editor.Scale, 0 ), y = 0 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 9 * math.Truncate( Editor.Scale, 0 ), y = 0 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 5 * math.Truncate( Editor.Scale, 0 ), y = 9 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 1 * math.Truncate( Editor.Scale, 0 ), y = 0 * math.Truncate( Editor.Scale, 0 ) }
	}

	Editor.polyDrawData.dropDownArrow[2] = {
		{ x = 0 * math.Truncate( Editor.Scale, 0 ), y = 0 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 5 * math.Truncate( Editor.Scale, 0 ), y = 10 * math.Truncate( Editor.Scale, 0 ) },
		{ x = 10 * math.Truncate( Editor.Scale, 0 ), y = 0 * math.Truncate( Editor.Scale, 0 ) }
	}

	function Editor.polyDrawData.dropDownArrow:SetNewXOffset( new_value )
		self[1] = {
			{ x = 0 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[1][1] },
			{ x = 5 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[1][2] },
			{ x = 10 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[1][3] },
			{ x = 9 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[1][4] },
			{ x = 5 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[1][5] },
			{ x = 1 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[1][6] }
		}

		self[2] = {
			{ x = 0 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[2][1] },
			{ x = 5 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[2][2] },
			{ x = 10 + new_value * math.Truncate( Editor.Scale, 0 ), y = self[2][3] }
		}
	end
	--PrintTable( Editor )

end

local function InitWindow ( window )

	if window == nil then
		window = vgui.Create( "DFrame" )
		window:SetTitle( "" )
		--window:SetTitle( Editor.PanelTitle )
		window:SetVisible( true )
		--window:SetDraggable( true )
		--window:ShowCloseButton( true )
		window:MakePopup()
	else
		EditorExclusiveCalls.Main.SetOpenInContextMenu( true )
		EditorExclusiveCalls.Main.SetCameraObstructionContextMenuOpen( false )
	end

	EditorExclusiveCalls.Main.SetSupressAimingKey( true )

	return window

end

local function InitPanelV2 ( window, reinit )

	reinit = reinit or false

	if IsValid( Editor.PANEL ) and not reinit then
		Editor.PANEL:Close()
		Editor.PANEL:Remove()
		Editor.PANEL = nil
	end


	if not reinit then
		InitEditorVariables( true )
		Editor.PANEL = InitWindow( window )
	elseif reinit then
		InitEditorVariables( false )
		--FindLongsetTextForContainer()
		--Editor.SidePanel:Remove()
		Editor.baseContainer:Remove()
		Editor.PANEL.TitleBarPanel:Remove()
		Editor.PANEL.closeButton:Remove()
		Editor.TextSizeCorrectionInProgress = false
	end

	Editor.PANEL:SetSize( Editor.PanelWidth, Editor.PanelHeight )
	Editor.PANEL:SetPos( ScrW() - ( Editor.TitleBarClear + Editor.PanelWidth ), Editor.TitleBarClear  )
	Editor.PANEL:ShowCloseButton( false )
	Editor.PANEL:SetDraggable( false ) --Editor.dragabbleWindow )

	Editor.PANEL:DockPadding( Editor.tileSize + 5, Editor.TitleBarClear, 5, 4 )

	Editor.PANEL.Paint = function( self, width, height )
		--draw.RoundedBox( Editor.radius, 0, Editor.TitleBarClear, width, height - Editor.TitleBarClear, Editor.colours.Base )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( 0, 0, width, height )

		--surface.SetDrawColor( Editor.colours.Accent )
		--surface.DrawRect( 0, 0, width, Editor.TitleBarClear )

		--surface.SetDrawColor( Editor.colours.Accent )
		--surface.DrawRect( 0, Editor.TitleBarClear, Editor.tileSize, height - Editor.TitleBarClear )

		surface.SetDrawColor( Editor.colours.Accent )
		surface.DrawRect( 0, 0, Editor.tileSize, height )
	end

	Editor.SidePanel = vgui.Create( "DIconLayout", Editor.PANEL )
	--Editor.SidePanel:Dock( LEFT )
	--Editor.SidePanel:SetSize( Editor.TitleBarClear, Editor.PanelHeight )
	Editor.SidePanel:SetPos( 0, Editor.TitleBarClear )
	Editor.SidePanel:SetSize( Editor.tileSize, Editor.PanelHeight )
	Editor.SidePanel:SetBorder( 0 )
	Editor.SidePanel:SetLayoutDir( TOP )
	Editor.SidePanel:SetSpaceX( 0 )
	Editor.SidePanel:SetSpaceY( 0 )

	Editor.SidePanel.Paint = function() end

	-- All of the containers are parented to this panel
	Editor.baseContainer = vgui.Create( "DScrollPanel", Editor.PANEL )
	Editor.baseContainer:Dock( FILL )

	local scollBar = Editor.baseContainer:GetVBar()
	scollBar:SetHideButtons( not Editor.scrollBarButtons )
	scollBar:SetSize( 21 * Editor.Scale, 21 * Editor.Scale )


	function scollBar:Paint( width, height )
		surface.SetDrawColor( Color(0, 0, 0, 0) )
		surface.DrawRect( 0, 0, width, height )
	end
	function scollBar.btnGrip:Paint( width, height )
		surface.SetDrawColor( Editor.colours.BaseAlt )
		surface.DrawRect( 0, 0, width, height )

		surface.SetDrawColor( Color(0, 0, 0, 45) )
		surface.DrawOutlinedRect( 0, 0, width, height, Editor.borderThickness )
	end

	if Editor.scrollBarButtons then
		function scollBar.btnUp:Paint( width, height )
			surface.SetDrawColor( Editor.colours.BaseAlt )
			surface.DrawRect( 0, 0, width, height )

			surface.SetDrawColor( Color(0, 0, 0, 45) )
			surface.DrawOutlinedRect( 0, 0, width, height, Editor.borderThickness )

			surface.SetDrawColor( Editor.colours.TextDark )
			draw.NoTexture()
			surface.DrawPoly( Editor.polyDrawData.triangleUp )

		end
		function scollBar.btnDown:Paint( width, height )
			surface.SetDrawColor( Editor.colours.BaseAlt )
			surface.DrawRect( 0, 0, width, height )

			surface.SetDrawColor( Color(0, 0, 0, 45) )
			surface.DrawOutlinedRect( 0, 0, width, height, Editor.borderThickness )

			surface.SetDrawColor( Editor.colours.TextDark )
			draw.NoTexture()
			surface.DrawPoly( Editor.polyDrawData.triangleDown )

		end
	end

	-- The default derma controls aren't great looking, lets do our own then!
	-- This panel served one purpose but now it does two jobs:
	-- 1: cover up slider knobs that don't obey the panel clipping
	-- 2: act as a grab bar for the panel
	Editor.PANEL.TitleBarPanel = vgui.Create( "DPanel", Editor.PANEL )
	Editor.PANEL.TitleBarPanel:SetPos( 0, 0 )
	Editor.PANEL.TitleBarPanel:SetSize( Editor.PanelWidth, Editor.TitleBarClear )
	Editor.PANEL.TitleBarPanel.PanelDrag = false

	Editor.PANEL.TitleBarPanel.Paint = function( self, width, height )
		surface.SetDrawColor( Editor.colours.Accent )
		surface.DrawRect( 0, 0, width, height )
	end



	Editor.PANEL.closeButton = vgui.Create( "DButton", Editor.PANEL )
	Editor.PANEL.closeButton:SetText( "" )
	Editor.PANEL.closeButton:SetSize( Editor.TitleBarClear, Editor.TitleBarClear )
	Editor.PANEL.closeButton:SetPos( Editor.PanelWidth - Editor.TitleBarClear, 0 )
	Editor.PANEL.closeButton.DoClick = function ()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
		EditorExclusiveCalls.Main.SetSupressAimingKey( false )
		if EditorExclusiveCalls.Main.GetOpenInContextMenu() then
			EditorExclusiveCalls.Main.SetOpenInContextMenu( false )
			EditorExclusiveCalls.Main.SetCameraObstructionContextMenuOpen( true )
		end
		Editor.PANEL:Close()
		Editor.PANEL:Remove()
		Editor.PANEL = nil
	end
	Editor.PANEL.closeButton.Paint = function ( self, width, height )
		surface.SetDrawColor( ColorAlpha( Editor.colours.Base, 127 ) )
		surface.DrawRect( 0, 0, width, height - ( height - Editor.TitleBarClear ) )

		-- The close symbol needs to be drawn in two parts
		surface.SetDrawColor( Editor.colours.TextDark )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.closeSymbol1 )

		-- because the poly formart can't be told that no connections when none is needed.
		surface.SetDrawColor( Editor.colours.TextDark )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.closeSymbol2 )
	end


	-- Title bar moving code.
	Editor.PANEL.TitleBarPanel.OnMousePressed = function ()

		local panelPosX, panelPosY = Editor.PANEL:GetPos()

		Editor.MousePosDelta.x = gui.MouseX() - panelPosX
		Editor.MousePosDelta.y = gui.MouseY() - panelPosY

		Editor.PANEL.TitleBarPanel:MouseCapture( true )
		Editor.PANEL.TitleBarPanel.PanelDrag = true
	end

	Editor.PANEL.TitleBarPanel.OnMouseReleased = function ()

		Editor.PANEL.TitleBarPanel:MouseCapture( false )
		Editor.PANEL.TitleBarPanel.PanelDrag = false

	end

	Editor.PANEL.Think = function ()
		if Editor.PANEL.TitleBarPanel.PanelDrag then

			-- TODO: Add clamping

			Editor.PANEL:SetPos( gui.MouseX() - Editor.MousePosDelta.x, gui.MouseY() - Editor.MousePosDelta.y )
		end
	end

end


local function CurrentActivePanel( panel_id_string )

	if panel_id_string == Editor.CurrentlyActivePanel then
		return true
	else
		Editor.CurrentlyActivePanel = panel_id_string

		--print( panel_id_string ) -- Debug

		return false
	end
end

local function CleanUpThePanels ( current_panel )

	if CurrentActivePanel( current_panel ) then
		--print( "current_panel: " .. current_panel )
		return false
	end

	for i=1, table.Count( Editor.destroyList ) do
		Editor.destroyList[i]:Remove()
	end

	table.Empty( Editor.destroyList )
	return true
end

local function PanelAnimation ( container )
	CleanUpThePanels()

	return true
end

local function IconLocation( str )

	if Editor.tileSize <= 32 then
		return Editor.IconLocation .. "32/" .. str
	end

	if Editor.tileSize <= 64 then
		return Editor.IconLocation .. "64/" .. str
	end

	if Editor.tileSize <= 128 then
		return Editor.IconLocation .. "128/" .. str
	end

	if Editor.tileSize <= 256 then
		return Editor.IconLocation .. "256/" .. str
	end

	if Editor.tileSize <= 512 then
		return Editor.IconLocation .. "512/" .. str
	end

	return Editor.IconLocation .. "1024/" .. str
end

local function IconName( name )
	return IconLocation( "editor_behaviour/" .. name .. ".png" )
end

local function TextSizeChecker( text, font, panel_width, account_line_height )

	if not isstring( text )
	or not isstring( font )
	or not isnumber( panel_width ) and panel_width ~= nil then
		return -1
	end

	local text_w, text_h = 0, 0

	-- GetTextSize doesn't take line breaks into account, thanks.

	local strings = string.Split( text, "\n" )

	for i=1, table.Count( strings ) do
		surface.SetFont( font )
		local tmp_text_w, tmp_text_h = surface.GetTextSize( strings[i] )

		if Editor.debug.consolePrint then
			MsgC( Color( 255, 255, 127 ),  strings[i] .. "\n" )
		end

		if tmp_text_w > text_w then
			text_w = tmp_text_w
		end

		if tmp_text_h > text_h then
			text_h = tmp_text_h
		end
	end

	if isbool( panel_width ) then
		if panel_width and table.Count( strings ) > 0 then
			text_h = text_h * table.Count( strings )
		end
	else
		if account_line_height and table.Count( strings ) > 0 then
			text_h = text_h * table.Count( strings )
		end
	end

	if panel_width == nil or isbool( panel_width ) then
		return text_w, text_h
	end

	if isnumber( text_w ) and isnumber( panel_width ) then

		local whileloop_counter = 0

		while text_w > ( panel_width + ( Editor.tileSize * Editor.elementSizeCorrection ) - 12 ) do
			whileloop_counter = whileloop_counter + 1

			Editor.elementSizeCorrection = Editor.elementSizeCorrection + 1

			-- Should be fine if peeps writng locale know about line breaks
			if whileloop_counter > 256 then
				break
			end
		end

		if Editor.debug.consolePrint then
			MsgC( Color( 100, 200, 200 ), "font: " .. tostring( font ) )
			MsgC( Color( 200, 200, 200 ), " text: " .. tostring( text ) .. "\n" )
			MsgC( Color( 200, 100, 200 ), "text_w: " .. tostring( text_w ) )
			MsgC( Color( 200, 200, 100 ), " panel_width: " .. tostring( panel_width ) .. "\n" )
			MsgC( Color( 255, 0, 255 ), " Editor.elementSizeCorrection: " .. tostring( Editor.elementSizeCorrection ) .. "\n")
		end

		return text_w, text_h
	end

	return nil, nil

end

local function AddTooltipPanel( basepanel, tooltip_text )

	if not isstring( tooltip_text ) then
		if Editor.debug.consolePrint then
			MsgC( Color( 255, 255, 127 ),  "AddTooltipPanel: 'tooltip_text' is invalid. Tell a programmer.\n" .. debug.traceback() .. "\n" )
		end
		return
	end

	local tooltip_panel = vgui.Create( "DPanel" )
	tooltip_panel:SetVisible( false )
	function tooltip_panel.Paint() end

	surface.SetFont( Editor.Font.Name )
	local label_w, label_h = surface.GetTextSize( tooltip_text )

	tooltip_panel:SetSize( label_w, label_h )

	local tooltip_label = vgui.Create( "DLabel", tooltip_panel )
	tooltip_label:Dock( LEFT )
	tooltip_label:SetFont( Editor.Font.Name )
	tooltip_label:SetText( tooltip_text )
	tooltip_label:SetColor( Editor.colours.TextDark )

	tooltip_label:SetSize( label_w, label_h )

	basepanel:SetTooltipPanel( tooltip_panel )
end

local function AddSideBarButton ( passedFunc, icon, tooltip, skip_register, not_a_panel )

	skip_register = skip_register or false
	not_a_panel = not_a_panel or false


	local ListItem = {
		Panel = vgui.Create( "DImageButton", Editor.SidePanel )
	}
	ListItem.Panel:SetSize( Editor.tileSize , Editor.tileSize )
	--ListItem:SetText( "" )

	local table_index = table.insert( Editor.categories.sidePanel, ListItem )
	local table_subtract = Editor.categories.sidePanelNonPanelTally

	if not_a_panel then
		Editor.categories.sidePanelNonPanelTally = Editor.categories.sidePanelNonPanelTally + 1
	end


	if isfunction( passedFunc ) then
		function ListItem.Panel.DoClick()

			if not_a_panel then
				passedFunc()
			else
				local is_cleaned = CleanUpThePanels( jit.util.funcuvname( passedFunc, 0 ) )
				if is_cleaned then
	
					passedFunc()
					cookie.Set( Editor.cookieJar.lastPanel, tostring( table_index - table_subtract ) )
				end
			end

		end
	else
		ListItem.Panel:Remove()
		ErrorNoHaltWithStack( "AddSideBarButton: passedFunc isn't a function: \"", passedFunc, "\"" )
		return -1
	end

	if not skip_register then
		passedFunc()
	end

	if isstring( tooltip ) then
		AddTooltipPanel( ListItem.Panel, tooltip )
	end

	if isstring( icon ) then
		ListItem.Panel:SetImage( icon )
	end

	ListItem.color = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )

	ListItem.Panel.Paint = function( self, width, height )
		surface.SetDrawColor( ListItem.color )
		surface.DrawRect( 0, 0, width, height )
	end


	return table_index, ListItem
end

local function AddNumberSlider ( container, conv, name, min, max, tooltip, decimal, panel_width )

	if Editor.TextSizeCorrectionInProgress then
		TextSizeChecker( name, Editor.Font.Name, Editor.tileSize * 4 )

		return false
	end

	if not isstring( tooltip ) and not tooltip == nil then
		decimal = tooltip
	end

	decimal = decimal or 0

	local ListItem = container:Add( "DNumSlider" )


	ListItem:SetSize( Editor.tileSize * ( 8 + Editor.elementSizeCorrection ), Editor.tileSize )
	ListItem:SetDark( true )
	ListItem:SetText( name or "" )
	ListItem:SetMin( min or 0 )
	ListItem:SetMax( max or 1 )
	ListItem:SetDecimals( decimal or 1 )
	ListItem:SetConVar( conv )


	-- 4 + x + 3 + 1

	function ListItem:PerformLayout() end -- It's great then you need it

	ListItem.Label:SetFont( Editor.Font.Name )
	ListItem.Label:Dock( NODOCK )

	ListItem.Slider:Dock( NODOCK )

	ListItem.TextArea:Dock( NODOCK )
	ListItem.TextArea:SetFont( Editor.Font.HalfName )

	if name ~= nil then
		ListItem.Label:SetSize( Editor.tileSize * ( 4 + Editor.elementSizeCorrection ), Editor.tileSize )
		ListItem.Label:SetPos( 0, 0 )
	
		ListItem.Slider:SetSize( Editor.tileSize * 3 , Editor.tileSize )
		ListItem.Slider:SetPos( Editor.tileSize * ( 4 + Editor.elementSizeCorrection ), 0 )
	
		ListItem.TextArea:SetSize( Editor.tileSize * 2, Editor.tileSize )
		ListItem.TextArea:SetPos( Editor.tileSize * ( 7 + Editor.elementSizeCorrection ), 0 )
	else
		ListItem.Label:SetSize( 0, Editor.tileSize )
		ListItem.Label:SetPos( 0, 0 )
	
		ListItem.Slider:SetSize( panel_width - ( Editor.tileSize * 3 ) , Editor.tileSize )
		ListItem.Slider:SetPos( 0, 0 )
	
		ListItem.TextArea:SetSize( Editor.tileSize * 3, Editor.tileSize )
		ListItem.TextArea:SetPos( panel_width - ( Editor.tileSize * 3 ), 0 )
	end


	ListItem.Slider.Knob:SetSize( 20 * Editor.Scale , 20 * Editor.Scale )

	local rng_color = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )
	--[
	function ListItem.Slider.Knob.Paint( self, width, height )
		--surface.SetDrawColor( rng_color )
		--surface.DrawRect( 0, 0, width, height )
		--surface.SetDrawColor( Editor.colours.TextDark )
		--surface.DrawRect( 0, 0, width, height )

		surface.SetDrawColor( Editor.colours.BaseAlt )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.Knob[1] )

		surface.SetDrawColor( Editor.colours.TextGray )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.Knob[2] )

		surface.SetDrawColor( Editor.colours.TextGray )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.Knob[3] )

		surface.SetDrawColor( Editor.colours.TextGray )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.Knob[4] )
	end

	if isstring( tooltip ) then
		--ListItem:SetTooltip( tooltip )
		AddTooltipPanel( ListItem, tooltip )
	end

	-- Debug
	--[[

	ListItem.Label.Paint = function ( self, width, height )
		surface.SetDrawColor( rng_color )
		surface.DrawRect( 0, 0, width, height )
	end--]]
	--[
	--local rng_color = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )

	local slider_paint_variables = 	{
		line_thickness = math.ceil( 1 * Editor.Scale, 0 ),
		line_height = Editor.tileSize * 0.5,
		line_start = 10 * Editor.Scale,
		line_end = ListItem.Slider:GetWide() - 20 * Editor.Scale,
		line_number = math.abs( max ) + math.abs( min )
	}

	slider_paint_variables.line_gaps = slider_paint_variables.line_end / slider_paint_variables.line_number

	if slider_paint_variables.line_gaps < slider_paint_variables.line_thickness * 2 then
		slider_paint_variables.line_gaps = slider_paint_variables.line_thickness * 2
	end

	-- To prevent slowdowns
	if slider_paint_variables.line_gaps * slider_paint_variables.line_number > slider_paint_variables.line_end then
		slider_paint_variables.line_number = slider_paint_variables.line_end / slider_paint_variables.line_gaps
	end

	ListItem.Slider.Paint = function ( self, width, height )
		--[[
		surface.SetDrawColor( rng_color )
		surface.DrawRect( 0, 0, width, height )
		--]]
		surface.SetDrawColor( Editor.colours.TextGray )
		surface.DrawRect( slider_paint_variables.line_start , slider_paint_variables.line_height , slider_paint_variables.line_end, slider_paint_variables.line_thickness )

		surface.SetDrawColor( Editor.colours.TextGray )
		surface.DrawRect( slider_paint_variables.line_start , slider_paint_variables.line_height + slider_paint_variables.line_thickness * 3 , math.Round( slider_paint_variables.line_thickness * 0.5, 0 ) , slider_paint_variables.line_height * 0.3 )

		surface.SetDrawColor( Editor.colours.TextGray )
		surface.DrawRect( slider_paint_variables.line_number * slider_paint_variables.line_gaps + slider_paint_variables.line_start - 1 , slider_paint_variables.line_height + slider_paint_variables.line_thickness * 3 , math.Round( slider_paint_variables.line_thickness * 0.5, 0 ) , slider_paint_variables.line_height * 0.3 )

		for i=1, slider_paint_variables.line_number - 1 do
			surface.SetDrawColor( Editor.colours.TextGray )
			surface.DrawRect( slider_paint_variables.line_start + ( slider_paint_variables.line_gaps * i ) , slider_paint_variables.line_height + slider_paint_variables.line_thickness * 3 , math.Round( slider_paint_variables.line_thickness * 0.5, 0 ) , slider_paint_variables.line_height * 0.2 )
		end

	end
	--]]


	return ListItem

end

local function ModHoverAimingNumberSlider ( container )

	if Editor.TextSizeCorrectionInProgress then
		return false
	end

	-- Very ugly but hey, it works!

	function container.TextArea:OnCursorEntered()
		EditorExclusiveCalls.Main.SetShouldHoldAim( true )
	end
	function container.TextArea:OnCursorExited()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
	end

	function container.Slider:OnCursorEntered()
		EditorExclusiveCalls.Main.SetShouldHoldAim( true )
	end
	function container.Slider:OnCursorExited()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
	end

	function container.Slider.Knob:OnCursorEntered()
		EditorExclusiveCalls.Main.SetShouldHoldAim( true )
	end
	function container.Slider.Knob:OnCursorExited()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
	end

	function container.Label:OnCursorEntered()
		EditorExclusiveCalls.Main.SetShouldHoldAim( true )
	end
	function container.Label:OnCursorExited()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
	end

	function container.Scratch:OnCursorEntered()
		EditorExclusiveCalls.Main.SetShouldHoldAim( true )
	end
	function container.Scratch:OnCursorExited()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
	end
end

local function AddColorSelector ( container, variable, tooltip )

	local colorMix = vgui.Create( "DColorMixer", container )

	local value = string.ToColor( GetConVar( variable ):GetString() )

	if isstring( tooltip ) then
		AddTooltipPanel( colorMix, tooltip )
	end

	colorMix:SetSize( Editor.tileSize * ( 8 + Editor.elementSizeCorrection ), Editor.tileSize * 5 )
	colorMix:SetColor( value )

	function colorMix.ValueChanged()

		local newValue = string.FromColor( colorMix:GetColor() )
		RunConsoleCommand( variable, newValue )

	end

	return colorMix

end

local function AddToggle( container, conv, name, tooltip, visual_invert )

	if Editor.TextSizeCorrectionInProgress then
		TextSizeChecker( name, Editor.Font.Name, Editor.tileSize * 7 )

		return false
	end

	local elementTable = {}
	elementTable.container = vgui.Create( "DPanel", container ) -- Base
	elementTable.container:SetSize( Editor.tileSize * ( 8 + Editor.elementSizeCorrection ), Editor.tileSize )
	elementTable.container.Paint = function () end


	elementTable.Label = vgui.Create( "DLabel", elementTable.container ) -- Label
	elementTable.Label:SetColor( Editor.colours.TextDark )
	elementTable.Label:SetSize( Editor.tileSize * ( 7 + Editor.elementSizeCorrection ), Editor.tileSize )
	elementTable.Label:SetText( name )
	elementTable.Label:Dock( LEFT )
	elementTable.Label:SetFont( Editor.Font.Name )

	--[[
	elementTable.DecorImage = vgui.Create( "DImage", elementTable.container ) -- Some decor
	elementTable.DecorImage:SetKeepAspect( true )
	elementTable.DecorImage:SetSize( Editor.tileSize, Editor.tileSize )
	elementTable.DecorImage:Dock( RIGHT )
	--]]

	elementTable.Button = vgui.Create( "DButton", elementTable.container ) -- The thing that does a thing.
	elementTable.Button:SetSize( Editor.tileSize * ( 8 + Editor.elementSizeCorrection ), Editor.tileSize )

	local OutLineDrawConst = {
		math.Truncate( ( Editor.Scale * 7 ) + ( Editor.tileSize * ( 7 + Editor.elementSizeCorrection ) ) ),
		math.Truncate( 7 * Editor.Scale ),
		math.Truncate( 38 * Editor.Scale ),
		math.Truncate( 4 * Editor.Scale )
	}


	local ToggleStateDrawConst = {
		math.Truncate( math.Truncate( Editor.Scale * 3 ) + OutLineDrawConst[1] + OutLineDrawConst[4] ),
		math.Truncate( math.Truncate( Editor.Scale * 3 ) + OutLineDrawConst[2] + OutLineDrawConst[4] ),
		math.Truncate( OutLineDrawConst[3] - ( math.Truncate( Editor.Scale * 3 ) * 2 + OutLineDrawConst[4] * 2 ) )
	}

	if not isbool( visual_invert ) then
		visual_invert = false
	end

	elementTable.conVarState = false

	if GetConVar( conv ):GetBool() then
		if not visual_invert then
			elementTable.conVarState = true
		end
	else
		if visual_invert then
			elementTable.conVarState = true
		end
	end

	-- Had some consistency problems.
	--print( "OutLineDrawConst" .. "@Scale." .. Editor.Scale )
	--PrintTable( OutLineDrawConst )

	--print( "visual_invert: " .. tostring( visual_invert ) .. " elementTable.conVarState: " .. tostring( elementTable.conVarState ) .. " convar state: " .. tostring( GetConVar( conv ):GetBool() ) )

	--print( "ToggleStateDrawConst" .. "@Scale." .. Editor.Scale )
	--PrintTable( ToggleStateDrawConst )

	local truestate_color = Color( Editor.colours.Accent.r * 0.8, Editor.colours.Accent.g * 0.8, Editor.colours.Accent.b * 0.8 )
	

	elementTable.Button.Paint = function ( self, width, height )
		--surface.SetDrawColor( Color(211,106,106,162) )
		--surface.DrawRect( 0, 0, width, height )

		surface.SetDrawColor( Editor.colours.TextDark )
		surface.DrawOutlinedRect( OutLineDrawConst[1], OutLineDrawConst[2], OutLineDrawConst[3], OutLineDrawConst[3], OutLineDrawConst[4] )

		if elementTable.conVarState then
			surface.SetDrawColor( truestate_color )
			surface.DrawRect( ToggleStateDrawConst[1],  ToggleStateDrawConst[2],  ToggleStateDrawConst[3],  ToggleStateDrawConst[3] )
		end
	end

	AddTooltipPanel( elementTable.Button, tooltip )

	elementTable.Button:SetText( "" )


	function elementTable.Button:DoClick()
		--MsgC( Color( 255, 125, 255), conv .. " has changed states\n" )
		--[
		if GetConVar( conv ):GetBool() then
			GetConVar( conv ):SetInt( 0 )
			if visual_invert then
				elementTable.conVarState = true
			else
				elementTable.conVarState = false
			end
		else
			GetConVar( conv ):SetInt( 1 )
			if visual_invert then
				elementTable.conVarState = false
			else
				elementTable.conVarState = true
			end
		end
		--]]
	end

	-- Debug
	--[[
	local rng_color = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )

	elementTable.container.Paint = function ( self, width, height )
		surface.SetDrawColor( rng_color )
		surface.DrawRect( 0, 0, width, height )
	end

	local rng_color2 = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )

	elementTable.Button.Paint = function ( self, width, height )
		surface.SetDrawColor( rng_color2 )
		surface.DrawRect( 0, 0, width, height )
	end--]]
	return elementTable
end

local function AddHoverAimingToggle( container, conv, name, tooltip, default )
	local returnedData = AddToggle( container, conv, name, tooltip, default )

	if Editor.TextSizeCorrectionInProgress then
		return false
	end

	function returnedData.Button:OnCursorEntered()
		EditorExclusiveCalls.Main.SetShouldHoldAim( true )
		--MsgC( Color( 0, 255, 100 ), "ENTER THE COOKIE MONSTER\n" )
	end

	function returnedData.Button:OnCursorExited()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
		--MsgC( Color( 255, 0, 100 ), "EXIT THE COOKIE MONSTER\n" )
	end

end

local function AddButton( container, text, func, font, textColour, height, width )

	width		= width			or Editor.tileSize * ( 8 + Editor.elementSizeCorrection )
	height		= height		or Editor.tileSize
	textColour	= textColour	or Editor.colours.TextDark
	font		= font			or Editor.Font.Name

	if Editor.TextSizeCorrectionInProgress then
		TextSizeChecker( text, Editor.Font.Name, width )

		return false
	end

	local element_container = vgui.Create( "DPanel", container )

	function element_container.Paint() end
	element_container:SetSize( width, height )

	element_container.label = vgui.Create( "DLabel", element_container )

	element_container.label:SetSize( width, height )
	element_container.label:SetText( text )
	element_container.label:SetFont( font )
	element_container.label:SetTextColor( textColour )

	element_container.button = vgui.Create( "DButton", element_container )

	element_container.button:SetSize( width, height )
	element_container.button:SetText( "" )
	function element_container.button.Paint() end
	element_container.button.DoClick = func

	--element_container.label = label
	--element_container.button = button

	if Editor.debug.drawButtonBounds then
		local rand_col = ColorAlpha( ColorRand(), 128 )

		function element_container.button.Paint( self, width, height )
			surface.SetDrawColor( rand_col )
			surface.DrawRect( 0, 0, width, height )
		end
	end

	function element_container:OnClick( new_value )
		element_container.button.DoClick = new_value
	end

	function element_container:SetText( new_value )
		element_container.label:SetText( new_value )
	end

	return element_container
end

local function AddLabel( container, text, font, textColour, height )

	if Editor.TextSizeCorrectionInProgress then
		TextSizeChecker( text, font, Editor.tileSize * 8 )

		return false
	end

	local label = vgui.Create( "DLabel", container )

	label:SetText( text )
	label:SetFont( font )
	--label:Dock( docktype )
	label:SetSize( Editor.tileSize * ( 8 + Editor.elementSizeCorrection ), height )
	label:SetTextColor( textColour )

	--[[
	local rng_color = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )
	function label.Paint(self, width, height)
		surface.SetDrawColor( rng_color )
		surface.DrawRect( 0, 0, width, height )
	end
	--]]

	return label
end

local function AddKeyBinder( container, conv, name, tooltip, default )

	if Editor.TextSizeCorrectionInProgress then
		TextSizeChecker( name, Editor.Font.Name, Editor.tileSize * 6 )

		return false
	end


	local binder = {}
	binder.panel = nil
	binder.keyLabel = nil
	binder.binding_cover = {}
	--binder.keyLabel.theLabel = nil
	binder.key_value = 0
	binder.thinker = function ()
		if input.IsKeyTrapping() then

			local key_code = input.CheckKeyTrapping()

			if isnumber( key_code ) then

				binder.binding_cover.panel:Close()
				if key_code == KEY_ESCAPE then

					function binder.panel:Think() end

				else

					binder.key_value = key_code
					GetConVar( conv ):SetInt( binder.key_value )
					binder.keyLabel:SetText( language.GetPhrase( input.GetKeyName( key_code ) ) )

				end
			end
		end
	end

	local onclick = function ()
		input.StartKeyTrapping()

		--local binder.binding_cover = {}
		binder.binding_cover.panel = vgui.Create( "DFrame" )

		binder.binding_cover.panel:SetPos( 0, 0 )
		binder.binding_cover.panel:SetSize( ScrW(), ScrH() )
		binder.binding_cover.panel:SetTitle( "" )
		binder.binding_cover.panel:SetSizable( false )
		binder.binding_cover.panel:SetDraggable( false )
		binder.binding_cover.panel:SetDeleteOnClose( false )
		--binder.binding_cover.panel:ShowCloseButton( true )
		binder.binding_cover.panel:MakePopup()

		binder.binding_cover.panel.btnClose.Paint = function () end
		binder.binding_cover.panel.btnMaxim.Paint = function () end
		binder.binding_cover.panel.btnMinim.Paint = function () end

		binder.binding_cover.panel.btnClose.DoClick = function ( btn ) binder.binding_cover.panel:Remove() end
		binder.binding_cover.panel.btnMaxim.DoClick = function ( btn ) binder.binding_cover.panel:Remove() end
		binder.binding_cover.panel.btnMinim.DoClick = function ( btn ) binder.binding_cover.panel:Remove() end

		function binder.binding_cover.panel:OnClose()
			binder.binding_cover.panel:SetVisible( true )

			local timer_kill = engine.TickCount() + 7000

			function binder.binding_cover.panel:Think()
				if binder.binding_cover.panel:GetAlpha() > 0 then
					binder.binding_cover.panel:SetAlpha( binder.binding_cover.panel:GetAlpha() - 720 / ( 1 / RealFrameTime() ) )
				else
					binder.binding_cover.panel:SetVisible( false )
					binder.binding_cover.panel:Remove()
				end
				if timer_kill < engine.TickCount() then
					print( "Killing the loop" )
					binder.binding_cover.panel:SetVisible( false )
					binder.binding_cover.panel:Remove()
				end
			end
		end

		local paint_colour = Color( 0, 0, 0, 255 )

		binder.binding_cover.panel:SetAlpha( 0 )

		binder.binding_cover.helper_text = vgui.Create( "DLabel", binder.binding_cover.panel )

		binder.binding_cover.helper_text:SetSize( TextSizeChecker( "#rtp.Editor.tooltip.keyBindingHelpText", Editor.Font.DoubleName ) )
		binder.binding_cover.helper_text:SetText( "#rtp.Editor.tooltip.keyBindingHelpText" )
		binder.binding_cover.helper_text:SetFont( Editor.Font.DoubleName )
		binder.binding_cover.helper_text:SetColor( Editor.colours.TextLight )
		binder.binding_cover.helper_text:Center()


		--[[
		binder.binding_cover.helper_alive = vgui.Create( "DLabel", binder.binding_cover.panel )

		local helper_text_w, helper_text_h = binder.binding_cover.helper_text:GetSize()
		local helper_text_x, helper_text_y = binder.binding_cover.helper_text:GetPos()

		binder.binding_cover.helper_alive:SetSize( helper_text_w, helper_text_h )
		binder.binding_cover.helper_alive:SetText( "..." )
		binder.binding_cover.helper_alive:SetFont( Editor.Font.DoubleName )
		binder.binding_cover.helper_alive:SetColor( Editor.colours.TextLight )
		binder.binding_cover.helper_alive:SetPos( helper_text_x + helper_text_w, helper_text_y )
		binder.binding_cover.helper_alive.anim_panel = vgui.Create( "DPanel", binder.binding_cover.helper_alive )
		binder.binding_cover.helper_alive.anim_panel:SetSize( binder.binding_cover.helper_alive:GetSize() )


		binder.binding_cover.helper_alive.anim_panel.Paint = function ( self, width, height )
			if FrameNumber() % 10 == 0 and FrameNumber() % 3 == 0 then
				if current_width_offset >= dot_width * 3 then
					current_width_offset = 0
				else
					current_width_offset = current_width_offset + dot_width
				end
			end
			surface.SetDrawColor( paint_colour )
			surface.DrawRect( 0 + current_width_offset, 0, width, height )
		end
		--]]

		binder.binding_cover.helper_text2 = vgui.Create( "DLabel", binder.binding_cover.panel )

		binder.binding_cover.helper_text2:SetSize( TextSizeChecker( "#rtp.Editor.tooltip.keyBindingHelpText2", Editor.Font.HalfName ) )
		binder.binding_cover.helper_text2:SetText( "#rtp.Editor.tooltip.keyBindingHelpText2" )
		binder.binding_cover.helper_text2:SetFont( Editor.Font.HalfName )
		binder.binding_cover.helper_text2:SetColor( Editor.colours.TextLight )
		binder.binding_cover.helper_text2:Center()
		binder.binding_cover.helper_text2:SetY( select( 2, binder.binding_cover.helper_text:GetSize() ) + select( 2, binder.binding_cover.helper_text2:GetPos() ) + ( Editor.tileSize * 0.25 ) )


		--local current_width_offset = 0
		--local alive_string = ""


		function binder.binding_cover.panel:Think()
			if binder.binding_cover.panel:GetAlpha() < 200 then
				binder.binding_cover.panel:SetAlpha( binder.binding_cover.panel:GetAlpha() + 720 / ( 1 / RealFrameTime() ) )
			else
				function binder.binding_cover.panel:Think() end
			end
		end

		binder.binding_cover.panel.Paint = function ( self, width, height )
			--[[
			if FrameNumber() % 10 == 0 and FrameNumber() % 3 == 0 then
				if current_width_offset >= 3 then
					current_width_offset = 0
					alive_string = ""
				else
					current_width_offset = current_width_offset + 1
					alive_string = alive_string .. "."
				end
				binder.binding_cover.helper_alive:SetText( alive_string )
			end--]]
			surface.SetDrawColor( paint_colour )
			surface.DrawRect( 0, 0, width, height )
		end
		--[[
		function binder.binding_cover:Think()
			if input.IsKeyTrapping() then
				if isnumber( input.CheckKeyTrapping() ) then
					binder.binding_cover.panel:Close()
				end
			end
		end]]

		binder.panel.Think = binder.thinker
	end

	binder.panel = AddButton( container, name, onclick, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize * ( 8 + Editor.elementSizeCorrection ) )

	binder.panel.label:SetSize( Editor.tileSize * ( 6 + Editor.elementSizeCorrection ), Editor.tileSize )

	--binder.keyLabel = vgui.Create( "DPanel", binder.panel )

	binder.keyLabel = vgui.Create( "DLabel", binder.panel )

	binder.keyLabel:SetPos( Editor.tileSize * ( 6 + Editor.elementSizeCorrection ), 0 )
	binder.keyLabel:SetSize( Editor.tileSize * ( 2 + Editor.elementSizeCorrection ), Editor.tileSize )
	binder.keyLabel:SetFont( Editor.Font.HalfName )

	local key_string = "NONE"
	if conv ~= nil then
		key_string = input.GetKeyName( GetConVar( conv ):GetInt() )
	end

	if not key_string then
		key_string = "NONE"
	end

	key_string = language.GetPhrase( key_string )

	binder.keyLabel:SetText( key_string )
	binder.keyLabel:SetTextColor( Editor.colours.TextDark )

	if not tooltip == nil then
		AddTooltipPanel( binder.panel, tooltip )
	end

	return binder
end

--	UNFINSHED. DON'T USE.
local function AddCommandKeyBinder( container, conv, name, tooltip, default )

	local comBinder = AddKeyBinder( container, nil, name, tooltip, default )

	if Editor.TextSizeCorrectionInProgress then return false end

	local key_string = input.LookupBinding( "conv" )

	if not key_string then
		key_string = "NONE"
	end

	key_string = language.GetPhrase( key_string )

	comBinder.keyLabel:SetText( key_string )

	return comBinder
end

local function open_local_dropdown( caller_panel )
	local dropdown = DermaMenu( false )

	local panel_w, panel_h = caller_panel:GetSize()

	local screenspace_posx, screenspace_posy = caller_panel:LocalToScreen( 0, 0 )

	--local basepanel_posx, basepanel_posy = BehaviourEditor.PANEL:GetPos()

	if Editor.debug.consolePrint then
		print( "open_local_dropdown screenspace_pos", screenspace_posx, screenspace_posy )
	end

	dropdown:SetX( screenspace_posx + math.Truncate( 2 * Editor.Scale, 0 ) )
	dropdown:SetY( panel_h + screenspace_posy )

	dropdown:MakePopup()

	dropdown:DockPadding( math.ceil( 1 * Editor.Scale ), math.ceil( 1 * Editor.Scale ), math.ceil( 1 * Editor.Scale ), math.ceil( 1 * Editor.Scale ) )

	dropdown:SetMaxHeight( math.abs( panel_h + screenspace_posy - ScrH() ) )
	dropdown:SetMinimumWidth( panel_w )

	-- TODO: Make scroll bar visualy identical to the other scolls bars

	function dropdown.Paint( self, width, height )
		surface.SetDrawColor( Editor.colours.BaseAlt )
		surface.DrawRect( 0, 0, width, height )
	end

	return dropdown
end

local function AddDropDownOption( dropdown, name, passedfunc, icon, close_on_select )

	if not isfunction( passedfunc ) then
		return
	end

	icon = icon or "icon16/brick.png"
	close_on_select = close_on_select or true

	if close_on_select then
		RegisterDermaMenuForClose( dropdown )
	end

	local option = AddButton( dropdown, name, function()
		passedfunc()
		if close_on_select then
			CloseDermaMenus()
		end
	end )

	local margins = {
		width = math.ceil( 2 * Editor.Scale ),
		height = math.ceil( 2 * Editor.Scale )
	}

	option.margins = margins

	function option.Paint( self, width, height )
		if self.Highlight then
			surface.SetDrawColor( ColorRand() )
			surface.DrawRect( margins.width, margins.height, width - bit.lshift( margins.width, 1 ), height - bit.lshift( margins.height, 1 ) )
		end

		surface.SetDrawColor( ColorAlpha( Editor.colours.BaseAlt, 127 ) )
		surface.DrawRect( margins.width, margins.height, width - bit.lshift( margins.width, 1 ), height - bit.lshift( margins.height, 1 ) )

	end

	surface.SetFont( Editor.Font.HalfName )
	local textWidth, textHeight = surface.GetTextSize( option.label:GetText() )

	local b_margin = math.ceil( 6 * Editor.Scale , 0 )

	option:SetSize( Editor.TitleBarClear + textWidth + b_margin, Editor.TitleBarClear )
	option.button:SetSize( Editor.TitleBarClear + textWidth + b_margin, Editor.TitleBarClear )
	option.label:SetSize( textWidth, Editor.TitleBarClear )

	option.label:SetFont( Editor.Font.HalfName )
	option.label:SetPos( Editor.TitleBarClear + math.ceil( b_margin * 0.1, 0 ), 0 )

	option.icon = vgui.Create( "DImage", option )

	option.icon:SetSize( Editor.TitleBarClear - ( margins.height * 4 ), Editor.TitleBarClear - ( margins.height * 4 ) )
	option.icon:SetPos( margins.width * 2, margins.height * 2 )
	option.icon:SetKeepAspect( true )

	option.icon:SetImage( icon )

	dropdown:AddPanel( option )

	return option
end

local function OpenEditorDropDown( caller_panel )

	local editor_panel = vgui.Create( "DPanel" )
	local inner_scroll_panel = vgui.Create( "DScrollPanel", editor_panel )
	local inner_panel = vgui.Create( "DIconLayout", inner_scroll_panel )

	local panel_w, panel_h = caller_panel:GetSize()
	local screenspace_posx, screenspace_posy = caller_panel:LocalToScreen( 0, 0 )
	local cursorPos_x, cursorPos_y = input.GetCursorPos()

	editor_panel.maxPanelHeight = ScrH() * 0.5
	editor_panel.minPanelWidth = 20
	editor_panel.arrowPos = gui.MouseX()

	editor_panel.isSelfDeleting = true
	editor_panel.isYAboveTheCaller = false
	editor_panel.isArrowShown = false
	editor_panel.keepOnScreen = false

	--editor_panel.categorySelector = false

	AccessorFunc( editor_panel, "maxPanelHeight", "MaxHeight", FORCE_NUMBER )
	AccessorFunc( editor_panel, "minPanelWidth", "MinimumWidth", FORCE_NUMBER )
	AccessorFunc( editor_panel, "arrowPos", "Arrow", FORCE_NUMBER )

	AccessorFunc( editor_panel, "isSelfDeleting", "DeleteSelf", FORCE_BOOL )
	AccessorFunc( editor_panel, "isYAboveTheCaller", "YAboveTheCaller", FORCE_BOOL )
	AccessorFunc( editor_panel, "isArrowShown", "ArrowShown", FORCE_BOOL )
	AccessorFunc( editor_panel, "keepOnScreen", "KeepOnScreen", FORCE_BOOL )

	--[[
	function editor_panel:SetX( new_value )
		--editor_panel
	end
	]]

	inner_scroll_panel:Dock( FILL )
	inner_panel:Dock( FILL )

	editor_panel:SetIsMenu( true )
	editor_panel:SetDrawOnTop( true )

	RegisterDermaMenuForClose( editor_panel )

	--local basepanel_posx, basepanel_posy = BehaviourEditor.PANEL:GetPos()

	--print( screenspace_posx - basepanel_posx .. ' ' .. screenspace_posy - basepanel_posy )

	-- Just incase there's no caller_panel
	editor_panel:SetPos( cursorPos_x, cursorPos_y )

	editor_panel:SetX( screenspace_posx + math.Truncate( 2 * Editor.Scale, 0 ) )
	editor_panel:SetY( panel_h + screenspace_posy )

	editor_panel:MakePopup()

	editor_panel:DockPadding( math.ceil( 1 * Editor.Scale ), math.ceil( 1 * Editor.Scale ), math.ceil( 1 * Editor.Scale ), math.ceil( 1 * Editor.Scale ) )

	editor_panel:SetMaxHeight( math.abs( panel_h + screenspace_posy - ScrH() ) )
	editor_panel:SetMinimumWidth( panel_w )

	editor_panel:SetSize( Editor.Scale * 200, Editor.Scale * 200 )

	function editor_panel.Paint( self, width, height )

		if editor_panel.isArrowShown then
			--Editor.polyDrawData.dropDownArrow:SetNewXOffset( editor_panel.arrowPos - screenspace_posx )

			local oldstate = DisableClipping( true )

			surface.SetDrawColor( Editor.colours.BaseAlt )
			draw.NoTexture()
			surface.DrawPoly( Editor.polyDrawData.dropDownArrow[2] )

			surface.SetDrawColor( Editor.colours.TextDark )
			draw.NoTexture()
			surface.DrawPoly( Editor.polyDrawData.dropDownArrow[1] )

			DisableClipping( oldstate )

		end

		surface.SetDrawColor( Editor.colours.BaseAlt )
		surface.DrawRect( 0, 0, width, height )
	end

	function editor_panel:SetYAboveTheCaller( new_value )
		if new_value then
			editor_panel.isYAboveTheCaller = true
			self:SetY( screenspace_posy - self:GetTall() )
		else
			editor_panel.isYAboveTheCaller = false
			self:SetY( panel_h + screenspace_posy )
		end
	end

	function editor_panel:Add( added_panel )
		return inner_panel:Add( added_panel )
	end

	function editor_panel:FitWidthInPanel( panel, offset )
		local width, height = panel:GetSize()
		local screenspace_x, screenspace_y = panel:LocalToScreen( 0, 0 )

		local self_width, self_height = self:GetSize()

		self:SetSize( width, self_height )

		offset = offset or 0

		self:SetX( screenspace_x + offset )
	end

	editor_panel.inner_scroll_panel = inner_scroll_panel
	editor_panel.inner_scroll_panel.inner_panel = inner_panel

	return editor_panel

end

local function AddTabbedPanel( container )

	local base_size_x, base_size_y = container:GetSize()

	local element_container = vgui.Create( "DPanel", container )
	element_container:Dock( FILL )

	local tab_container = vgui.Create( "DIconLayout", element_container )
	tab_container:SetSize( 2 * Editor.tileSize, base_size_y )
	tab_container:Dock( RIGHT )

	--element_container:SetSize( 200, 200 )

	--[
	function element_container.Paint( self, width, height )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( 0, 0, width, height )
	end
	--]]
	--[
	function tab_container.Paint( self, width, height )
		surface.SetDrawColor( ColorRand( true ) )
		surface.DrawRect( 0, 0, width, height )
	end
	--]]

	function element_container:AddCategory( name )

	end

	return element_container
end

local function AddTextField( container, conv, name, font, textColour, tooltip, height, width )

	width		= width			or Editor.tileSize * ( 8 + Editor.elementSizeCorrection )
	height		= height		or Editor.tileSize
	textColour	= textColour	or Editor.colours.TextDark
	font		= font			or Editor.Font.Name

	if Editor.TextSizeCorrectionInProgress then
		TextSizeChecker( name, Editor.Font.Name, width )

		return false
	end

	local element_container = vgui.Create( "DPanel", container )

	function element_container.Paint() end
	element_container:SetSize( width, height )

	element_container.name = vgui.Create( "DLabel", element_container )

	element_container.name:SetSize( width/2, height )
	element_container.name:SetText( name )
	element_container.name:SetFont( font )
	element_container.name:SetTextColor( textColour )

	element_container.label = vgui.Create( "DLabel", element_container )

	element_container.label:SetSize( width/2, height )
	element_container.label:SetPos( width/2, 0 )
	element_container.label:SetText( GetConVar( conv ):GetString() )
	element_container.label:SetFont( font )
	element_container.label:SetTextColor( textColour )

	element_container.button = vgui.Create( "DButton", element_container )

	element_container.button:SetSize( width, height )
	element_container.button:SetText( "" )
	function element_container.button.Paint() end
	element_container.button.DoClick = function ()

		CloseDermaMenus()



		local text_field = vgui.Create( "DTextEntry", element_container )
		text_field:SetSize( element_container:GetSize() )
		text_field:SetValue( element_container.label:GetText() )
		text_field:SetUpdateOnType( false )
		text_field:SetMultiline( false )
		text_field:SetPaintBackground( false )
		text_field:SetFont( element_container.label:GetFont() )
		text_field:SetTextColor( element_container.label:GetTextColor() )


		text_field.OnEnter = function ()
			local current_text = text_field:GetText()
			print( current_text )
			GetConVar( conv ):SetString( current_text )
			element_container.label:SetText( current_text )
			element_container.label:SetVisible( true )
			element_container.name:SetVisible( true )
			text_field:Remove()
		end

		text_field.OnLoseFocus = function ()
			element_container.label:SetVisible( true )
			element_container.name:SetVisible( true )
			text_field:Remove()
		end

		element_container.label:SetVisible( false )
		element_container.name:SetVisible( false )

	text_field:RequestFocus()
	end

	--element_container.label = label
	--element_container.button = button

	if Editor.debug.drawButtonBounds then
		local rand_col = ColorAlpha( ColorRand(), 128 )

		function element_container.button.Paint( self, width, height )
			surface.SetDrawColor( rand_col )
			surface.DrawRect( 0, 0, width, height )
		end
	end

	function element_container:OnClick( new_value )
		element_container.button.DoClick = new_value
	end

	function element_container:SetText( new_value )
		element_container.label:SetText( new_value )
	end

	if tooltip ~= nil then
		AddTooltipPanel( element_container.button, tooltip )
	end

	return element_container

end

local function OpenPrompt( main_text, added_text, yes_func, no_func, yes_text, no_text )

	local prompt_cover = {}
	prompt_cover.panel = vgui.Create( "DFrame" )

	prompt_cover.panel:SetPos( 0, 0 )
	prompt_cover.panel:SetSize( ScrW(), ScrH() )
	prompt_cover.panel:SetTitle( "" )
	prompt_cover.panel:SetSizable( false )
	prompt_cover.panel:SetDraggable( false )
	prompt_cover.panel:SetDeleteOnClose( false )
	--prompt_cover.panel:ShowCloseButton( true )
	prompt_cover.panel:MakePopup()

	prompt_cover.panel.btnClose.Paint = function () end
	prompt_cover.panel.btnMaxim.Paint = function () end
	prompt_cover.panel.btnMinim.Paint = function () end

	prompt_cover.panel.btnClose.DoClick = function ( btn ) prompt_cover.panel:Remove() end
	prompt_cover.panel.btnMaxim.DoClick = function ( btn ) prompt_cover.panel:Remove() end
	prompt_cover.panel.btnMinim.DoClick = function ( btn ) prompt_cover.panel:Remove() end

	function prompt_cover.panel:OnClose()
		prompt_cover.panel:SetVisible( true )

		local timer_kill = engine.TickCount() + 7000

		function prompt_cover.panel:Think()
			if prompt_cover.panel:GetAlpha() > 0 then
				prompt_cover.panel:SetAlpha( prompt_cover.panel:GetAlpha() - 720 / ( 1 / RealFrameTime() ) )
			else
				prompt_cover.panel:SetVisible( false )
				prompt_cover.panel:Remove()
			end
			if timer_kill < engine.TickCount() then
				print( "Killing the loop" )
				prompt_cover.panel:SetVisible( false )
				prompt_cover.panel:Remove()
			end
		end
	end

	local function yes_run_func()
		yes_func()
		prompt_cover.panel:Close()

	end

	local function no_run_func()
		no_func()
		prompt_cover.panel:Close()
	end

	prompt_cover.button1 = AddButton( prompt_cover.panel, yes_text, yes_run_func, Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize * 4 )
	prompt_cover.button1.Paint = function ( self, width, height )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( 0,0,width,height )
	end
	prompt_cover.button1:Center()
	prompt_cover.button1.gotten = {}
	prompt_cover.button1.gotten[0], prompt_cover.button1.gotten[1] = prompt_cover.button1:GetPos()
	prompt_cover.button1:SetPos( prompt_cover.button1.gotten[0] - ( Editor.tileSize * 3 ), prompt_cover.button1.gotten[1] + ( Editor.tileSize * 2.5 ) )




	prompt_cover.button1.gotten[2], prompt_cover.button1.gotten[3] = TextSizeChecker( yes_text, Editor.Font.DoubleName )

	prompt_cover.button1.label:SetSize( prompt_cover.button1.gotten[2], Editor.tileSize )
	prompt_cover.button1.label:Center()










	prompt_cover.button2 = AddButton( prompt_cover.panel, no_text, no_run_func, Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize * 4 )
	prompt_cover.button2.Paint = function ( self, width, height )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( 0,0,width,height )
	end
	prompt_cover.button2:SetPos( prompt_cover.button1.gotten[0] + ( Editor.tileSize * 3 ), prompt_cover.button1.gotten[1] + ( Editor.tileSize * 2.5 ) )




	prompt_cover.button1.gotten[2], prompt_cover.button1.gotten[3] = TextSizeChecker( no_text, Editor.Font.DoubleName )

	prompt_cover.button2.label:SetSize( prompt_cover.button1.gotten[2] + 1, Editor.tileSize )
	prompt_cover.button2.label:Center()

	local paint_colour = Color( 0, 0, 0, 255 )

	prompt_cover.panel:SetAlpha( 0 )

	prompt_cover.helper_text = vgui.Create( "DLabel", prompt_cover.panel )

	prompt_cover.helper_text:SetSize( TextSizeChecker( main_text, Editor.Font.DoubleName ) )
	prompt_cover.helper_text:SetText( main_text )
	prompt_cover.helper_text:SetFont( Editor.Font.DoubleName )
	prompt_cover.helper_text:SetColor( Editor.colours.TextLight )
	prompt_cover.helper_text:Center()

	prompt_cover.helper_text2 = vgui.Create( "DLabel", prompt_cover.panel )

	prompt_cover.helper_text2:SetSize( TextSizeChecker( added_text, Editor.Font.HalfName ) )
	prompt_cover.helper_text2:SetText( added_text )
	prompt_cover.helper_text2:SetFont( Editor.Font.HalfName )
	prompt_cover.helper_text2:SetColor( Editor.colours.TextLight )
	prompt_cover.helper_text2:Center()
	prompt_cover.helper_text2:SetY( select( 2, prompt_cover.helper_text:GetSize() ) + select( 2, prompt_cover.helper_text2:GetPos() ) + ( Editor.tileSize * 0.25 ) )

	function prompt_cover.panel:Think()
		if prompt_cover.panel:GetAlpha() < 200 then
			prompt_cover.panel:SetAlpha( prompt_cover.panel:GetAlpha() + 720 / ( 1 / RealFrameTime() ) )
		else
			function prompt_cover.panel:Think() end
		end
	end

	prompt_cover.panel.Paint = function ( self, width, height )
		surface.SetDrawColor( paint_colour )
		surface.DrawRect( 0, 0, width, height )
	end

	function prompt_cover:SetMainText( text )
		prompt_cover.helper_text:SetSize( TextSizeChecker( text, Editor.Font.DoubleName ) )
		prompt_cover.helper_text:SetText( text )
	end

	function prompt_cover:SetAddedText( text )
		prompt_cover.helper_text2:SetSize( TextSizeChecker( text, Editor.Font.HalfName ) )
		prompt_cover.helper_text2:SetText( text )
	end

	return prompt_cover

end













local BehaviourEditor = {
	Debug = {
		dragAndDropDrawBounds = false
	},
	Strings = {},
	RibbonBar = {},
	elementList = {},
	abstact_keys_strings = {
		[0]			= "rtp.BehaviourEditor.KeyEnumeration.None",
		[1]			= "rtp.BehaviourEditor.KeyEnumeration.Attack",
		[2]			= "rtp.BehaviourEditor.KeyEnumeration.Jump",
		[4]			= "rtp.BehaviourEditor.KeyEnumeration.Duck",
		[8]			= "rtp.BehaviourEditor.KeyEnumeration.Forward",
		[16]		= "rtp.BehaviourEditor.KeyEnumeration.Back",
		[32]		= "rtp.BehaviourEditor.KeyEnumeration.Use",
		[64]		= "rtp.BehaviourEditor.KeyEnumeration.Cancel",
		[128]		= "rtp.BehaviourEditor.KeyEnumeration.Left",
		[256]		= "rtp.BehaviourEditor.KeyEnumeration.Right",
		[512]		= "rtp.BehaviourEditor.KeyEnumeration.MoveLeft",
		[1024]		= "rtp.BehaviourEditor.KeyEnumeration.MoveRight",
		[2048]		= "rtp.BehaviourEditor.KeyEnumeration.Attack2",
		[4096]		= "rtp.BehaviourEditor.KeyEnumeration.Run",
		[8192]		= "rtp.BehaviourEditor.KeyEnumeration.Reload",
		[16384]		= "rtp.BehaviourEditor.KeyEnumeration.Alt1",
		[32768]		= "rtp.BehaviourEditor.KeyEnumeration.Alt2",
		[65536]		= "rtp.BehaviourEditor.KeyEnumeration.Score",
		[131072]	= "rtp.BehaviourEditor.KeyEnumeration.Speed",
		[262144]	= "rtp.BehaviourEditor.KeyEnumeration.Walk",
		[524288]	= "rtp.BehaviourEditor.KeyEnumeration.Zoom",
		[1048576]	= "rtp.BehaviourEditor.KeyEnumeration.Weapon1",
		[2097152]	= "rtp.BehaviourEditor.KeyEnumeration.Weapon2",
		[4194304]	= "rtp.BehaviourEditor.KeyEnumeration.Bullrush",
		[8388608]	= "rtp.BehaviourEditor.KeyEnumeration.Grenade1",
		[16777216]	= "rtp.BehaviourEditor.KeyEnumeration.Grenade2"
	},
	UnLocalized_abstact_keys_strings = {
		[0]			= "None",
		[1]			= "Attack",
		[2]			= "Jump",
		[4]			= "Duck",
		[8]			= "Forward",
		[16]		= "Back",
		[32]		= "Use",
		[64]		= "Cancel",
		[128]		= "Left",
		[256]		= "Right",
		[512]		= "Move Left",
		[1024]		= "Move Right",
		[2048]		= "Attack 2",
		[4096]		= "Run",
		[8192]		= "Reload",
		[16384]		= "Alt 1",
		[32768]		= "Alt 2",
		[65536]		= "Score",
		[131072]	= "Speed",
		[262144]	= "Walk",
		[524288]	= "Zoom",
		[1048576]	= "Weapon 1",
		[2097152]	= "Weapon 2",
		[4194304]	= "Bullrush",
		[8388608]	= "Grenade 1",
		[16777216]	= "Grenade 2"
	},
	behaviourTable = {},
	Specials = {
		weapon_field_selects_base = MithralThirdPerson_Config.weapon_selects_base,
		astractKeys = MithralThirdPerson_Config.condition_has_data
	},
	MousePosDelta = {
		x = 0,
		y = 0
	},
	PanelWidthInTiles = 20,
	removeTimerAdd = 10
}

local function InitBehaviourVars()

	BehaviourEditor.RibbonBar = {}

	--BehaviourEditor.behaviourTable = EditorExclusiveCalls.Behaviour.GetTable()
	BehaviourEditor.timeStamp = -1
	BehaviourEditor.perfered_scrolldown = 0
	BehaviourEditor.perfered_scrolldown_saved_remove = 0
	BehaviourEditor.PanelWidthInTiles = 18


	BehaviourEditor.PanelWidth = ( BehaviourEditor.PanelWidthInTiles * Editor.tileSize ) + 10 + 21 * Editor.Scale
	BehaviourEditor.PanelHeight = 15 * Editor.tileSize

end

local function InitBehaviourWindow ( window )

	if window == nil then
		window = vgui.Create( "DFrame" )
		window:SetTitle( "" )
		--window:SetTitle( Editor.PanelTitle )
		window:SetVisible( true )
		--window:SetDraggable( true )
		--window:ShowCloseButton( true )
		window:MakePopup()
	end

	return window

end

local function InitBehaviourEditor()

	if IsValid( BehaviourEditor.PANEL ) then
		BehaviourEditor.PANEL:Close()
		BehaviourEditor.PANEL:Remove()
		BehaviourEditor.PANEL = nil
	end

	BehaviourEditor.PANEL = InitBehaviourWindow( BehaviourEditor.PANEL )

	InitBehaviourVars()


	BehaviourEditor.PANEL:SetSize( BehaviourEditor.PanelWidth, BehaviourEditor.PanelHeight )
	BehaviourEditor.PANEL:SetPos( ScrW() - ( ( Editor.TitleBarClear * 2 ) + BehaviourEditor.PanelWidth ), Editor.TitleBarClear * 2  )
	BehaviourEditor.PANEL:ShowCloseButton( false )
	BehaviourEditor.PANEL:SetDraggable( false ) --Editor.dragabbleWindow )

	BehaviourEditor.PANEL:DockPadding( 5, Editor.TitleBarClear, 5, 4 )

	BehaviourEditor.PANEL.Paint = function( self, width, height )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( 0, 0, width, height )
	end

	-- All of the containers are parented to this panel
	BehaviourEditor.baseContainer = vgui.Create( "DScrollPanel", BehaviourEditor.PANEL )
	BehaviourEditor.baseContainer:Dock( FILL )

	function BehaviourEditor.baseContainer.Paint()	end

	local scollBar = BehaviourEditor.baseContainer:GetVBar()
	scollBar:SetHideButtons( not Editor.scrollBarButtons )
	scollBar:SetSize( 21 * Editor.Scale, 21 * Editor.Scale )


	function scollBar:Paint( width, height )
		surface.SetDrawColor( Color(0, 0, 0, 0) )
		surface.DrawRect( 0, 0, width, height )
	end
	function scollBar.btnGrip:Paint( width, height )
		surface.SetDrawColor( Editor.colours.BaseAlt )
		surface.DrawRect( 0, 0, width, height )

		surface.SetDrawColor( Color(0, 0, 0, 45) )
		surface.DrawOutlinedRect( 0, 0, width, height, Editor.borderThickness )
	end

	if BehaviourEditor.scrollBarButtons then
		function scollBar.btnUp:Paint( width, height )
			surface.SetDrawColor( Editor.colours.BaseAlt )
			surface.DrawRect( 0, 0, width, height )

			surface.SetDrawColor( Color(0, 0, 0, 45) )
			surface.DrawOutlinedRect( 0, 0, width, height, Editor.borderThickness )

			surface.SetDrawColor( Editor.colours.TextDark )
			draw.NoTexture()
			surface.DrawPoly( Editor.polyDrawData.triangleUp )

		end
		function scollBar.btnDown:Paint( width, height )
			surface.SetDrawColor( Editor.colours.BaseAlt )
			surface.DrawRect( 0, 0, width, height )

			surface.SetDrawColor( Color(0, 0, 0, 45) )
			surface.DrawOutlinedRect( 0, 0, width, height, Editor.borderThickness )

			surface.SetDrawColor( Editor.colours.TextDark )
			draw.NoTexture()
			surface.DrawPoly( Editor.polyDrawData.triangleDown )

		end
	end

	-- The default derma controls aren't great looking, lets do our own then!
	-- This panel served one purpose but now it does two jobs:
	-- 1: cover up slider knobs that don't obey the panel clipping
	-- 2: act as a grab bar for the panel
	BehaviourEditor.PANEL.TitleBarPanel = vgui.Create( "DPanel", BehaviourEditor.PANEL )
	BehaviourEditor.PANEL.TitleBarPanel:SetPos( 0, 0 )
	BehaviourEditor.PANEL.TitleBarPanel:SetSize( BehaviourEditor.PanelWidth, Editor.TitleBarClear )
	BehaviourEditor.PANEL.TitleBarPanel.PanelDrag = false

	BehaviourEditor.PANEL.TitleBarPanel.Paint = function( self, width, height )
		surface.SetDrawColor( Editor.colours.Accent )
		surface.DrawRect( 0, 0, width, height )
	end



	BehaviourEditor.PANEL.closeButton = vgui.Create( "DButton", BehaviourEditor.PANEL )
	BehaviourEditor.PANEL.closeButton:SetText( "" )
	BehaviourEditor.PANEL.closeButton:SetSize( Editor.TitleBarClear, Editor.TitleBarClear )
	BehaviourEditor.PANEL.closeButton:SetPos( BehaviourEditor.PanelWidth - Editor.TitleBarClear, 0 )

	BehaviourEditor.PANEL.closeButton.DoClick = function ()
		EditorExclusiveCalls.Main.SetShouldHoldAim( false )
		EditorExclusiveCalls.Main.SetSupressAimingKey( false )
		BehaviourEditor.PANEL:Close()
		BehaviourEditor.PANEL:Remove()
		BehaviourEditor.PANEL = nil
	end

	BehaviourEditor.PANEL.closeButton.Paint = function ( self, width, height )
		surface.SetDrawColor( ColorAlpha( Editor.colours.Base, 127 ) )
		surface.DrawRect( 0, 0, width, height - ( height - Editor.TitleBarClear ) )

		-- The close symbol needs to be drawn in two parts
		surface.SetDrawColor( Editor.colours.TextDark )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.closeSymbol1 )

		-- because the poly formart can't be told that no connections when none is needed.
		surface.SetDrawColor( Editor.colours.TextDark )
		draw.NoTexture()
		surface.DrawPoly( Editor.polyDrawData.closeSymbol2 )
	end


	-- Title bar moving code.
	BehaviourEditor.PANEL.TitleBarPanel.OnMousePressed = function ()

		local panelPosX, panelPosY = BehaviourEditor.PANEL:GetPos()

		BehaviourEditor.MousePosDelta.x = gui.MouseX() - panelPosX
		BehaviourEditor.MousePosDelta.y = gui.MouseY() - panelPosY

		BehaviourEditor.PANEL.TitleBarPanel:MouseCapture( true )
		BehaviourEditor.PANEL.TitleBarPanel.PanelDrag = true
	end

	BehaviourEditor.PANEL.TitleBarPanel.OnMouseReleased = function ()

		BehaviourEditor.PANEL.TitleBarPanel:MouseCapture( false )
		BehaviourEditor.PANEL.TitleBarPanel.PanelDrag = false

	end

	BehaviourEditor.PANEL.Think = function ()
		if BehaviourEditor.PANEL.TitleBarPanel.PanelDrag then

			-- TODO: Add clamping

			BehaviourEditor.PANEL:SetPos( gui.MouseX() - BehaviourEditor.MousePosDelta.x, gui.MouseY() - BehaviourEditor.MousePosDelta.y )
		end
	end

end

local function ModTitleRibbonButton( ribbon_button, icon )

	icon = icon or "icon16/brick.png"

	local margins = {
		width = math.ceil( 2 * Editor.Scale ),
		height = math.ceil( 2 * Editor.Scale )
	}

	ribbon_button.margins = margins

	function ribbon_button.Paint( self, width, height )
		surface.SetDrawColor( ColorAlpha( Editor.colours.BaseAlt, 127 ) )
		surface.DrawRect( margins.width, margins.height, width - bit.lshift( margins.width, 1 ), height - bit.lshift( margins.height, 1 ) )
	end

	surface.SetFont( Editor.Font.HalfName )
	local textWidth, textHeight = surface.GetTextSize( ribbon_button.label:GetText() )

	local b_margin = math.ceil( 6 * Editor.Scale , 0 )

	ribbon_button:SetSize( Editor.TitleBarClear + textWidth + b_margin, Editor.TitleBarClear )
	ribbon_button.button:SetSize( Editor.TitleBarClear + textWidth + b_margin, Editor.TitleBarClear )
	ribbon_button.label:SetSize( textWidth, Editor.TitleBarClear )

	ribbon_button.label:SetFont( Editor.Font.HalfName )
	ribbon_button.label:SetPos( Editor.TitleBarClear + math.ceil( b_margin * 0.1, 0 ), 0 )

	ribbon_button.icon = vgui.Create( "DImage", ribbon_button )

	ribbon_button.icon:SetSize( Editor.TitleBarClear - ( margins.height * 4 ), Editor.TitleBarClear - ( margins.height * 4 ) )
	ribbon_button.icon:SetPos( margins.width * 2, margins.height * 2 )
	ribbon_button.icon:SetKeepAspect( true )

	ribbon_button.icon:SetImage( icon )

	--[[
	function ribbon_button.icon.Paint( self, width, height )
		surface.SetDrawColor( ColorAlpha( Editor.colours.Error, 127 ) )
		surface.DrawRect( margins.width, margins.height, width - bit.lshift( margins.width, 1 ), height - bit.lshift( margins.height, 1 ) )
	end
	--]]

	return ribbon_button
end

local function OpenFileSelectDropdown()

	local dropdown = open_local_dropdown( BehaviourEditor.RibbonBar.file_open_button )

	local files, dirs = file.Find( "mithral_third_person/*.json", "DATA" )

	for i=1, table.Count( files ) do

		local pass_on_func = function()
			--print( MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES .. " " .. "" .. files[i] .. "" )
			--print( MithralThirdPerson_COM_BEHAVIOUR_REFRESH )
			RunConsoleCommand( MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES, files[i] )
			--RunConsoleCommand( MithralThirdPerson_COM_BEHAVIOUR_REFRESH )
			--[[
			timer.Simple( 0.1, function()
				--print( GetConVar( MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES ):GetString() )
				--RunConsoleCommand( MithralThirdPerson_COM_BEHAVIOUR_REFRESH )
			end )--]]
		end

		AddDropDownOption( dropdown, files[i], pass_on_func, IconLocation( "/editor_behaviour/mithral_third_person_icon_file.png" ), true )
	end

end

local function AbstractKeyEnumToString( enum_table )

	if enum_table == nil or not istable( enum_table ) then
		enum_table = {0}
	end

	local key_string = ""

	--PrintTable( enum_table )

	--file.Write( "debug_out.json", util.TableToJSON( value_table.effect_data, true ) )

	for unused_var, value in pairs( enum_table ) do
		key_string = key_string .. "'" .. language.GetPhrase( BehaviourEditor.abstact_keys_strings[value] ) .. "' "
	end

	return key_string
end

local function unpack_enum_to_table( enum, maxBits, addToTable )

	local val_maxBits = 24

	maxBits = maxBits or val_maxBits

	if isbool(maxBits) then
		addToTable = maxBits
		maxBits = val_maxBits
	end

	local bitshift_accumilator = 1
	local enum_table = {}

	if enum == 0 then
		return {[0]=0}
	end

	for i=0, maxBits do
		if bit.band( enum, bitshift_accumilator ) == bitshift_accumilator then
			if addToTable then
				-- Had to add this since some older code doesn't expect gaps in numeric tables.
				table.insert( enum_table, bitshift_accumilator )
			else
				enum_table[i+1] = bitshift_accumilator
			end
		end
		bitshift_accumilator = bit.lshift( bitshift_accumilator, 1 )
	end

	return enum_table
end

local function pack_table_to_enum( enum_table, maxBits )

	maxBits = maxBits or 24

	local accumilator = 1
	local enum = 0

	PrintTable( enum_table )

	--for i=0, maxBits do
	for itter, enum_val in pairs( enum_table ) do
		--if enum_table[i+1] ~= nil then

		for i=0, maxBits do
			if bit.band( enum_val, accumilator ) == accumilator then
				enum = bit.bor( enum, accumilator )
				break
			end
			accumilator = bit.lshift( accumilator, 1 )
		end
		accumilator = 1
	end

	return enum
end

local function open_abstractkeyEditor( caller_panel, index, condition_data_edit )

	local dropdown = OpenEditorDropDown( caller_panel )
	-- Width will be changed with FitWidthInPanel anyways.
	dropdown:SetSize( 200, Editor.tileSize * 3.6 )
	dropdown:FitWidthInPanel( BehaviourEditor.baseContainer )

	condition_data_edit = condition_data_edit or false

	--dropdown:SetIsMenu( false )
	dropdown:SetDeleteSelf( false )

	local storedValue = caller_panel.current_value

	if condition_data_edit then
		storedValue = pack_table_to_enum( storedValue )
	end

	-- What a fucking mess. I wish that I could do it more cleanly.
	function BehaviourEditor.baseContainer:Think()
		if not dropdown:IsVisible() then
			if caller_panel.currentValue ~= storedValue then
				caller_panel.label:SetText( AbstractKeyEnumToString( unpack_enum_to_table( storedValue ) ) )
				caller_panel.currentValue = storedValue

				if condition_data_edit then
					BehaviourEditor.behaviourTable[index].condition_data[BehaviourEditor.behaviourTable[index].condition] = unpack_enum_to_table( storedValue, true )
				else
					BehaviourEditor.behaviourTable[index].effect_data[BehaviourEditor.behaviourTable[index].effect] = storedValue
				end

				EditorExclusiveCalls.Behaviour.ModifyRule( index, BehaviourEditor.behaviourTable[index] )

				local container_vbar = BehaviourEditor.baseContainer:GetVBar()
				BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()
			end
			--print( "DEADBEEF" )
			dropdown:Remove()

			BehaviourEditor.baseContainer.Think = nil

		end
	end

	function dropdown.Paint( self, width, height )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ) )

		surface.SetDrawColor( Editor.colours.BaseAlt )
		surface.DrawOutlinedRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ), math.Truncate( 1 * Editor.Scale ) + 1 )
	end

	local element_table = {}

	for enum_val, enum_name in pairs( BehaviourEditor.abstact_keys_strings ) do
		--print( enum_val, enum_name )
		if enum_val ~= 0 then

			element_table[enum_val] = {}
			element_table[enum_val].panel = dropdown:Add( AddButton( nil, language.GetPhrase( enum_name ), function () end, Editor.Font.Name, Editor.colours.TextDark, bit.rshift( Editor.tileSize, 1 ) , Editor.tileSize * 3 ) )


			element_table[enum_val].value_state = false

			--local value_table = unpack_enum_to_table( storedValue )

			--for unused_var, val_isthere in pairs( value_table ) do
			if bit.band( enum_val, storedValue ) == enum_val then
				element_table[enum_val].value_state = true
			end
			--end

			local function side_effect()

				if element_table[enum_val].value_state then
					element_table[enum_val].value_state = false
					storedValue = bit.bxor( storedValue, enum_val )
				else
					element_table[enum_val].value_state = true
					storedValue = bit.bor( storedValue, enum_val )
				end

				caller_panel.label:SetText( AbstractKeyEnumToString( unpack_enum_to_table( storedValue ) ) )
			end

			element_table[enum_val].panel.button.DoClick = side_effect

			element_table[enum_val].panel.Paint = function( self, width, height )
				if element_table[enum_val].value_state then
					surface.SetDrawColor( Editor.colours.Accent )
					surface.DrawRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ) )
					--[[
					surface.SetDrawColor( Editor.colours.BaseAlt )
					surface.DrawOutlinedRect( 0, 0, width , height, math.Truncate( 1 * Editor.Scale ) + 1 )
					--]]

					if element_table[enum_val].panel.label:GetColor() ~= Editor.colours.TextLight then
						element_table[enum_val].panel.label:SetColor( Editor.colours.TextLight )
					end
				else
					surface.SetDrawColor( Editor.colours.BaseAlt )
					surface.DrawOutlinedRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ), math.Truncate( 1 * Editor.Scale ) + 1 )

					if element_table[enum_val].panel.label:GetColor() ~= Editor.colours.TextDark then
						element_table[enum_val].panel.label:SetColor( Editor.colours.TextDark )
					end
				end
			end
		end
	end

	local dropdown_width, dropdown_height = dropdown:GetSize()

	local entraOptionsBPanel = dropdown:Add( vgui.Create( "DIconLayout" ) )
	entraOptionsBPanel:SetSize( dropdown_width, Editor.tileSize )
	function entraOptionsBPanel.Paint( self, width, height ) end

	local selectAllButton = AddButton( entraOptionsBPanel, language.GetPhrase( "rtp.BehaviourEditor.SelectAll" ), function () end, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize , bit.rshift( dropdown_width, 1 ) )
	local deselectAllButton = AddButton( entraOptionsBPanel, language.GetPhrase( "rtp.BehaviourEditor.DeSelectAll" ), function () end, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize , bit.rshift( dropdown_width, 1 ) )



	local function selectAllKeyEnumerations()
		for enum_val, enum_name in pairs( BehaviourEditor.abstact_keys_strings ) do
			if enum_val ~= 0 then
				element_table[enum_val].value_state = true
				storedValue = bit.bor( storedValue, enum_val )
			end
		end
		--print( storedValue )
		caller_panel.label:SetText( AbstractKeyEnumToString( unpack_enum_to_table( storedValue ) ) )
	end

	local function deselectAllKeyEnumerations()
		for enum_val, enum_name in pairs( BehaviourEditor.abstact_keys_strings ) do
			if enum_val ~= 0 then
				element_table[enum_val].value_state = false
				storedValue = 0
			end
		end
		--print( storedValue )
		caller_panel.label:SetText( AbstractKeyEnumToString( unpack_enum_to_table( storedValue ) ) )
	end

	selectAllButton:OnClick( selectAllKeyEnumerations )
	deselectAllButton:OnClick( deselectAllKeyEnumerations )

	return dropdown
end

local function open_cause_dropdown( caller_panel, index )
	local dropdown = open_local_dropdown( caller_panel )

	--print( "open" )
	--PrintTable( BehaviourEditor.behaviourTable[index] )

	local function side_effect( cond_num )

		if caller_panel.currentValue ~= cond_num then
			caller_panel.label:SetText( tostring( cond_num ) )
			caller_panel.currentValue = cond_num

			local isCommingBased = BehaviourEditor.Specials.weapon_field_selects_base[cond_num]
			local isCurrentBased = BehaviourEditor.Specials.weapon_field_selects_base[BehaviourEditor.behaviourTable[index].condition]

			if isCommingBased and not isCurrentBased then
				local weaponList = weapons.GetList()
				local weaponList_length = table.Count( weaponList )

				for itter, weapon_table in pairs( weaponList ) do

					if weapon_table.ClassName == BehaviourEditor.behaviourTable[index].weapon_name then
					
						--print( BehaviourEditor.behaviourTable[index].weapon_name )
						--print( weapon_table.Base )
						--print( weapon_table.ClassName )

						BehaviourEditor.behaviourTable[index].weapon_name_old = BehaviourEditor.behaviourTable[index].weapon_name
						BehaviourEditor.behaviourTable[index].weapon_name = weapon_table.Base
						goto endOfWeaponListLoop

					end

				end

				--print( BehaviourEditor.behaviourTable[index].weapon_name )
				--print( BehaviourEditor.behaviourTable[index].weapon_name_old )

				BehaviourEditor.behaviourTable[index].weapon_name_old = BehaviourEditor.behaviourTable[index].weapon_name
				BehaviourEditor.behaviourTable[index].weapon_name = "weapon_base"

				::endOfWeaponListLoop::

			elseif not isCommingBased and isCurrentBased then

				local swap_weapon = BehaviourEditor.behaviourTable[index].weapon_name_old

				BehaviourEditor.behaviourTable[index].weapon_name_old = BehaviourEditor.behaviourTable[index].weapon_name
				BehaviourEditor.behaviourTable[index].weapon_name = swap_weapon

				--print( BehaviourEditor.behaviourTable[index].weapon_name )
				--print( BehaviourEditor.behaviourTable[index].weapon_name_old )

			end

			BehaviourEditor.behaviourTable[index].condition = cond_num

			EditorExclusiveCalls.Behaviour.ModifyRule( index, BehaviourEditor.behaviourTable[index] )

			local container_vbar = BehaviourEditor.baseContainer:GetVBar()
			BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()
		end
	end

	for i=1, 6 do
		local menu_option = AddDropDownOption( dropdown, language.GetPhrase( "rtp.BehaviourEditor.Condition" .. i ) , function () side_effect( i ) end, IconLocation("editor_behaviour/mithral_third_person_icon_file.png"), true )
		AddTooltipPanel( menu_option, language.GetPhrase( "rtp.BehaviourEditor.tooltip.Condition" .. i ) )
	end

end

local function open_effect_dropdown( caller_panel, index )
	local dropdown = open_local_dropdown( caller_panel )

	local function side_effect( eff_num )
		if caller_panel.currentValue ~= eff_num then
			caller_panel.label:SetText( tostring( eff_num ) )
			caller_panel.currentValue = eff_num

			BehaviourEditor.behaviourTable[index].effect = eff_num

			EditorExclusiveCalls.Behaviour.ModifyRule( index, BehaviourEditor.behaviourTable[index] )

			local container_vbar = BehaviourEditor.baseContainer:GetVBar()
			BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()
		end
	end

	for i=1, 7 do
		local menu_option = AddDropDownOption( dropdown, language.GetPhrase( "rtp.BehaviourEditor.Effect" .. i ) , function () side_effect( i ) end, IconLocation("editor_behaviour/mithral_third_person_icon_file.png"), true )
		AddTooltipPanel( menu_option, language.GetPhrase( "rtp.BehaviourEditor.tooltip.Effect" .. i ) )
	end

end


local function weapon_edit_side_effect( weap_string, caller_panel, index )
	if caller_panel.currentValue ~= weap_string then
		caller_panel.label:SetText( tostring( weap_string ) )
		caller_panel.currentValue = weap_string

		BehaviourEditor.behaviourTable[index].weapon_name = weap_string

		EditorExclusiveCalls.Behaviour.ModifyRule( index, BehaviourEditor.behaviourTable[index] )

		local container_vbar = BehaviourEditor.baseContainer:GetVBar()
		BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()
	end
end

local function open_weapon_selector_dropdown( caller_panel, index, weapon_base_selector )
	local dropdown = open_local_dropdown( caller_panel )

	local weaponList = weapons.GetList()
	--PrintTable( weaponList )

	local list_to_add = {}

	if weapon_base_selector then
		table.SortByMember( weaponList, "Base", true )

		local insert_point
		for itter, weapon_table in pairs( weaponList ) do

			insert_point = table.insert( list_to_add, weapon_table.Base )

			for i=1, table.Count( list_to_add ) - 1 do
				if list_to_add[i] == weapon_table.Base then
					table.remove( list_to_add, insert_point )
					break
				end
			end
		end
	else
		table.SortByMember( weaponList, "ClassName", true )

		for itter, weapon_table in pairs( weaponList ) do
			table.insert( list_to_add, weapon_table.ClassName )
		end

		--list_to_add = weaponList
	end


	for i=1, table.Count( list_to_add ) do
		if list_to_add[i] ~= nil then
			AddDropDownOption( dropdown, list_to_add[i], function () weapon_edit_side_effect( list_to_add[i], caller_panel, index ) end, IconLocation("editor_behaviour/mithral_third_person_icon_file.png"), true )
		end
	end

end

local function open_weapon_selector_dropdown_new( caller_panel, index )
	local dropdown = OpenEditorDropDown( caller_panel )
	dropdown:SetSize( 200, Editor.tileSize * 3.6 )
	dropdown:FitWidthInPanel( BehaviourEditor.baseContainer )

	local tabbed_container = dropdown:Add( AddTabbedPanel( dropdown ) )

	function dropdown.Paint( self, width, height )
		surface.SetDrawColor( Editor.colours.Base )
		surface.DrawRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ) )

		surface.SetDrawColor( Editor.colours.BaseAlt )
		surface.DrawOutlinedRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ), math.Truncate( 1 * Editor.Scale ) + 1 )
	end


	-- Basically boilder plate code.
	local function side_effect( weap_string )
		if caller_panel.currentValue ~= weap_string then
			caller_panel.label:SetText( tostring( weap_string ) )
			caller_panel.currentValue = weap_string

			BehaviourEditor.behaviourTable[index].weapon_name = weap_string

			EditorExclusiveCalls.Behaviour.ModifyRule( index, BehaviourEditor.behaviourTable[index] )

			local container_vbar = BehaviourEditor.baseContainer:GetVBar()
			BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()
		end
	end

	local weaponList = weapons.GetList()
	local known_weapon_bases = {}
	local known_weapon_weapon_sort = {}

	table.SortByMember( weaponList, "Base", true )

	if true then
		local insert_point
		for itter, weapon_table in pairs( weaponList ) do

			insert_point = table.insert( known_weapon_bases, weapon_table.Base )

			for i=1, table.Count( known_weapon_bases ) - 1 do
				if known_weapon_bases[i] == weapon_table.Base then
					table.remove( known_weapon_bases, insert_point )
					--print( weapon_table.Base )
					break
				end
			end
		end
	end

	table.SortByMember( weaponList, "ClassName", true )

	for itter, weapon_base in ipairs( known_weapon_bases ) do
		known_weapon_weapon_sort[itter] = {}
		known_weapon_weapon_sort[itter].base = weapon_base
		for index, weapon_table in pairs( weaponList ) do
			if weapon_base == weapon_table.Base and weapon_base ~= weapon_table.ClassName then
				table.insert( known_weapon_weapon_sort[itter], weapon_table.ClassName )
			end
		end
	end
	--PrintTable( weaponList )
	PrintTable( known_weapon_bases )
	PrintTable( known_weapon_weapon_sort )

	for intter, weapon_list in pairs( known_weapon_weapon_sort ) do
		return
	end

	return dropdown

end

local function start_manual_weapon_edit( container, index )

	CloseDermaMenus()

	local text_field = vgui.Create( "DTextEntry", container )
	text_field:SetSize( container:GetSize() )
	text_field:SetValue( container.label:GetText() )
	text_field:SetUpdateOnType( false )
	text_field:SetMultiline( false )
	text_field:SetPaintBackground( false )
	text_field:SetFont( container.label:GetFont() )
	text_field:SetTextColor( container.label:GetTextColor() )
	text_field.OnEnter = function ()
		local current_text = text_field:GetText()
		print( current_text )
		weapon_edit_side_effect( current_text, container, index )
		container.label:SetText( current_text )
		container.label:SetVisible( true )
		text_field:Remove()
	end

	text_field.OnLoseFocus = function ()
		container.label:SetVisible( true )
		text_field:Remove()
	end


	container.label:SetVisible( false )

	text_field:RequestFocus()
end

local function PopulateEditor()

	local refresh_time = {}
	--refresh_time.init1 = SysTime()
	--refresh_time.init2 = SysTime()

	refresh_time.thinker1 = 0
	refresh_time.thinker2 = 1
	refresh_time.removeRule = 0
	refresh_time.removeRule_DoNothing = 0


	local container = vgui.Create( "DIconLayout", BehaviourEditor.baseContainer )
	local container_vbar = BehaviourEditor.baseContainer:GetVBar()
	local empty_func = function () end

	local function dnd_func( receiver, tableOfDroppedPanels, isDropped, menuIndex, mouseX, mouseY )

		if isDropped then
			if receiver.tablePos ~= tableOfDroppedPanels[1].tablePos then
				EditorExclusiveCalls.Behaviour.SwapRules( receiver.tablePos, tableOfDroppedPanels[1].tablePos )
			end
		end
		receiver.beingReciever = true
		tableOfDroppedPanels[1].beingDragged = true
		DebugInfo(1, tostring( receiver.tablePos .. " " .. tableOfDroppedPanels[1].tablePos ) )

	end


	container:Dock( FILL )



	local button_paint_func = function( self, width, height )
			surface.SetDrawColor( Editor.colours.BaseAlt )
			surface.DrawOutlinedRect( math.Truncate( 1 * Editor.Scale ), math.Truncate( 1 * Editor.Scale ), math.Truncate( width - ( 2 * Editor.Scale ) ), math.Truncate( height - ( 2 * Editor.Scale ) ), math.Truncate( 1 * Editor.Scale ) + 1 )
	end

	function container.Think()
		if refresh_time.removeRule ~= 0 and refresh_time.removeRule < SysTime() then
			BehaviourEditor.timeStamp = 0
			refresh_time.removeRule = 0
		end
		if BehaviourEditor.timeStamp < EditorExclusiveCalls.Behaviour.GetTimeStamp() then

			refresh_time.thinker1 = SysTime()

			BehaviourEditor.timeStamp = EditorExclusiveCalls.Behaviour.GetTimeStamp()
			BehaviourEditor.behaviourTable = EditorExclusiveCalls.Behaviour.GetTable()
			BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()
			--PrintTable( BehaviourEditor.behaviourTable )

			for index=1, table.Count( BehaviourEditor.elementList ) do
				BehaviourEditor.elementList[index]:Remove()
			end

			for i, value_table in pairs( BehaviourEditor.behaviourTable ) do

				-- Prevent making of more panels if it takes more than 1 second.
				if refresh_time.thinker1+1 < SysTime() then

					local warning_panel = vgui.Create( "DIconLayout", container )
	
					warning_panel:SetSize( Editor.tileSize * BehaviourEditor.PanelWidthInTiles, Editor.tileSize )
	
					function warning_panel.Paint( self, width, height ) end

					AddLabel( warning_panel, language.GetPhrase( "rtp.BehaviourEditor.TookTooLong" ) , Editor.Font.Name, Editor.colours.Error, Editor.tileSize )
					table.insert( BehaviourEditor.elementList, warning_panel )

					if MithralThirdPerson_debug.IsDebugEnabled() or GetConVar( "developer" ):GetBool() then
						MsgC( Color( 253, 28, 80 ), "Break on item: " .. tostring( i ) .. " \n" )
					end

					goto loopBreakout
				end

				-- Put up a warning that anything beyond won't be precessed.
				if i == GetConVar( MithralThirdPerson_Config.var_name.BEHAVIOUR_MAX_RULES ):GetInt()+1 then

					local warning_panel = vgui.Create( "DIconLayout", container )
	
					warning_panel:SetSize( Editor.tileSize * BehaviourEditor.PanelWidthInTiles, Editor.tileSize )
	
					function warning_panel.Paint( self, width, height ) end

					AddLabel( warning_panel, language.GetPhrase( "rtp.BehaviourEditor.TooManyRules" ) , Editor.Font.Name, Editor.colours.Error, Editor.tileSize )
					table.insert( BehaviourEditor.elementList, warning_panel )
					--break
				end

				-- Used for button offsets
				local element_width = 0

				local empty_values = false


				-- Might be used later
				local effect_selector
				local effect_data_field
				local condition_data_field

				-- Needed to add these seperators due to how many times I by accident changed the wrong one.
				--[[----------------------------------------+
				|											|
				|		Drag&Drop Handle					|
				|											|
				--]]----------------------------------------+

				local dnd_handle = vgui.Create( "DPanel", container )

				dnd_handle.tablePos = i
				dnd_handle.beingDragged = false
				dnd_handle.beingReciever = false
				dnd_handle:SetCursor( "sizens" )
				dnd_handle:SetSize( Editor.tileSize * BehaviourEditor.PanelWidthInTiles, Editor.tileSize )

				dnd_handle.Paint = function( self, width, height )
					if dnd_handle.PaintingDragging then
						surface.SetDrawColor( Editor.colours.Base )
						surface.DrawRect( 0, 0, width, height )
					end

					if dnd_handle.beingReciever and not dnd_handle.beingDragged and not dnd_handle.PaintingDragging then
						surface.SetDrawColor( ColorAlpha( Editor.colours.Accent, 64 ) )
						surface.DrawRect( 0, 0, width - Editor.tileSize, height )
						dnd_handle.beingReciever = false
					end
					
					if not dnd_handle.beingDragged and not dnd_handle.PaintingDragging then
						surface.SetDrawColor( Editor.colours.BaseAlt )
						surface.DrawRect( Editor.tileSize * 0.15, Editor.tileSize * 0.25, Editor.tileSize * 0.75, Editor.tileSize * 0.1 )

						surface.SetDrawColor( Editor.colours.BaseAlt )
						surface.DrawRect( Editor.tileSize * 0.15, Editor.tileSize * 0.45, Editor.tileSize * 0.75, Editor.tileSize * 0.1 )

						surface.SetDrawColor( Editor.colours.BaseAlt )
						surface.DrawRect( Editor.tileSize * 0.15, Editor.tileSize * 0.65, Editor.tileSize * 0.75, Editor.tileSize * 0.1 )
					else
						dnd_handle.beingDragged = false
					end
				end

				if BehaviourEditor.Debug.dragAndDropDrawBounds then
					local dnd_debug_bounds_colour = ColorAlpha( ColorRand(), 127 )
					dnd_handle.Paint = function( self, width, height )
						surface.SetDrawColor( dnd_debug_bounds_colour )
						surface.DrawRect( 0, 0, width, height )
					end
					
				end

				dnd_handle:Receiver( "behaviour_dnd", dnd_func,{})
				dnd_handle:Droppable( "behaviour_dnd" )

				--[[----------------------------------------+
				|											|
				|		Element  panel						|
				|											|
				--]]----------------------------------------+

				local bpanel = vgui.Create( "DIconLayout", dnd_handle )

				bpanel:SetSize( Editor.tileSize * (BehaviourEditor.PanelWidthInTiles-1) - 1, Editor.tileSize )
				bpanel:SetPos( Editor.tileSize, 0 )
				--bpanel:Dock( RIGHT )

				function bpanel.Paint() end

				--[[----------------------------------------+
				|											|
				|		Cause Selector						|
				|											|
				--]]----------------------------------------+

				if value_table.condition == nil then
					value_table.condition = 1
					empty_values = true
				end

				local cause_selector = AddButton( bpanel, tostring( value_table.condition ), empty_func, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize )

				-- Used for checking wheather or not it's needed to change the value
				cause_selector.currentValue = value_table.condition
				cause_selector.label:SetSize( TextSizeChecker( cause_selector.label:GetText(), Editor.Font.DoubleName ) )
				cause_selector.label:Center()

				AddTooltipPanel( cause_selector, language.GetPhrase( "rtp.BehaviourEditor.tooltip.ConditionButton" ) )

				cause_selector.Paint = button_paint_func

				cause_selector.button.DoClick = function ()
					open_cause_dropdown( cause_selector, i )
				end

				element_width = element_width + cause_selector:GetWide()

				--[[----------------------------------------+
				|											|
				|		Weapon Selector						|
				|											|
				--]]----------------------------------------+

				if value_table.condition ~= 0 then
					if value_table.weapon_name == nil then
						value_table.weapon_name = "nil"
						empty_values = true
					end
	
					local weapon_selector = AddButton( bpanel, tostring( value_table.weapon_name ), empty_func, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize * 4 )
	
					-- I might use localized strings for weapons names later.
					weapon_selector.currentValue = value_table.weapon_name
					--weapon_selector.label:SetSize( TextSizeChecker( weapon_selector.label:GetText(), Editor.Font.DoubleName ) )
					--weapon_selector.label:Center()
	
					weapon_selector.button.DoRightClick = function () start_manual_weapon_edit( weapon_selector, i ) end
	
					weapon_selector.Paint = button_paint_func
	
					if BehaviourEditor.Specials.weapon_field_selects_base[value_table.condition] then
						AddTooltipPanel( weapon_selector, language.GetPhrase( "rtp.BehaviourEditor.tooltip.WeaponBaseName" ) )
						weapon_selector.button.DoClick = function ()
							open_weapon_selector_dropdown( weapon_selector, i, true )
						end
					else
						AddTooltipPanel( weapon_selector, language.GetPhrase( "rtp.BehaviourEditor.tooltip.WeaponClassName" ) )
						weapon_selector.button.DoClick = function ()
							open_weapon_selector_dropdown( weapon_selector, i, false )
						end
					end
	
					--[[
					weapon_selector.button.DoClick = function ()
						open_weapon_selector_dropdown( weapon_selector, i, BehaviourEditor.Specials.weapon_field_selects_base[value_table.condition] )
					end
					--]]
	
					element_width = element_width + weapon_selector:GetWide()	
				end


				--[[----------------------------------------+
				|											|
				|		Abstracted key match field			|
				|											|
				--]]----------------------------------------+

				if value_table.condition == 2 or value_table.condition == 5 then

					if not value_table.condition_data then
						value_table.condition_data = {}
						empty_values = true
					end

					if not value_table.condition_data[value_table.condition] then
						value_table.condition_data[value_table.condition] = {1, 32}
						empty_values = true
					end

					--local key_string = AbstractKeyEnumToString( value_table.condition_data[value_table.condition] )


					--file.Write( "debug_out.json", util.TableToJSON( value_table.effect_data, true ) )

					--for unused_var, value in pairs( value_table.condition_data[value_table.condition] ) do
					--	key_string = key_string .. " " .. BehaviourEditor.abstact_keys_strings[value]
					--end

					condition_data_field = AddButton( bpanel, AbstractKeyEnumToString( value_table.condition_data[value_table.condition] ), empty_func, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize * 4 )

					condition_data_field.current_value = value_table.condition_data[value_table.condition]
					--condition_data_field.label:SetSize( TextSizeChecker( condition_data_field.label:GetText(), Editor.Font.DoubleName ) )
					--condition_data_field.label:Center()

					condition_data_field.Paint = button_paint_func

					AddTooltipPanel( condition_data_field, language.GetPhrase( "rtp.BehaviourEditor.tooltip.AsbtractKeys_condition" ) )

					condition_data_field.button.DoClick = function ()
						local field_editor = open_abstractkeyEditor( condition_data_field, i, true )
					end

					element_width = element_width + condition_data_field:GetWide()
				end

				--[[----------------------------------------+
				|											|
				|		Effect Selector						|
				|											|
				--]]----------------------------------------+

				if value_table.effect == nil then
					value_table.effects = 1
					empty_values = true
				end

				effect_selector = AddButton( bpanel, tostring( value_table.effect ), empty_func, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize )

				effect_selector.currentValue = value_table.effect
				effect_selector.label:SetSize( TextSizeChecker( effect_selector.label:GetText(), Editor.Font.DoubleName ) )
				effect_selector.label:Center()

				effect_selector.Paint = button_paint_func

				AddTooltipPanel( effect_selector, language.GetPhrase( "rtp.BehaviourEditor.tooltip.EffectButton" ) )

				effect_selector.button.DoClick = function ()
					open_effect_dropdown( effect_selector, i )
				end

				element_width = element_width + effect_selector:GetWide()

				--[[----------------------------------------+
				|											|
				|		Effect data field abstract keys		|
				|											|
				--]]----------------------------------------+

				if value_table.effect == 3 or value_table.effect == 5 then

					if not value_table.effect_data then
						value_table.effect_data = {}
						empty_values = true
					end

					if not value_table.effect_data[value_table.effect] then
						value_table.effect_data[value_table.effect] = 2048
						empty_values = true
					end

					effect_data_field = AddButton( 
						bpanel,
						AbstractKeyEnumToString( unpack_enum_to_table( value_table.effect_data[value_table.effect] ) ),
						empty_func,
						Editor.Font.Name,
						Editor.colours.TextDark,
						Editor.tileSize,
						(Editor.tileSize * (BehaviourEditor.PanelWidthInTiles-1)) - element_width - 1 - Editor.tileSize )

					effect_data_field.current_value = value_table.effect_data[value_table.effect]
					--effect_data_field.label:SetSize( TextSizeChecker( effect_selector.label:GetText(), Editor.Font.DoubleName ) )
					--effect_data_field.label:Center()

					effect_data_field.Paint = button_paint_func

					AddTooltipPanel( effect_data_field, language.GetPhrase( "rtp.BehaviourEditor.tooltip.AsbtractKeys_effect" ) )

					effect_data_field.button.DoClick = function ()
						local field_editor = open_abstractkeyEditor( effect_data_field, i )
					end

					element_width = element_width + effect_data_field:GetWide()
				end

				--[[----------------------------------------+
				|											|
				|		Effect data field fov				|
				|											|
				--]]----------------------------------------+

				if value_table.effect == 6 or value_table.effect == 7 then

					if not value_table.effect_data then
						value_table.effect_data = {}
						empty_values = true
					end

					if not value_table.effect_data[value_table.effect] then
						value_table.effect_data[value_table.effect] = 20
						empty_values = true
					end

					effect_data_field = AddNumberSlider( bpanel, nil, nil, 0, 100, nil, 0, 200 )


					effect_data_field.current_value = value_table.effect_data[value_table.effect]
					effect_data_field:SetValue( effect_data_field.current_value )


					function effect_data_field:OnValueChanged( val )
						effect_data_field.Think = function()
							if not effect_data_field:IsEditing() then
								effect_data_field.Think = nil
								--print( val, effect_data_field.current_value, value_table.effect_data[value_table.effect] )
								effect_data_field.current_value = math.Truncate(val)
								BehaviourEditor.behaviourTable[i].effect_data[value_table.effect] = math.Truncate(val)
								EditorExclusiveCalls.Behaviour.ModifyRule( i, BehaviourEditor.behaviourTable[i] )
							end
						end
					end

					element_width = element_width + effect_data_field:GetWide()
				end

				--[[
				print( element_width < Editor.tileSize * (BehaviourEditor.PanelWidthInTiles - 2) )
				if element_width < Editor.tileSize * (BehaviourEditor.PanelWidthInTiles - 2) then
					local empty_space = vgui.Create( "DPanel", bpanel )
					empty_space:SetSize( bpanel:GetWide() - element_width - Editor.tileSize - 1 )
				end
				--]]


				--[[----------------------------------------+
				|											|
				|		Remove button						|
				|											|
				--]]----------------------------------------+

				local remove_rule_button = AddButton( dnd_handle, "-", empty_func, Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize, Editor.tileSize )
				remove_rule_button:SetPos( Editor.tileSize * (BehaviourEditor.PanelWidthInTiles-1), 0 )
				remove_rule_button.label:SetSize( TextSizeChecker( "-", Editor.Font.DoubleName ) )
				remove_rule_button.label:Center()

				AddTooltipPanel( remove_rule_button , "#rtp.BehaviourEditor.RemoveRule" )

				--remove_rule_button.Paint = button_paint_func

				remove_rule_button.button.DoClick = function ()
					if refresh_time.removeRule_DoNothing > SysTime() then
						return
					end

					EditorExclusiveCalls.Behaviour.RemoveRule( i )

					if refresh_time.removeRule == 0 then
						refresh_time.removeRule = SysTime() + BehaviourEditor.removeTimerAdd
						print( refresh_time.removeRule )

						BehaviourEditor.perfered_scrolldown_saved_remove = container_vbar:GetScroll()
						BehaviourEditor.perfered_scrolldown = BehaviourEditor.perfered_scrolldown_saved_remove
						
					elseif refresh_time.removeRule > SysTime() then

						BehaviourEditor.timeStamp = EditorExclusiveCalls.Behaviour.GetTimeStamp()
						BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll()

					else
						BehaviourEditor.perfered_scrolldown = BehaviourEditor.perfered_scrolldown_saved_remove
					end

					dnd_handle:Remove()
					container_vbar:SetScroll( BehaviourEditor.perfered_scrolldown - Editor.tileSize + 1 )
				end

				if empty_values then
					EditorExclusiveCalls.Behaviour.ModifyRule( i, BehaviourEditor.behaviourTable[i] )
				end



				table.insert( BehaviourEditor.elementList, dnd_handle )

			end

			::loopBreakout::

			local add_new_rule
			local add_rule_func = function()
				EditorExclusiveCalls.Behaviour.AddRule()
				local container_vbar = BehaviourEditor.baseContainer:GetVBar()
				BehaviourEditor.perfered_scrolldown = container_vbar:GetScroll() + Editor.tileSize
			end

			add_new_rule = AddButton( container, "#rtp.BehaviourEditor.AddRule", add_rule_func, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize, container:GetWide() )

			table.insert( BehaviourEditor.elementList, add_new_rule )

			container:InvalidateLayout( true )
			--BehaviourEditor.baseContainer:ScrollToChild( container )
			BehaviourEditor.baseContainer:Rebuild()
			container_vbar:SetScroll( BehaviourEditor.perfered_scrolldown )

			BehaviourEditor.perfered_scrolldown = 0
			refresh_time.removeRule_DoNothing = SysTime() + 0.5

			if MithralThirdPerson_debug.IsDebugEnabled() or GetConVar( "developer" ):GetBool() then
				MsgC( Color( 254, 210, 239 ), "Total rules: " .. tostring( table.Count( BehaviourEditor.behaviourTable ) ) .. " \n" )
				MsgC( Color( 254, 210, 239 ), "Editor populate time: " .. tostring( (SysTime() - refresh_time.thinker1) / 1 ) .. " s\n" )
			end

		end

	end

	--refresh_time.init2 = SysTime()

	--MsgC( Color( 254, 210, 239 ), "Editor populate time: " .. tostring( refresh_time.init2 - refresh_time.init1 ) .. " s\n" )

	--BehaviourEditor.PANEL.TitleBarPanel

	--BehaviourEditor.baseContainer
end

local function PopulateEditorTitleBar()
	BehaviourEditor.RibbonBar.file_open_button = ModTitleRibbonButton( AddButton( BehaviourEditor.PANEL.TitleBarPanel, "#rtp.BehaviourEditor.OpenBehaviourJSONFile", function() OpenFileSelectDropdown() end ), IconName( "mithral_third_person_icon_folder" ) )
end

local function DrawBehaviourEditor()
	InitBehaviourEditor()
	PopulateEditorTitleBar()
	PopulateEditor()

	EditorExclusiveCalls.Main.SetShouldHoldAim( false )
	if EditorExclusiveCalls.Main.GetOpenInContextMenu() then
		EditorExclusiveCalls.Main.SetOpenInContextMenu( false )
		EditorExclusiveCalls.Main.SetCameraObstructionContextMenuOpen( true )
	end
	Editor.PANEL:Close()
	Editor.PANEL:Remove()
	Editor.PANEL = nil

end













local function InitCameraSettingsPanel ( basepanel )

	basepanel = basepanel or Editor.categories

	basepanel.CameraCategory = Editor.baseContainer:Add( "DIconLayout" )
	basepanel.CameraCategory:Dock( FILL )
	--basepanel.CameraCategory:SetPos( 0, Editor.TitleBarClear )
	--basepanel.CameraCategory:SetSize( Editor.tileSize, Editor.PanelHeight )
	basepanel.CameraCategory:SetBorder( 0 )
	basepanel.CameraCategory:SetLayoutDir( TOP )
	basepanel.CameraCategory:SetSpaceX( 0 )
	basepanel.CameraCategory:SetSpaceY( 0 )

	basepanel.CameraCategory.panelContentTable = {}

	--[[local rng_color = Color( math.random( 255 ), math.random( 255 ), math.random( 255 ), 127 )

	basepanel.CameraCategory.Paint = function ( self, width, height )
		surface.SetDrawColor( rng_color )
		surface.DrawRect( 0, 0, width, height )
	end--]]

	AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_FORWARD, "#rtp.Editor.name.camera_forward", 0, 1000 )
	AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_UP, "#rtp.Editor.name.camera_up", -50, 50 )
	AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_RIGHT, "#rtp.Editor.name.camera_right", -100, 100 )
	AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_FOV, "#rtp.Editor.name.camera_fov", 30, 100 )


	table.insert( basepanel.CameraCategory.panelContentTable, AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_AIM_FORWARD, "#rtp.Editor.name.camera_aim_forward", 0, 200, "#rtp.Editor.tooltip.camera_aim_forward" ) )
	table.insert( basepanel.CameraCategory.panelContentTable, AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_AIM_UP, "#rtp.Editor.name.camera_aim_up", -50, 50, "#rtp.Editor.tooltip.camera_aim_up" ) )
	table.insert( basepanel.CameraCategory.panelContentTable, AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_AIM_RIGHT, "#rtp.Editor.name.camera_aim_right", -100, 100, "#rtp.Editor.tooltip.camera_aim_right" ) )
	table.insert( basepanel.CameraCategory.panelContentTable, AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_AIM_FOV, "#rtp.Editor.name.camera_aim_fov", 0, 50, "#rtp.Editor.tooltip.camera_aim_fov" ) )

	if ( MithralThirdPerson_debug.IsDebugEnabled() or GetConVar( "developer" ):GetBool() or experimental ) and system.IsLinux() then
		AddHoverAimingToggle( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.EFFECT_FAKEDOF, "#rtp.Editor.name.effect_fakedof", "#rtp.Editor.tooltip.effect_fakedof" )
	end


	AddToggle( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.ALWAYS_AIM, "#rtp.Editor.name.always_aim", "#rtp.Editor.tooltip.always_aim" )

	AddKeyBinder( basepanel.CameraCategory , MithralThirdPerson_Config.var_name.PLAYER_AIMING_BUTTON, "#rtp.Editor.name.aimingKeyBinder" )
	AddToggle( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.AIMING_BUTTON_TOGGLE, "#rtp.Editor.name.aimingKeyToggle", "#rtp.Editor.tooltip.aimingKeyToggle" )

	AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_AIM_POSITION_CHANGE_SPEED, "#rtp.Editor.name.camera_aim_position_change_speed", 1, 10, "#rtp.Editor.tooltip.camera_aim_position_change_speed", 2 )
	--AddNumberSlider( basepanel.CameraCategory, MithralThirdPerson_Config.var_name.CAMERA_FALLING_OFFSET, "#rtp.Editor.name.camera_falling_offset", -15, 15, "#rtp.Editor.tooltip.camera_falling_offset" )

	ModHoverAimingNumberSlider( basepanel.CameraCategory.panelContentTable[1] )
	ModHoverAimingNumberSlider( basepanel.CameraCategory.panelContentTable[2] )
	ModHoverAimingNumberSlider( basepanel.CameraCategory.panelContentTable[3] )
	ModHoverAimingNumberSlider( basepanel.CameraCategory.panelContentTable[4] )

	table.insert( Editor.destroyList, basepanel.CameraCategory ) -- Add it for later removal.



	Editor.baseContainer:InvalidateLayout( false )

	return true

end

local function InitCrosshairPanel( basepanel )

	-- ugh... copypasta code from earlier.
	basepanel = basepanel or Editor.categories


	basepanel.CrosshairPanel = Editor.baseContainer:Add( "DIconLayout" )
	basepanel.CrosshairPanel:Dock( FILL )
	--basepanel.CrosshairPanel:SetPos( 0, Editor.TitleBarClear )
	--basepanel.CrosshairPanel:SetSize( Editor.tileSize, Editor.PanelHeight )
	basepanel.CrosshairPanel:SetBorder( 0 )
	basepanel.CrosshairPanel:SetLayoutDir( TOP )
	basepanel.CrosshairPanel:SetSpaceX( 0 )
	basepanel.CrosshairPanel:SetSpaceY( 0 )


	AddToggle( basepanel.CrosshairPanel, MithralThirdPerson_Config.var_name.CROSSHAIR_HIDDEN_IF_NOT_AIMING, "#rtp.Editor.name.crosshair_hidden_if_not_aiming", "#rtp.Editor.tooltip.crosshair_hidden_if_not_aiming", true )
	AddToggle( basepanel.CrosshairPanel, MithralThirdPerson_Config.var_name.STATIC_CROSSHAIR, "#rtp.Editor.name.static_crosshair" )
	AddColorSelector( basepanel.CrosshairPanel, MithralThirdPerson_Config.var_name.STATIC_CROSSHAIR_COLOUR, "#rtp.Editor.tooltip.static_crosshair_colour" )
	AddToggle( basepanel.CrosshairPanel, MithralThirdPerson_Config.var_name.TRACE_CROSSHAIR, "#rtp.Editor.name.trace_crosshair" )
	AddColorSelector( basepanel.CrosshairPanel, MithralThirdPerson_Config.var_name.TRACE_CROSSHAIR_COLOUR, "#rtp.Editor.tooltip.trace_crosshair_colour" )



	table.insert( Editor.destroyList, basepanel.CrosshairPanel ) -- Add it for later removal.

	Editor.baseContainer:InvalidateLayout( false )

	return true

end

local function InitMiscellaneousPanel( basepanel )

	-- ugh... copypasta code from earlier.
	basepanel = basepanel or Editor.categories

	basepanel.MiscellaneousPanel = Editor.baseContainer:Add( "DIconLayout" )
	basepanel.MiscellaneousPanel:Dock( FILL )
	--basepanel.MiscellaneousPanel:SetPos( -90, Editor.TitleBarClear )
	--basepanel.MiscellaneousPanel:SetSize( Editor.tileSize * 7, Editor.PanelHeight )
	basepanel.MiscellaneousPanel:SetBorder( 0 )
	basepanel.MiscellaneousPanel:SetLayoutDir( TOP )
	basepanel.MiscellaneousPanel:SetSpaceX( 0 )
	basepanel.MiscellaneousPanel:SetSpaceY( 0 )

	AddNumberSlider( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.AIMING_TIMER, "#rtp.Editor.name.aiming_timer", 0, 10, "#rtp.Editor.tooltip.aiming_timer" )
	AddNumberSlider( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.FIRSTPERSON_FRACTION, "#rtp.Editor.name.firstperson_fraction", 0, 10000, "#rtp.Editor.tooltip.firstperson_fraction" )
	AddNumberSlider( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.PLAYER_ROTATION_SPEED, "#rtp.Editor.name.player_rotation_speed", 0, 10, "#rtp.Editor.tooltip.player_rotation_speed" )
	AddToggle( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.FORCED_FIRSTPERSON_CONTEXT_MENU, "#rtp.Editor.name.forced_firstperson_context_menu", "#rtp.Editor.tooltip.forced_firstperson_context_menu" )
	AddToggle( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.CAMERA_ORIGIN_ON_HEAD, "#rtp.Editor.name.misc.origin_on_head", "#rtp.Editor.tooltip.origin_on_head" )
	AddToggle( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.CAMERA_DISABLE_ROTATION_WHEN_MOVE, "Disable player rotation when moving", "#rtp.Editor.tooltip.camera_disable_rotation_when_move" )

	--AddCommandKeyBinder( basepanel.MiscellaneousPanel , MithralThirdPerson_COM_INVERT_CAMERA_RIGHT_COMBINED, "#rtp.Editor.name.invertCamareRightPosKeyBinder" )
	--AddCommandKeyBinder( basepanel.MiscellaneousPanel , MithralThirdPerson_COM_TOGGLE_ADDON, "#rtp.Editor.name.addonToggleKeyBinder" )

	--AddToggle( basepanel.MiscellaneousPanel, MithralThirdPerson_Config.var_name.PRINT_BEHAVIOUR, "#rtp.Editor.name.misc.print_behaviour", "#rtp.Editor.tooltip.print_behaviour" )
	AddButton( basepanel.MiscellaneousPanel, "#rtp.Editor.name.misc.open_behaviour_editor", function () DrawBehaviourEditor() end, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize )

	local function reset_all_properties ()

		local function reset_convars()
			for key, value in pairs(MithralThirdPerson_Config.var_name) do
				GetConVar( value ):SetString( MithralThirdPerson_Config.default[key] )
			end
			Editor.PANEL:Close()
			Editor.PANEL:Remove()
			Editor.PANEL = nil
		end

		OpenPrompt( "#rtp.Editor.name.reset_prompt.text_main", "#rtp.Editor.name.reset_prompt.added_text", reset_convars, function () end, "#rtp.Editor.name.reset_prompt.button_yes", "#rtp.Editor.name.reset_prompt.button_no" )

	end

	AddButton( basepanel.MiscellaneousPanel, "#rtp.Editor.name.misc.reset_settings", reset_all_properties, Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize )



	table.insert( Editor.destroyList, basepanel.MiscellaneousPanel ) -- Add it for later removal.

	Editor.baseContainer:InvalidateLayout( false )

	return true
end

local function InitDEVPanel( basepanel )

	-- ugh... copypasta code from earlier.
	basepanel = basepanel or Editor.categories

	basepanel.DEVPanel = Editor.baseContainer:Add( "DIconLayout" )
	basepanel.DEVPanel:Dock( FILL )
	--basepanel.DEVPanel:SetPos( 0, Editor.TitleBarClear )
	--basepanel.DEVPanel:SetSize( Editor.tileSize, Editor.PanelHeight )
	basepanel.DEVPanel:SetBorder( 0 )
	basepanel.DEVPanel:SetLayoutDir( TOP )
	basepanel.DEVPanel:SetSpaceX( 0 )
	basepanel.DEVPanel:SetSpaceY( 0 )


	for key, value in pairs( MithralThirdPerson_Config.var_name ) do
		if MithralThirdPerson_Config.var_type[key].is_bool then
			AddToggle( basepanel.DEVPanel, value, key, value .. "\n" .. MithralThirdPerson_Config.con_var_help[key] .. "\nDefault: " .. MithralThirdPerson_Config.default[key] )
		elseif MithralThirdPerson_Config.var_type[key].is_number then
			AddNumberSlider( basepanel.DEVPanel, value, key, MithralThirdPerson_Config.min[key], MithralThirdPerson_Config.max[key], value .. "\n" .. MithralThirdPerson_Config.con_var_help[key] .. "\nDefault: " .. MithralThirdPerson_Config.default[key] )
		else
			AddTextField( basepanel.DEVPanel, value, key, Editor.Font.Name, Editor.colours.TextDark, value .. "\n" .. MithralThirdPerson_Config.con_var_help[key] .. "\nDefault: " .. MithralThirdPerson_Config.default[key] )
		end
	end

	--AddColorSelector( basepanel.DEVPanel, MithralThirdPerson_Config.var_name.TRACE_CROSSHAIR_COLOUR, MithralThirdPerson_Config.var_name.TRACE_CROSSHAIR_COLOUR .. "\n" )
	--AddColorSelector( basepanel.DEVPanel, MithralThirdPerson_Config.var_name.STATIC_CROSSHAIR_COLOUR, MithralThirdPerson_Config.var_name.STATIC_CROSSHAIR_COLOUR .. "\n" )


	table.insert( Editor.destroyList, basepanel.DEVPanel ) -- Add it for later removal.

	Editor.baseContainer:InvalidateLayout( false )

	return true
end

local function InitAboutPanel( basepanel )

	basepanel = basepanel or Editor.categories

	basepanel.AboutPanel = Editor.baseContainer:Add( "DIconLayout" )
	basepanel.AboutPanel:Dock( FILL )
	--basepanel.AboutPanel:SetPos( 0, Editor.TitleBarClear )
	--basepanel.AboutPanel:SetSize( Editor.tileSize, Editor.PanelHeight )
	basepanel.AboutPanel:SetBorder( 0 )
	basepanel.AboutPanel:SetLayoutDir( TOP )
	basepanel.AboutPanel:SetSpaceX( 0 )
	basepanel.AboutPanel:SetSpaceY( 0 )


	--Editor.TitleBarClear, 5, 4
	--[[
	local panel_color = table.Copy( Editor.colours.Accent )

	panel_color.a = 200

	function basepanel.AboutPanel.Paint( self, width, height )
		DisableClipping( true )
		surface.SetDrawColor( panel_color )
		surface.DrawRect( -5, 0, width + 10, Editor.PanelHeight - Editor.TitleBarClear )
		DisableClipping( false )
	end
	--]]

	basepanel.AboutPanel.elementList = {}


	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, "", Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize * 2 ))
	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, "#rtp.Editor.name.about.Title", Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize ))
	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, MithralThirdPerson_revision, Editor.Font.HalfName, Editor.colours.TextDark, Editor.tileSize * 0.25 ))
	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, "", Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize * 2 ))
	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, "#rtp.Editor.name.about.OriginRepo", Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize * 0.8 ))
	table.insert( basepanel.AboutPanel.elementList,
	AddButton( basepanel.AboutPanel, "https://github.com/artryazanov/\ngmod-rotating-third-person", function() gui.OpenURL( "https://github.com/artryazanov/gmod-rotating-third-person" ) end, Editor.Font.Name, Editor.colours.Accent, Editor.tileSize ))
	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, "", Editor.Font.DoubleName, Editor.colours.TextDark, Editor.tileSize * 0.5 ))
	table.insert( basepanel.AboutPanel.elementList,
	AddLabel( basepanel.AboutPanel, "#rtp.Editor.name.about.CurrentRepo", Editor.Font.Name, Editor.colours.TextDark, Editor.tileSize * 0.8 ))
	table.insert( basepanel.AboutPanel.elementList,
	AddButton( basepanel.AboutPanel, "https://gitlab.com/Vavency/\ngmod-rotating-third-person", function() gui.OpenURL( "https://gitlab.com/Vavency/gmod-rotating-third-person" ) end, Editor.Font.Name, Editor.colours.Accent, Editor.tileSize ))

	--PrintTable( basepanel.AboutPanel.elementList )

	table.insert( Editor.destroyList, basepanel.AboutPanel ) -- Add it for later removal.

	Editor.baseContainer:InvalidateLayout( false )

	return true

end

local function AddAddonToggle()

	local index, buttonItem = AddSideBarButton( function() end, IconLocation( "mithral_third_person_icon_addon_toggle.png" ), "Turn on and off the addon", true, true )
	local internal_convar_state = GetConVar( MithralThirdPerson_Config.var_name.ADDON_ENABLED ):GetBool()

	if internal_convar_state then
		buttonItem.color = Color( 101, 229, 156 )
	else
		buttonItem.color = Color( 255, 117, 119 )
	end

	function buttonItem.Panel.DoClick()
		GetConVar( MithralThirdPerson_Config.var_name.ADDON_ENABLED ):SetBool( not internal_convar_state )
		internal_convar_state = GetConVar( MithralThirdPerson_Config.var_name.ADDON_ENABLED ):GetBool()
	
		if internal_convar_state then
			buttonItem.color = Color( 101, 229, 156 )
		else
			buttonItem.color = Color( 255, 117, 119 )
		end
	end

end

local function DrawEditor( window )

	local editorStartTime = SysTime()

	-- First run
	InitPanelV2( window )

	AddAddonToggle()
	AddSideBarButton( function () InitCameraSettingsPanel( Editor.categories ) end, IconLocation("mithral_third_person_icon_camera.png"), "Most commonly used settings for the camera." )
	AddSideBarButton( function () InitCrosshairPanel( Editor.categories ) end, IconLocation("mithral_third_person_icon_crosshair.png"), "Change the crosshair to your liking." )
	AddSideBarButton( function () InitMiscellaneousPanel( Editor.categories ) end, IconLocation("mithral_third_person_icon_miscellaneous.png"), "Miscellaneous settings" )
	AddSideBarButton( function () InitAboutPanel( Editor.categories ) end, IconLocation("mithral_third_person_icon_info.png"), "About" )

	if MithralThirdPerson_debug.IsDebugEnabled() or GetConVar( "developer" ):GetBool() then
		AddSideBarButton( function () InitDEVPanel( Editor.categories ) end,
			"icon/mithral_third_person_icon_debug.png", "devPanel", true )
	end

	-- The way I get the width of all the elements is to init the panel first time, add all tabs then half-nuke the panel and reinit for real. Hacky but avoids the panel suddenly changing sizes.
	InitPanelV2( Editor.window, true )

	--MsgC( Color( 255, 0, 255 ), " Editor.elementSizeCorrection: " .. tostring( Editor.elementSizeCorrection ) .. "\n")

	local last_open_panel = cookie.GetNumber( Editor.cookieJar.lastPanel, 4 )
	local last_panel = 4

	if last_open_panel == 1 then

		InitCameraSettingsPanel( Editor.categories )

	elseif last_open_panel == 2 then

		InitCrosshairPanel( Editor.categories )

	elseif last_open_panel == 3 then

		InitMiscellaneousPanel( Editor.categories )

	elseif last_open_panel == 4 then

		InitAboutPanel( Editor.categories )

	else
		cookie.Set( Editor.cookieJar.lastPanel, tostring( last_panel ) )
		InitAboutPanel( Editor.categories )
	end

	if MithralThirdPerson_debug.IsDebugEnabled() or GetConVar( "developer" ):GetBool() then
		MsgC( Color( 123, 125, 250 ), "Editor populate time: " .. tostring( (SysTime() - editorStartTime) / 1 ) .. " s\n" )
	end

	--DrawBehaviourEditor()


end

list.Set( "DesktopWindows", "RotatingThirdPerson", {
	title = Editor.PanelTitle,
	icon = "icon64/mithral_third_person.png",
	width = 64,
	height = 64,
	onewindow = true,
	init = function( icon, window )
		DrawEditor( window )
	end
} )

concommand.Remove( MithralThirdPerson_COM_EDITOR_OPEN )
concommand.Add( MithralThirdPerson_COM_EDITOR_OPEN , function () DrawEditor() end )
--RunConsoleCommand( MithralThirdPerson_COM_EDITOR_OPEN )

--EditorExclusiveCalls = MithralThirdPerson_editor

return EditorExclusiveCalls