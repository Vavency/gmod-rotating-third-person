if CLIENT then

	local RTPTHINK = {}

	local runit = nil
	local blank_func = function() end

	local version = 1

	local BEHAVIOUR_VARS = {}
	BEHAVIOUR_VARS.table_safedefault = {
		{
			weapon_name = "weapon_physgun",
			condition = 2,
			condition_data = {[2] = {1,32}},
			effect = 2,
			effect_data = {[2] = {2048}}
		},
		{
			weapon_name = "weapon_physgun",
			condition = 2,
			condition_data = {[2] = {1}},
			effect = 4,
			effect_data = {}
		},
	}
	BEHAVIOUR_VARS.table = table.Copy( BEHAVIOUR_VARS.table_safedefault )
	BEHAVIOUR_VARS.table_maintenance = true
	BEHAVIOUR_VARS.lastFileModTime = 0
	BEHAVIOUR_VARS.nextFileCheck = 0
	BEHAVIOUR_VARS.tickCountStamp = 0
	BEHAVIOUR_VARS.tickCountStamp_Read = 0
	BEHAVIOUR_VARS.data_Directory = "mithral_third_person_v"..version.."/"
	BEHAVIOUR_VARS.lastFileName = ""
	BEHAVIOUR_VARS.fileSuccess = false
	BEHAVIOUR_VARS.forceFPP = false
	BEHAVIOUR_VARS.lockcamera = false
	BEHAVIOUR_VARS.zeroOutCameraTraceContents = false
	BEHAVIOUR_VARS.keypress = 0
	BEHAVIOUR_VARS.keytap = 0

	local runit_vars = {}
	runit_vars.table = {}
	runit_vars.optimized_table= {}
	runit_vars.current_weapon = ""
	runit_vars.update = 0
	runit_vars.conditions = {}
	runit_vars.effects = {}
	runit_vars.muti_statement = {}

	local repairs = {
		effect = {},
		condition = {}
	}

	--[[
	local table_generate_test = {}
	table_generate_test.max_conditions = 3
	table_generate_test.max_effects = 4
	--]]

	-- Debug
	--local MithralThirdPerson_debug = include( "includes/mithtal_third_person_debug.lua" )


	local function Behaviour_vars_reset ()
		BEHAVIOUR_VARS.forceFPP = false
		BEHAVIOUR_VARS.lockcamera = false
		BEHAVIOUR_VARS.zeroOutCameraTraceContents = false
		BEHAVIOUR_VARS.keypress = 0
		BEHAVIOUR_VARS.keytap = 0
	end

	function RTPTHINK.Behaviour_GetForceFirstPerson ()
		return BEHAVIOUR_VARS.forceFPP
	end

	function RTPTHINK.Behaviour_GetLockCamera ()
		return BEHAVIOUR_VARS.lockcamera
	end

	function RTPTHINK.Behaviour_GetHoldDownKeys ()
		return BEHAVIOUR_VARS.keypress
	end

	function RTPTHINK.Behaviour_GetZeroOutCameraTraceMask ()
		return BEHAVIOUR_VARS.zeroOutCameraTraceContents
	end

	function RTPTHINK.Behaviour_GetTapKeys ()
		return BEHAVIOUR_VARS.keytap
	end

	local function effect_lockcamera()
		BEHAVIOUR_VARS.lockcamera = true
	end

	local function effect_force_first_person()
		--DebugInfo( 1, "Setting FFP" )
		BEHAVIOUR_VARS.forceFPP = true
	end

	local function effect_hold_down_keys( values )
		--DebugInfo( 3, "Holding down keys" )
		BEHAVIOUR_VARS.keypress = bit.bor( BEHAVIOUR_VARS.keypress, values )
	end

	local function effect_tap_keys( values )
		--DebugInfo( 5, "Holding down keys" )
		BEHAVIOUR_VARS.keytap = bit.bor( BEHAVIOUR_VARS.keytap, values )
	end

	local function effect_zero_out_trace()
		--DebugInfo( 4, "Zeroing out camera trace" )
		BEHAVIOUR_VARS.zeroOutCameraTraceContents = true
	end

	local function effect_set_camera_fov( values )
		ThirdPerson.SetCameraFOV( values )
	end

	local function effect_set_aiming_camera_fov( values )
		ThirdPerson.SetCameraAimingSubtractFOV( values )
	end

	runit_vars.effects[1] = effect_force_first_person
	runit_vars.effects[2] = effect_lockcamera
	runit_vars.effects[3] = effect_hold_down_keys
	runit_vars.effects[4] = effect_zero_out_trace
	runit_vars.effects[5] = effect_tap_keys
	runit_vars.effects[6] = effect_set_camera_fov
	runit_vars.effects[7] = effect_set_aiming_camera_fov

	local function condition_true()
		return true
	end

	local function condition_weapon_keys_down( data )
		for k, v in ipairs( data ) do
			if not LocalPlayer():KeyDown(v) then
				return false
			end
		end
		return true
	end
	
	local function condition_aiming_key_down()
		return ThirdPerson.isAimingKeyDown()
	end

	runit_vars.conditions[1] = condition_true
	runit_vars.conditions[2] = condition_weapon_keys_down
	runit_vars.conditions[3] = condition_aiming_key_down
	runit_vars.conditions[4] = condition_true
	runit_vars.conditions[5] = condition_weapon_keys_down
	runit_vars.conditions[6] = condition_aiming_key_down

	--[[
	print( "Printing effect funcs" )
	PrintTable( runit_vars.effects )
	print( "Printing condition funcs" )
	PrintTable( runit_vars.conditions )
	--]]

	function RTPTHINK.GetBehaviourTable ()
		return BEHAVIOUR_VARS.table
	end

	function RTPTHINK.GetTimeStamp ()
		return BEHAVIOUR_VARS.tickCountStamp
	end

	local function WriteFreshFile ( filename )
		file.Write( filename, BEHAVIOUR_VARS.table_safedefault )
	end

	function RTPTHINK.AddRule()
		local weapon = LocalPlayer():GetActiveWeapon()
		if weapon == NULL then
			weapon = "weapon_pistol"
		else
			weapon = weapon:GetClass()
		end

		table.insert( BEHAVIOUR_VARS.table, {
			condition = 1,
			effect = 1,
			weapon_name = weapon,
			--base_name = "weapon_base",
			effect_data = {},
			condition_data = {}
		} )

		BEHAVIOUR_VARS.tickCountStamp = engine.TickCount()

	end

	function RTPTHINK.RemoveRule( index )

		table.remove( BEHAVIOUR_VARS.table, index )

		BEHAVIOUR_VARS.tickCountStamp = engine.TickCount()

	end

	function RTPTHINK.ModifyRule( index, mtable )

		-- Why did this work?
		BEHAVIOUR_VARS.table[index] = mtable

		--[[
		MsgC( Color( 181,219,255), "Recieved:\n" )
		PrintTable( mtable )
		MsgC( Color( 181,219,255), "Replaced:\n" )
		PrintTable( BEHAVIOUR_VARS.table[index] )
		--]]

		BEHAVIOUR_VARS.tickCountStamp = engine.TickCount()

	end

	function RTPTHINK.SwapRules( pos_from, pos_to )
		local table_from, table_to = table.Copy( BEHAVIOUR_VARS.table[pos_from] ), table.Copy( BEHAVIOUR_VARS.table[pos_to] )
		BEHAVIOUR_VARS.table[pos_from] = table_to
		BEHAVIOUR_VARS.table[pos_to] = table_from

		BEHAVIOUR_VARS.tickCountStamp = engine.TickCount()
	end

	local function GetWeaponBase( w )
		local t = weapons.GetStored( w )
		if t then return t.Base end
		return nil
	end

	local function OptimizeTableFromJSON()

		local startTime = SysTime()

		local processed_compile_table = {}
 
		for table_position1, single_table in pairs(BEHAVIOUR_VARS.table) do

			if table_position1 >= GetConVar( MithralThirdPerson_Config.var_name.BEHAVIOUR_MAX_RULES ):GetInt() then
				ErrorNoHalt( "Too many rules, either remove some or raise the limit with \" " .. MithralThirdPerson_Config.var_name.BEHAVIOUR_MAX_RULES .. " \" convar.\n" )
				goto endOfPreCompileLoop
			end

			--single_table.effect_data[single_table.effect] = repairs.effect[single_table.effect]( single_table.effect_data[single_table.effect] )

			if processed_compile_table[single_table.weapon_name] then
				--print( "------------------------------- " .. single_table.weapon_name )
				--PrintTable( processed_compile_table[single_table.weapon_name] )
				local add_condition = true
				local index_condition = 1
				local add_effect = true
				for k, v in ipairs( processed_compile_table[single_table.weapon_name].condition_pairs ) do
					--PrintTable( v )
					if v.condition == single_table.condition and not MithralThirdPerson_Config.condition_has_data[single_table.condition] then
						add_condition = false
					end
					for kk, vv in ipairs( v.effects ) do
						if vv.effect == single_table.effect and not MithralThirdPerson_Config.effect_has_data[single_table.effect] then
							add_effect = false
						elseif vv.effect == single_table.effect and MithralThirdPerson_Config.effect_has_data[single_table.effect] then
							-- Lazy way
							--PrintTable( single_table.effect_data )
							if table.ToString( table.Copy( vv.effect_data ) ) == table.ToString( table.Copy( single_table.effect_data[single_table.effect] ) ) then add_effect = false end
							--print( table.ToString( table.Copy( vv.effect_data ) ), table.ToString( table.Copy( single_table.effect_data[single_table.effect] ) ) )
							index_condition = k
						end
					end
				end
				--print( "addvars " ,add_condition, add_effect )

				if add_condition then
					--print( "Adding another condition" )
					table.insert( processed_compile_table[single_table.weapon_name].condition_pairs, {
						condition = single_table.condition,
						condition_data = single_table.condition_data,
						effects = {
							{
								effect = single_table.effect,
								effect_data = single_table.effect_data[single_table.effect]
							}
						}
					})
				elseif add_effect then
					--print( "Adding effect to condition" )
					table.insert( processed_compile_table[single_table.weapon_name].condition_pairs[index_condition].effects, {
						effect = single_table.effect,
						effect_data = single_table.effect_data
					} )
					--PrintTable( processed_compile_table[single_table.weapon_name].condition_pairs[index_condition].effects )
				end
			else
				processed_compile_table[single_table.weapon_name] = {
					basedon = GetWeaponBase( single_table.weapon_name ),
					condition_pairs = {
						{
							condition = single_table.condition,
							condition_data = single_table.condition_data,
							effects = {
								{
									effect = single_table.effect,
									effect_data = single_table.effect_data
								}
							}
						}
					}
				}

			end

		end

		::endOfPreCompileLoop::

		runit_vars.optimized_table = processed_compile_table
		runit_vars.update = true

	end


	local function LoadJSONAndPrepareLua()

		if not BEHAVIOUR_VARS.table_maintenance then
			if BEHAVIOUR_VARS.nextFileCheck < engine.TickCount() then
				BEHAVIOUR_VARS.nextFileCheck = engine.TickCount() + ( math.Truncate( ( 1 / engine.TickInterval() ), 0 ) * 10 )
			else
				return
			end
		end

		local filename = GetConVar( MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES ):GetString()

		--[[
		if not BEHAVIOUR_VARS.lastFileName == filename then

			BEHAVIOUR_VARS.lastFileName = filename
			BEHAVIOUR_VARS.table_maintenance = true

		end--]]

		filename = BEHAVIOUR_VARS.data_Directory .. filename

		if not file.Exists( filename, "DATA" ) then
			file.CreateDir( BEHAVIOUR_VARS.data_Directory )
			WriteFreshFile( filename )
			--return
		end


		if BEHAVIOUR_VARS.lastFileModTime < file.Time( filename, "DATA" ) then

			BEHAVIOUR_VARS.lastFileModTime = file.Time( filename, "DATA" )
			BEHAVIOUR_VARS.table_maintenance = true

		end

		if BEHAVIOUR_VARS.table_maintenance then

			BEHAVIOUR_VARS.table_maintenance = false
			-- Super crash happy if left in a looping hook

			file.AsyncRead( filename, "DATA", function( fileName, gamePath, status, data )

				if status == FSASYNC_OK then

					BEHAVIOUR_VARS.table = util.JSONToTable( data )

					BEHAVIOUR_VARS.fileSuccess = true
					BEHAVIOUR_VARS.tickCountStamp = engine.TickCount()
					BEHAVIOUR_VARS.tickCountStamp_Read = BEHAVIOUR_VARS.tickCountStamp



					if not istable( BEHAVIOUR_VARS.table ) then
						BEHAVIOUR_VARS.table = table.Copy( BEHAVIOUR_VARS.table_safedefault )
						file.Write( filename, util.TableToJSON( BEHAVIOUR_VARS.table, true ) )
						return
					end

					OptimizeTableFromJSON()

					--[[
					if MithralThirdPerson_debug.IsDebugEnabled() then
						DebugInfo(1, filename .. " loaded. OK status: " .. status)
						DebugInfo(2, " data: "  .. data )
						DebugInfo(3, " BEHAVIOUR_VARS.table: "  .. table.ToString( BEHAVIOUR_VARS.table ) )
						print( table.ToString( BEHAVIOUR_VARS.table, "BEHAVIOUR_VARS.table", true ) )
						DebugInfo(4, " Nextwindow: " .. BEHAVIOUR_VARS.nextFileCheck .. " CurrentTick: " .. engine.TickCount() .. " diff: " .. BEHAVIOUR_VARS.nextFileCheck - engine.TickCount())
					end
					--]]
				else

					BEHAVIOUR_VARS.fileSuccess = false

					local status_strings = {}
					status_strings[-8] = "ERR_NOT_MINE"
					status_strings[-7] = "ERR_RETRY_LATER"
					status_strings[-6] = "ERR_ALIGNMENT"
					status_strings[-5] = "ERR_FAILURE"
					status_strings[-4] = "ERR_READING"
					status_strings[-3] = "ERR_NOMEMORY"
					status_strings[-2] = "ERR_UNKNOWNID"
					status_strings[-1] = "ERR_FILEOPEN"
					status_strings[0] = "OK... Why are you seeing this then?"
					status_strings[1] = "STATUS_PENDING"
					status_strings[2] = "STATUS_INPROGRESS"
					status_strings[3] = "STATUS_ABORTED"
					status_strings[4] = "STATUS_UNSERVICED"


					MsgC( Color( 255,202,103), filename .. " didn't load. status:" .. tostring( status ) .. "\n" )
					MsgC( Color( 255,202,103), status_strings[status] .. "\n" )
					DebugInfo( 1, filename .. " didn't load. status:" .. tostring( status ) )


				end

			end, false )

		end

		if BEHAVIOUR_VARS.tickCountStamp > BEHAVIOUR_VARS.tickCountStamp_Read and BEHAVIOUR_VARS.fileSuccess and ( not BEHAVIOUR_VARS.table_maintenance ) then

			print( "got an update!" )

			file.Write( filename, util.TableToJSON( BEHAVIOUR_VARS.table, true ) )

			BEHAVIOUR_VARS.lastFileModTime = file.Time( filename, "DATA" )

			BEHAVIOUR_VARS.tickCountStamp_Read = BEHAVIOUR_VARS.tickCountStamp

			OptimizeTableFromJSON()

		end

	end


	-- table contains sets of condition functions and their effect if they match

	local function populate_runit( t_table )
		for k, v in ipairs( t_table.condition_pairs ) do
			--print( "---------------------" )
			--PrintTable( v )

			local key = table.insert( runit_vars.table, {} )

			runit_vars.table[key] = {}

			runit_vars.table[key].condition = runit_vars.conditions[v.condition]
			runit_vars.table[key].condition_data = v.condition_data[v.condition]
			runit_vars.table[key].effects = {}

			for kk, vv in pairs( v.effects ) do
				table.insert( runit_vars.table[key].effects, { effect = runit_vars.effects[vv.effect], effect_data = vv.effect_data[vv.effect] } )
			end
			--PrintTable( runit_vars.table[key] )
		end
		
	end
 
	local function runit()
		local activeWeapon = LocalPlayer():GetActiveWeapon()
		if activeWeapon:IsValid() then
			activeWeapon = tostring( activeWeapon:GetClass() )
		else
			return
		end

		--DebugInfo( 0, "Passed valid weapon " .. engine.TickCount() )

		if activeWeapon ~= runit_vars.current_weapon or runit_vars.update then
			--DebugInfo( 1, "Different weapon selected " .. engine.TickCount() )
			runit_vars.current_weapon = activeWeapon
			runit_vars.update = false
			runit_vars.table = {}

			local t_table = runit_vars.optimized_table[activeWeapon] or {condition_pairs = {}}
			--PrintTable( t_table )
			--print( "--------------------- ", activeWeapon )
			--PrintTable( t_table )
			populate_runit( t_table )

			t_table = runit_vars.optimized_table[GetWeaponBase( activeWeapon )] or {condition_pairs = {}}
			--print( "--------------------- ", GetWeaponBase( activeWeapon ) )
			--PrintTable( t_table )
			populate_runit( t_table )

			--print( "---------------------" )
			--PrintTable( runit_vars.table )

			--print( "---------end---------" )
		end

		--local i = 0
		for k, v in pairs( runit_vars.table ) do
			--i = i + 1
			--DebugInfo( 10 + i, tostring(v.condition( v.condition_data )) .. " " .. tostring( v.condition ) )
			if v.condition( v.condition_data ) then
				for kk, vv in pairs( v.effects ) do
					vv.effect( vv.effect_data )
				end
			end
		end
	end

	function RTPTHINK.Behaviour_Run()

		LoadJSONAndPrepareLua()
		Behaviour_vars_reset()

		runit()
	end


	local function SwitchTableMaintenance()
		timer.Simple( 0.1, function ()
			BEHAVIOUR_VARS.table_maintenance = true
			BEHAVIOUR_VARS.lastFileModTime = 0
			--print( "BEHAVIOUR_VARS.table_maintenance " .. tostring( BEHAVIOUR_VARS.table_maintenance ) )
		end )
	end

	concommand.Remove( MithralThirdPerson_COM_BEHAVIOUR_REFRESH )
	concommand.Add( MithralThirdPerson_COM_BEHAVIOUR_REFRESH, SwitchTableMaintenance )

	cvars.RemoveChangeCallback( MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES, MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES )
	cvars.AddChangeCallback( MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES, SwitchTableMaintenance, MithralThirdPerson_Config.var_name.BEHAVIOUR_RULES )

	return RTPTHINK

end