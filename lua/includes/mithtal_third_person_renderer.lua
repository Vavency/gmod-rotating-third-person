
local RENDER_R = {}

local POST_SFX = {}

POST_SFX.init = true
POST_SFX.init_W = 0
POST_SFX.init_H = 0

POST_SFX.last_frame_number = -1

POST_SFX.scale = 0.5
POST_SFX.width = ScrW() * POST_SFX.scale
POST_SFX.height = ScrH() * POST_SFX.scale

POST_SFX.sizeMode = RT_SIZE_OFFSCREEN
POST_SFX.depthMode = MATERIAL_RT_DEPTH_SHARED
POST_SFX.textureFlags = 0 -- enumeraiions for textureFlags don't exist in game acording to wiki
POST_SFX.rtFlags = 0 --CREATERENDERTARGETFLAGS_HDR
POST_SFX.imageFormat = IMAGE_FORMAT_RGBA8888

POST_SFX.tick_PostProcess = 0

-- Init before it gets removed split second later
local FLASHLIGHT_VARS = {
	projection = ProjectedTexture(),
	isFlashlightOn = false,
	flashlight_check = false,
	next_frame_removal = false,
	lightValue = 0,
	desired_distance = 0,
	desired_samplePos = 0,
	traceFractionDistance = 0,
	brightness = 1.25
}

local playerSize

-- Debug
local MithralThirdPerson_debug = include( "includes/mithtal_third_person_debug.lua" )

local function Texture_Init ()
	if POST_SFX.init then --or POST_SFX.init_W ~= ScrW() or POST_SFX.init_H ~= ScrH() then
		MithralThirdPerson_debug.draw_text( "'Texture_Init()' has ran more than once!", 4)

		POST_SFX.init_W = ScrW()
		POST_SFX.init_H = ScrH()

		POST_SFX.scale = 0.5
		POST_SFX.width = ScrW() * POST_SFX.scale
		POST_SFX.height = ScrH() * POST_SFX.scale

		POST_SFX.target_texture = GetRenderTargetEx( "RotatingThirdPerson_RenderScreenspaceEffects_fakedof_target", POST_SFX.width, POST_SFX.height, POST_SFX.sizeMode, POST_SFX.depthMode, POST_SFX.textureFlags, 	POST_SFX.rtFlags, POST_SFX.imageFormat )

		if POST_SFX.init then

			POST_SFX.material = CreateMaterial( "RotatingThirdPerson_RenderScreenspaceEffects_fakedof_material", "UnlitGeneric", { ["$basetexture"] = POST_SFX.target_texture:GetName(), ["$translucent"] = "1" } )

			POST_SFX.effect_material = Material( "pp/bokehblur" )

			POST_SFX.overridetexture = GetRenderTargetEx( "RotatingThirdPerson_RenderScreenspaceEffects_fakedof_overridetexture", 1, 1, POST_SFX.sizeMode, MATERIAL_RT_DEPTH_NONE, POST_SFX.textureFlags, POST_SFX.rtFlags, POST_SFX.imageFormat )

			POST_SFX.overrideMaterial = (CreateMaterial( "RotatingThirdPerson_RenderScreenspaceEffects_fakedof_overridematerial", "UnlitGeneric", { ["$basetexture"] = POST_SFX.overridetexture:GetName() }))

			POST_SFX.effect_material:SetTexture( "$BASETEXTURE", render.GetScreenEffectTexture() )
			POST_SFX.effect_material:SetTexture( "$DEPTHTEXTURE", POST_SFX.target_texture  )

			render.PushRenderTarget( POST_SFX.overridetexture )
				render.Clear(255, 0, 0, 255)
			render.PopRenderTarget()

			POST_SFX.effect_material:SetFloat( "$size", 3 )
			POST_SFX.effect_material:SetFloat( "$focus", 0 )
			POST_SFX.effect_material:SetFloat( "$focusradius", 1 )

			POST_SFX.init = false
		end
	end
end

local function drawModelsChildren ( ent )

	local ent_table = ent:GetChildren()

	for i=1, table.Count( ent_table ) do

		if not table.IsEmpty( ent_table[i]:GetChildren() ) then
			drawModelsChildren( ent_table[i] )
		end

		if ent_table[i] == nil then
			goto RenderSkip
		end

		--if ent_table[i]:IsEffectActive( EF_BONEMERGE ) then
		--	goto RenderSkip
		--end

		if ent_table[i]:IsEffectActive( EF_NODRAW ) then
			goto RenderSkip
		end

		if ent_table[i] == game.GetWorld() then
			goto RenderSkip
		end

		if ent_table[i]:GetModel() == nil then
			goto RenderSkip
		end

		ent_table[i]:DrawModel()

		::RenderSkip::

	end

end

-- Super expensive and some times doesn't provide good results.
function RENDER_R.Render_FakeDOF ( IsAiming )

	Texture_Init()

	--[[
	if POST_SFX.last_frame_number ~= FrameNumber() then
		POST_SFX.last_frame_number = FrameNumber()
	else
		return
	end--]]

	--TODO: USE STENCILS FOR THIS
	--Why: Since I don't care about bluring anything but the player.
	if IsAiming and GetConVar( MithralThirdPerson_Config.var_name.EFFECT_FAKEDOF ):GetBool() then
	--if true then

		--[[
		MithralThirdPerson_debug.draw_text(  "ThirdPerson.PostDrawTranslucentRenderables: ", 4 )
		MithralThirdPerson_debug.draw_text(  "	POST_SFX.width: " .. tostring( POST_SFX.width ) .. " ScrW(): " .. tostring( ScrW() ) )
		MithralThirdPerson_debug.draw_text(  "	POST_SFX.height: " .. tostring( POST_SFX.height ) .. " ScrH(): " .. tostring( ScrH() ) )
		MithralThirdPerson_debug.draw_text(  "	POST_SFX.target_texture:Height(): " .. POST_SFX.target_texture:Height() )
		MithralThirdPerson_debug.draw_text(  "	POST_SFX.target_texture:Width(): " .. POST_SFX.target_texture:Width() )
		MithralThirdPerson_debug.draw_text(  "	POST_SFX.overridetexture:Height(): " .. POST_SFX.overridetexture:Height() )
		MithralThirdPerson_debug.draw_text(  "	POST_SFX.overridetexture:Width(): " .. POST_SFX.overridetexture:Width() )
		--]]

		local ply = LocalPlayer()

		render.UpdateScreenEffectTexture()
		--render.UpdateFullScreenDepthTexture()
		--render.PushRenderTarget( render.GetFullScreenDepthTexture()  )
		render.PushRenderTarget( POST_SFX.target_texture  )
			--cam.Start3D(cameraOrigin, cameraAngles_viewpunched, cameraFOV, 0, 0, POST_SFX.width, POST_SFX.height)
			cam.Start3D()
				render.SuppressEngineLighting(true)

				render.Clear( 0, 0, 0, 255, true )
				render.ModelMaterialOverride( POST_SFX.overrideMaterial )

				ply:DrawModel()
				drawModelsChildren( ply )


				if pac.RenderOverride then
					pac.FlashlightDisable(true)
			
					pac.ShowEntityParts(ply)
					pac.ForceRendering(true)
			
					pac.SetupBones(ply)
			
					pac.RenderOverride(ply, "opaque")
					pac.RenderOverride(ply, "translucent")
			
					pac.ForceRendering(false)
					pac.FlashlightDisable(false)
				end

				render.BlurRenderTarget( POST_SFX.target_texture , 1, 1, 1 )

				render.ModelMaterialOverride( nil )
				render.SuppressEngineLighting( false )
			cam.End3D()
		render.PopRenderTarget()

		if GetConVar( "developer" ):GetBool() and MithralThirdPerson_debug.IsDebugEnabled() then
			MithralThirdPerson_debug.draw_text(  "ThirdPerson.PostDrawTranslucentRenderables: ", 4 )
			--render.SetMaterial( CreateMaterial( "MithralThirdPerson_RenderScreenspaceEffects_fakedof_material", "UnlitGeneric", { ["$basetexture"] = POST_SFX.target_texture } ) )
			--render.DrawScreenQuadEx( bit.rshift( ScrW(), 1 ), bit.rshift( ScrH(), 1 ) , bit.rshift( ScrW(), 1 ) , bit.rshift( ScrH(), 1 ) )
			render.DrawTextureToScreen( POST_SFX.target_texture  )
		end

		render.SetMaterial( POST_SFX.effect_material )
		--render.DrawScreenQuadEx( bit.rshift( ScrW(), 1 ), bit.rshift( ScrH(), 1 ) , bit.rshift( ScrW(), 1 ) , bit.rshift( ScrH(), 1 ) )
		render.DrawScreenQuad()


	end
end

-- Dead code?
local function update_fl_bright( projected_texture, light_brightness, light_sample )
	-- 'Desired birghtness' - ('currently read lightlevel' * 'Desired birghtness')
	local avgbrightness = (light_brightness -
							(light_sample.x * light_brightness)) +
							(light_brightness - (light_sample.y * light_brightness)) +
							(light_brightness - (light_sample.z * light_brightness)
						  ) / 3

	return math.Approach(projected_texture:GetBrightness(), avgbrightness, RealFrameTime() * 10)
end

local function ThirdPerson_Flashlight_Update ( projected_texture, light_end_posistion, farz_distance, light_brightness, light_sample, v_table )

	--FLASHLIGHT_VARS.projection:SetEnableShadows( ThirdPerson.Behaviour.ForceFirstPerson() )
	--projected_texture:SetEnableShadows( true )
	projected_texture:SetFOV( GetConVar( "r_flashlightfov" ):GetInt() )

	-- Lets use a "good enough" method
	local light_origin = v_table.campos - ( v_table.trace_result.Normal * 16 )
	if not v_table.isFirstPersonForced then
		projected_texture:SetAngles( ( v_table.trace_result.Normal ):Angle() )
	else
		projected_texture:SetAngles( EyeAngles() )
	end
	--projected_texture:SetBrightness( update_fl_bright( projected_texture, light_brightness, light_sample ) )
	projected_texture:SetBrightness( 1.5 )
	projected_texture:SetFarZ( farz_distance )
	projected_texture:SetNearZ( 24 )
	projected_texture:SetPos( light_origin )
	projected_texture:Update()
end

function RENDER_R.ThirdPerson_Flashlight_Cleanup ( projected_texture, light_end_posistion, farz_distance, light_brightness,light_sample)
	projected_texture:SetPos( Vector( 32768, 32768 , 32768 ) )
	projected_texture:SetAngles( Angle( 180, 0 , 0 ) )
	--projected_texture:SetEnableShadows( true )
	projected_texture:SetFOV( 10 )
	--projected_texture:Remove()
	FLASHLIGHT_VARS.next_frame_removal = true
end

function RENDER_R.Remove()

	if FLASHLIGHT_VARS.next_frame_removal and FLASHLIGHT_VARS.projection:IsValid() then
		FLASHLIGHT_VARS.projection:Remove()
		FLASHLIGHT_VARS.next_frame_removal = false
	end

	RENDER_R.ThirdPerson_Flashlight_Cleanup( FLASHLIGHT_VARS.projection )
end

function RENDER_R.return_flashlight_check ()
	return FLASHLIGHT_VARS.flashlight_check
end

function RENDER_R.ThirdPerson_Flashlight ( v_table )


	if POST_SFX.tick_PostProcess ~= FrameNumber() then
		POST_SFX.tick_PostProcess = FrameNumber()
	else
		return -- v_table.oncheck
	end

	if FLASHLIGHT_VARS.next_frame_removal and FLASHLIGHT_VARS.projection:IsValid() then
		FLASHLIGHT_VARS.projection:Remove()
		FLASHLIGHT_VARS.next_frame_removal = false
	end

	if LocalPlayer():FlashlightIsOn() and FLASHLIGHT_VARS.projection:IsValid() then
		FLASHLIGHT_VARS.projection:Remove()
		FLASHLIGHT_VARS.next_frame_removal = false
	end

	--[[
	MithralThirdPerson_debug.draw_text( "MithralThirdPerson_renderer::ThirdPerson_Flashlight" )
	MithralThirdPerson_debug.draw_text( " v_table.campos: " .. table.ToString( Vector(v_table.trace_result.StartPos):ToTable() ) .. " v_table.angle: " .. table.ToString( Angle(v_table.trace_result.ameraAngles ):ToTable() ) )
	MithralThirdPerson_debug.draw_text( " v_table.fraction: " .. tostring( v_table.trace_result.Fraction ) .. " v_table.hitpos: " .. table.ToString( Vector(v_table.trace_result.HitPos):ToTable() ) )
	MithralThirdPerson_debug.draw_text( " v_table.last_tick: " .. tostring( v_table.last_tick ) .. " v_table.oncheck: " .. tostring( v_table.oncheck ) )
	MithralThirdPerson_debug.draw_text( " v_table.last_tick: " .. tostring( v_table.last_tick ) .. " FLASHLIGHT_VARS.isFlashlightOn: " .. tostring( FLASHLIGHT_VARS.isFlashlightOn ) )
	MithralThirdPerson_debug.draw_text( " FLASHLIGHT_VARS.projection:GetPos(): " .. table.ToString( Vector(FLASHLIGHT_VARS.projection:GetPos()):ToTable() ) )
	--]]

	if v_table.last_tick and not FLASHLIGHT_VARS.isFlashlightOn then
		FLASHLIGHT_VARS.isFlashlightOn = true
		FLASHLIGHT_VARS.flashlight_check = false
	elseif v_table.last_tick and FLASHLIGHT_VARS.isFlashlightOn then
		FLASHLIGHT_VARS.isFlashlightOn = false
		FLASHLIGHT_VARS.flashlight_check = false
	end

	if not FLASHLIGHT_VARS.isFlashlightOn then
		RENDER_R.ThirdPerson_Flashlight_Cleanup( FLASHLIGHT_VARS.projection )
		return -- v_table.oncheck
	end

	if not LocalPlayer():FlashlightIsOn() then


		-- This function gets called mutiple times and I don't want to do another trace needlessly. Solution? Use cameraTraceResult.Fraction to get total disatance treversed
		FLASHLIGHT_VARS.desired_distance = GetConVar( "r_flashlightfar" ):GetInt() + v_table.campos_rel.x
		--FLASHLIGHT_VARS.desired_samplePos = v_table.trace_result.StartPos + v_table.trace_result.cameraAngles:Forward() * FLASHLIGHT_VARS.desired_distance
		--FLASHLIGHT_VARS.traceFractionDistance = 32768 * v_table.trace_result.Fraction

		--[[
		if FLASHLIGHT_VARS.traceFractionDistance > FLASHLIGHT_VARS.desired_distance then
			FLASHLIGHT_VARS.lightValue = render.GetLightColor( FLASHLIGHT_VARS.desired_samplePos )
		else
			FLASHLIGHT_VARS.lightValue = render.GetLightColor( v_table.trace_result.HitPos )
		end
		--]]

		if not FLASHLIGHT_VARS.projection:IsValid() then
			FLASHLIGHT_VARS.projection = ProjectedTexture() -- Bring it back from the dead.
			FLASHLIGHT_VARS.projection:SetBrightness( 0 )
			FLASHLIGHT_VARS.projection:SetColor( { 255, 255, 255 } )
			FLASHLIGHT_VARS.projection:SetNearZ( 16 )
			--FLASHLIGHT_VARS.projection:SetTexture( "effects/flashlight001" )
			FLASHLIGHT_VARS.projection:SetTexture( "effects/flashlight/soft" )
		end

		ThirdPerson_Flashlight_Update(	FLASHLIGHT_VARS.projection,
										FLASHLIGHT_VARS.desired_samplePos,
										FLASHLIGHT_VARS.desired_distance,
										FLASHLIGHT_VARS.brightness,
										FLASHLIGHT_VARS.lightValue,
										v_table
									)
	else
		RENDER_R.ThirdPerson_Flashlight_Cleanup( FLASHLIGHT_VARS.projection )
	end

end


return RENDER_R
