MithralThirdPerson_Config = {
	default = {},
	var_name = {},
	con_var_help = {},
	var_type = {},
	min = {},
	max = {},
	current_value_str = {},
	--current_value_int = {},
	--current_value_fp = {},
	--vector = {},
	--current_value_bool = {},
	current_value_color = {},
	condition_has_data = { [2] = true, [5] = true }, -- Used so far by behaviour editor and ConstructLuaFromJSON()
	effect_has_data = { [3] = true, [5] = true, [6] = true, [7] = true },
	weapon_selects_base = { [4] = true ,[5] = true , [6] = true },
	behaviour_run_once_optimization = false
}

local function add_console_variable( key, var_name, default, help_text, var_type, min, max )
	local created_convar

	MithralThirdPerson_Config.var_name[key] = var_name
	MithralThirdPerson_Config.default[key] = default

	MithralThirdPerson_Config.con_var_help[key] = help_text or "undefined the help text."

	MithralThirdPerson_Config.var_type[key] = var_type or { is_bool = false, is_number = false, is_colour = false }
	MithralThirdPerson_Config.var_type[key].is_number = MithralThirdPerson_Config.var_type[key].is_number or false
	MithralThirdPerson_Config.var_type[key].is_bool = MithralThirdPerson_Config.var_type[key].is_bool or false
	MithralThirdPerson_Config.var_type[key].is_colour = MithralThirdPerson_Config.var_type[key].is_colour or false

	if MithralThirdPerson_Config.var_type[key].is_bool then
		MithralThirdPerson_Config.min[key] = 0
		MithralThirdPerson_Config.max[key] = 1
		created_convar = CreateClientConVar( MithralThirdPerson_Config.var_name[key], MithralThirdPerson_Config.default[key], true, false, MithralThirdPerson_Config.con_var_help[key], MithralThirdPerson_Config.min[key], MithralThirdPerson_Config.max[key] )

		--[[
		MithralThirdPerson_Config.current_value_bool[key] = created_convar:GetBool()
		MithralThirdPerson_Config.current_value_fp[key] = created_convar:GetFloat()
		MithralThirdPerson_Config.current_value_int[key] = created_convar:GetInt()
		MithralThirdPerson_Config.current_value_str[key] = created_convar:GetString()

		cvars.RemoveChangeCallback( MithralThirdPerson_Config.var_name[key], "callback." .. key )

		cvars.AddChangeCallback( MithralThirdPerson_Config.var_name[key], function( convar_name, old_val, new_val )
			new_val = util.StringToType( new_val, "int" )
			MithralThirdPerson_Config.current_value_bool[key] = new_val > 0
			MithralThirdPerson_Config.current_value_fp[key] = new_val
			MithralThirdPerson_Config.current_value_int[key] = math.floor( new_val )
			MithralThirdPerson_Config.current_value_str[key] = tostring( new_val )
		end, 
		"callback." .. key )
		--]]
	elseif MithralThirdPerson_Config.var_type[key].is_number then
		MithralThirdPerson_Config.min[key] = min or -8192
		MithralThirdPerson_Config.max[key] = max or 8192

		created_convar = CreateClientConVar( MithralThirdPerson_Config.var_name[key], MithralThirdPerson_Config.default[key], true, false, MithralThirdPerson_Config.con_var_help[key], min, max )

		--[[
		MithralThirdPerson_Config.current_value_bool[key] = created_convar:GetBool()
		MithralThirdPerson_Config.current_value_fp[key] = created_convar:GetFloat()
		MithralThirdPerson_Config.current_value_int[key] = created_convar:GetInt()
		MithralThirdPerson_Config.current_value_str[key] = created_convar:GetString()

		cvars.RemoveChangeCallback( MithralThirdPerson_Config.var_name[key], "callback." .. key )

		cvars.AddChangeCallback( MithralThirdPerson_Config.var_name[key], function( convar_name, old_val, new_val )
			new_val = util.StringToType( new_val, "float" )
			MithralThirdPerson_Config.current_value_bool[key] = new_val > 0
			MithralThirdPerson_Config.current_value_fp[key] = new_val
			MithralThirdPerson_Config.current_value_int[key] = math.floor( new_val )
			MithralThirdPerson_Config.current_value_str[key] = tostring( new_val )
		end, 
		"callback." .. key )
		--]]
	elseif MithralThirdPerson_Config.var_type[key].is_colour then
		created_convar = CreateClientConVar( MithralThirdPerson_Config.var_name[key], MithralThirdPerson_Config.default[key], true, false, MithralThirdPerson_Config.con_var_help[key] )

		MithralThirdPerson_Config.current_value_color[key] = string.ToColor( created_convar:GetString() )
		MithralThirdPerson_Config.current_value_str[key] = string.ToColor( created_convar:GetString() )

		cvars.RemoveChangeCallback( MithralThirdPerson_Config.var_name[key], "callback." .. key )

		cvars.AddChangeCallback( MithralThirdPerson_Config.var_name[key], function( convar_name, old_val, new_val )
			MithralThirdPerson_Config.current_value_color[key] = string.ToColor( new_val )
			MithralThirdPerson_Config.current_value_str[key] = new_val
		end, 
		"callback." .. key )

	else
		created_convar = CreateClientConVar( MithralThirdPerson_Config.var_name[key], MithralThirdPerson_Config.default[key], true, false, MithralThirdPerson_Config.con_var_help[key] )

		MithralThirdPerson_Config.current_value_str[key] = created_convar:GetString()

		cvars.RemoveChangeCallback( MithralThirdPerson_Config.var_name[key], "callback." .. key )

		cvars.AddChangeCallback( MithralThirdPerson_Config.var_name[key], function( convar_name, old_val, new_val )
			MithralThirdPerson_Config.current_value_str[key] = new_val
		end, 
		"callback." .. key )
	end
end

local common_help_text = {}
common_help_text.cam_forward = " Examples: \n   45 units behind the player \n   -42 units in front of the player"
common_help_text.cam_right = " Examples: \n   15 units to the right of the player \n   -16 units to the left of the player"
common_help_text.color = " \"Red Green Blue Alpha\" Examples: \n   \"255 0 255 127\" - Half transparent magenta\n   \"0 255 0 255\" - Fully opague green"
common_help_text.experimental = "Experimental option:\n"

add_console_variable( "ADDON_ENABLED",						"mithral_third_person_addon_enabled",						"1",	"Enables or Disables the addon on the fly, \n   0 - diabled \n   1 - enabled\n   When disabled the addon still has a small effect on the game (about 0.05ms on mine).", { is_bool = true } )
add_console_variable( "EXPERIMENTAL",						"mithral_third_person_experitmental",						"0",	"Experimental features. Don't turn this on.\n   Restart of the script or map is needed.", { is_bool = true } )
add_console_variable( "CAMERA_FORWARD",						"mithral_third_person_camera_forward",						"50",	"How far forward the camera is (inverted)." .. common_help_text.cam_forward, { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_RIGHT",						"mithral_third_person_camera_right",						"20",	"How far to the right the camera is." .. common_help_text.cam_right, { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_UP",							"mithral_third_person_camera_up",							"-10",	"For far up the camera is going to above eye level.", { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_FOV",							"mithral_third_person_camera_fov",							"75",	"The Field of View of the camera, how much the camera can see.", { is_number = true }, 10, 100 )
add_console_variable( "CAMERA_FOV_CHANGE_SPEED",			"mithral_third_person_camera_fov_change_speed",				"1",	"How quickly Field of View changes", { is_number = true }, 0.01, 1000 )
add_console_variable( "CAMERA_AIM_FORWARD",					"mithral_third_person_camera_aim_forward",					"40",	"How far forward the camera is when pressing aim key (inverted)." .. common_help_text.cam_forward, { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_AIM_RIGHT",					"mithral_third_person_camera_aim_right",					"20",	"How far to the right the camera is when pressing aim key." .. common_help_text.cam_right, { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_AIM_UP",						"mithral_third_person_camera_aim_up",						"-10",	"For far up the camera is going to above eye level when pressing aim key.", { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_AIM_FOV",						"mithral_third_person_camera_aim_fov",						"5",	"Camera FOV (Field of View) subtraction when pressing aim key.", { is_number = true }, 0, 75 )
add_console_variable( "CAMERA_AIM_POSITION_CHANGE_SPEED",	"mithral_third_person_camera_aim_position_change_speed",	"1",	"How fast the camera is going to the new position.", { is_number = true }, -1000, 1000 )
add_console_variable( "CAMERA_DISABLE_ROTATION_WHEN_MOVE",	"mithral_third_person_camera_disable_rotation_when_move",	"1",	"Disables or Enables player rotation when moving. \n   0 - Player rotates with the camera. \n   1 - Player sticks with the last input angle", { is_bool = true } )
add_console_variable( "PLAYER_ROTATION_SPEED",				"mithral_third_person_player_rotation_speed",				"5",	"How quickly player rotates.", { is_number = true }, 0.1, 1000 )
add_console_variable( "ACCURATE_AIM",						"mithral_third_person_disable_accurate_aim",				"0",	"Aim to the camera centre" , { is_bool = true })
add_console_variable( "PLAYER_AIMING_BUTTON",				"mithral_third_person_player_aiming_button",				"108",	"Which button is the aming key. \n WARNING: don't touch this if you have no idea what it's doing.\n	Reference: https://wiki.facepunch.com/gmod/Enums/KEY" )
add_console_variable( "CROSSHAIR_HIDDEN_IF_NOT_AIMING",		"mithral_third_person_crosshair_hidden_if_not_aiming",		"0",	"Hides the crosshair when aiming button isn't pressed.", { is_bool = true } )
--add_console_variable( "CROSSHAIR_TRACE_POSITION",			"mithral_third_person_crosshair_trace_position",			"0",	"OBSOLETE\nShows where is player is aiming.", { is_bool = true } )
add_console_variable( "AIM_ON_ATTACK",						"mithral_third_person_aim_on_attack",						"1",	"When pressing most action keys (shooting, interaction, etc.) \n   Aim to what camera angle or when '" .. MithralThirdPerson_Config.var_name.ACCURATE_AIM .. "' is set to 1, the centre.", { is_bool = true } )
add_console_variable( "AIMING_TIMER",						"mithral_third_person_aiming_timer",						"1",	"For how long the player keeps on aiming after shooting.", { is_number = true }, -1000, 1000 )
add_console_variable( "ALWAYS_AIM",							"mithral_third_person_always_aim",							"0",	"Keeps the player aiming on the centre of the screen", { is_bool = true } )
add_console_variable( "TRACE_CROSSHAIR",					"mithral_third_person_crosshair_traced",					"0",	"Draw traced (where player is aiming) crosshair", { is_bool = true } )
add_console_variable( "STATIC_CROSSHAIR",					"mithral_third_person_crosshair_static",					"0",	"Draw static crosshair", { is_bool = true } )
add_console_variable( "TRACE_CROSSHAIR_COLOUR",				"mithral_third_person_crosshair_traced_color",				"255 0 255 255","Colour of traced crosshair." .. common_help_text.color, { is_colour = true } )
add_console_variable( "STATIC_CROSSHAIR_COLOUR",			"mithral_third_person_crosshair_static_color",				"0 255 0 255",	"Colour of static crosshair." .. common_help_text.color, { is_colour = true } )
add_console_variable( "FIRSTPERSON_FRACTION",				"mithral_third_person_firstperson_fraction",				"250",	"Now close the third person camera needs to be before triggering first person mode.\nNote: this doesn't use Hammer Units, mess with the value untill it works for you.", { is_number = true } )
add_console_variable( "EFFECT_FAKEDOF",						"mithral_third_person_effect_fakedof",						"0",	"Fake depth of field effect when aiming.\nQuite buggy right now, might not work in the main branch of gmod.", { is_bool = true } )
add_console_variable( "FORCED_FIRSTPERSON_CONTEXT_MENU",	"mithral_third_person_forced_firstperson_context_menu",		"1",	"Switch to first person when opening context menu.", { is_bool = true } )
add_console_variable( "BEHAVIOUR_RULES",					"mithral_third_person_rules_file",							"mithral_third_person_rules.json",	"behaviour rules file (without file extention)" )
add_console_variable( "CAMERA_FALLING_OFFSET",				"mithral_third_person_falling_offset",						"5",	"How much to offest the camera up position when falling.", { is_number = true }, -1000, 1000 )
add_console_variable( "PRINT_BEHAVIOUR",					"mithral_third_person_behaviour_print",						"0",	"Prints out generated behaviour code at the end of it's construction \n   Note: This prints out no matter if the generated code is runnable or not.", { is_bool = true } )
add_console_variable( "CAMERA_ORIGIN_ON_HEAD",				"mithral_third_person_camera_follows_the_head",				"0",	"The camera follows the head of player instead of point based on the players position.", { is_bool = true } )
add_console_variable( "BEHAVIOUR_MAX_EFFECTS",				"mithral_third_person_behaviour_max_effects",				"10",	"Maximum number effects that are allowed to be placed in a single tick.", { is_number = true }, 0 )
add_console_variable( "BEHAVIOUR_MAX_CONDITIONS",			"mithral_third_person_behaviour_max_conditions",			"10",	"Maximum number conditions that are allowed to be placed in a single tick.", { is_number = true }, 0 )
add_console_variable( "BEHAVIOUR_MAX_WEAPONS",				"mithral_third_person_behaviour_max_weapons",				"100",	"Maximum number weapons that are allowed to be placed in a single tick.", { is_number = true }, 0 )
add_console_variable( "BEHAVIOUR_MAX_RULES",				"mithral_third_person_behaviour_max_rules",					"300",	"Maximum number rules that are allowed to be placed in a single tick.", { is_number = true }, 0 )
add_console_variable( "AIMING_BUTTON_TOGGLE",				"mithral_third_person_player_aiming_button_toggle",			"0",	"Aiming button acting as a toggle instead of held.", { is_bool = true } )
add_console_variable( "WEAPON_PASSIVE_STANCE",				"mithral_third_person_player_passive_stance",				"1",	"Sets whitelisted weapon stances to passive.", { is_bool = true } )
--add_console_variable( "SIMPLER_CAM_TRACE_FILTER",			"mithral_third_person_simple_camera_trace_filter",			"0",	"", { is_bool = true } )

--add_console_variable( "PLAYERMODEL_LIES",					"mithral_third_person_player_model_fake_angle_",			"1",	common_help_text.experimental .. "Changes render angles for your player model, doesn't change how you look like to others.")
--add_console_variable( "FIRSTPERSON_CHECK_FOR_PLAYER_HIT",	"mithral_third_person_firstperson_check_for_player_hitbox",	"1" )
add_console_variable( "CAPTURE_EXTERNAL_INPUT_MODIFIERS",	"mithral_third_person_capture_external_input_modfiers",		"0",	common_help_text.experimental .. "captures external camera angle modifications", { is_bool = true } )

MithralThirdPerson_COM_EDITOR_OPEN = "mithral_third_person_addon_open_editor"
MithralThirdPerson_COM_BEHAVIOUR_REFRESH = "mithral_third_person_behaviour_refresh"
MithralThirdPerson_COM_SET_CAMETA_ANGLE = "mithral_third_person_set_camera_angle"
MithralThirdPerson_COM_INVERT_CAMERA_RIGHT = "mithral_third_person_invert_camera_right"
MithralThirdPerson_COM_INVERT_CAMERA_AIM_RIGHT = "mithral_third_person_invert_camera_aim_right"
MithralThirdPerson_COM_INVERT_CAMERA_RIGHT_COMBINED = "mithral_third_person_invert_camera_right_combined"
MithralThirdPerson_COM_TOGGLE_ADDON = "mithral_third_person_toggle_addon"
