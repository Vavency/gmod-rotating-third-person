local MithralThirdPerson_debug = {}

MithralThirdPerson_DEBUG_CURRENT_INSTANCE = MithralThirdPerson_DEBUG_CURRENT_INSTANCE or 0

MithralThirdPerson_DEBUG_CURRENT_INSTANCE = MithralThirdPerson_DEBUG_CURRENT_INSTANCE + 1

MithralThirdPerson_DEBUG_DRAW = MithralThirdPerson_DEBUG_DRAW or {}

MithralThirdPerson_DEBUG_DRAW.bench = MithralThirdPerson_DEBUG_DRAW.bench or {
	measure = function()end,
	sample_rate = 6,
	tablebench = {},
	smooth = {
		average = {},
		max = {}
	},
	accumulator = {
		average = {},
		max = {}
	}
}

local debugDraw = {}
debugDraw.debug_build = true
debugDraw.xcord = 0.010
debugDraw.ycords_start = 0.08
debugDraw.stayoncreen = 0.05
debugDraw.textcolour = {
	Color(255, 255, 255),
	Color(0, 255, 0),
	Color(255, 127, 255),
	Color(255, 127, 0),
	Color(123, 125, 250),
	Color(93, 237, 197),
}
debugDraw.textStore = {}
debugDraw.instance_number = MithralThirdPerson_DEBUG_CURRENT_INSTANCE
debugDraw.current_frame = 0
debugDraw.enumeration_to_current_tick = 0
debugDraw.enumeration_to_string_runs = 0
debugDraw.CONTENT_to_string_table = {
	"SOLID",
	"WINDOW",
	"AUX",
	"GRATE",
	"SLIME",
	"WATER",
	"BLOCKLOS",
	"OPAQUE",
	"TESTFOGVOLUME	",
	"TEAM4",
	"TEAM3",
	"TEAM1",
	"TEAM2",
	"IGNORE_NODRAW_OPAQUE",
	"MOVEABLE",
	"AREAPORTAL",
	"PLAYERCLIP",
	"MONSTERCLIP",
	"CURRENT_0",
	"CURRENT_180",
	"CURRENT_270",
	"CURRENT_90",
	"CURRENT_DOWN",
	"CURRENT_UP",
	"DEBRIS",
	"DETAIL",
	"HITBOX",
	"LADDER",
	"MONSTER",
	"ORIGIN",
	"TRANSLUCENT"
}
debugDraw.KeyEnum_string_table = {
	"IN_ATTACK",
	"IN_JUMP",
	"IN_DUCK",
	"IN_FORWARD",
	"IN_BACK",
	"IN_USE",
	"IN_CANCEL",
	"IN_LEFT",
	"IN_RIGHT",
	"IN_MOVELEFT",
	"IN_MOVERIGHT",
	"IN_ATTACK2",
	"IN_RUN",
	"IN_RELOAD",
	"IN_ALT1",
	"IN_ALT2",
	"IN_SCORE",
	"IN_SPEED",
	"IN_WALK",
	"IN_ZOOM",
	"IN_WEAPON1",
	"IN_WEAPON2",
	"IN_BULLRUSH",
	"IN_GRENADE1",
	"IN_GRENADE2"
}

MithralThirdPerson_DEBUG_DRAW.ycords = debugDraw.ycords_start
MithralThirdPerson_DEBUG_DRAW.current_frame = 0

function MithralThirdPerson_debug.GetKeyEnumStringTable()
	return debugDraw.KeyEnum_string_table
end

function MithralThirdPerson_debug.GetContentStringTable()
	return debugDraw.CONTENT_to_string_table
end

function MithralThirdPerson_debug.IsDebugEnabled()
	return debugDraw.debug_build
end

function MithralThirdPerson_debug.GetColour( int )
	return debugDraw.textcolour[int]
end


function MithralThirdPerson_debug.draw_text ( text, color_num, y_position ) -- I got sick of doing it the manual way.

	if MithralThirdPerson_DEBUG_DRAW.current_frame ~= FrameNumber() then
		debugDraw.textStore = {}
		MithralThirdPerson_DEBUG_DRAW.ycords = debugDraw.ycords_start
		debugDraw.stayoncreen = 0
		MithralThirdPerson_DEBUG_DRAW.current_frame = FrameNumber()
		debugDraw.current_frame = FrameNumber()
	end


	local temp_y_position

	-- Used as array counter in order to reference color
	color_num = color_num or 1

	-- Used to put ScreenText on a custom height
	temp_y_position = y_position or MithralThirdPerson_DEBUG_DRAW.ycords

	 -- ( 'debugDraw.xcord' width offset on screen, 'temp' height offset on screen , 'arg1' Text to display, 'debugDraw.stayoncreen' how long to stay on screen, 'debugDraw.textcolour[arg2]' is a Color from an array )
	debugoverlay.ScreenText( debugDraw.xcord, temp_y_position, text, debugDraw.stayoncreen, debugDraw.textcolour[color_num] )

	table.insert( debugDraw.textStore, { text = text, colour = color_num} )

	-- If we use custom height then don't increment the auto height
	if y_position == nil then
		MithralThirdPerson_DEBUG_DRAW.ycords = MithralThirdPerson_DEBUG_DRAW.ycords + 0.01
	end

end

-- EXTRA_TAGS.headerColorTag , EXTRA_TAGS.DoAsReturn ,  EXTRA_TAGS.CountRuns , EXTRA_TAGS.append_string
function MithralThirdPerson_debug.enum_to_name ( enumeration, string_table, EXTRA_TAGS )

	local added_string

	if EXTRA_TAGS == nil then
		EXTRA_TAGS = {}
		EXTRA_TAGS.headerColorTag = 2
		EXTRA_TAGS.append_string = ""
		EXTRA_TAGS.CountRuns = false
	end

	EXTRA_TAGS.headerColorTag = EXTRA_TAGS.headerColorTag or 2

	added_string = EXTRA_TAGS.append_string or ""

	if EXTRA_TAGS.CountRuns then
		debugDraw.enumeration_to_string_runs = debugDraw.enumeration_to_string_runs + 1
		added_string = added_string .. " Run: " .. debugDraw.enumeration_to_string_runs
	end

	if ( not ( debugDraw.enumeration_to_current_tick == engine.TickCount() ) ) then
		debugDraw.enumeration_to_string_runs = 0
		debugDraw.enumeration_to_current_tick = engine.TickCount()
	end

	local CONTENT_bin = string.reverse(math.IntToBin(enumeration))
	local CONTENT_string = ""


	local test_num = 1
	--local test_mask = 1073741825

	for i=1, string.len(CONTENT_bin) do
		if tobool( bit.band( test_num, enumeration ) ) then
			CONTENT_string = CONTENT_string .. " "  .. string_table[i] .. ":" .. i
		end
		--print( bit.band( test_num, test_mask ) )
		test_num = bit.lshift( test_num, 1 )
	end

	--[[
	for i=1, string.len(CONTENT_bin) do
		if tobool(CONTENT_bin[i]) then
			CONTENT_string = CONTENT_string .. " "  .. string_table[i] .. ":" .. i
		end
	end
	--]]

	if not ( EXTRA_TAGS == nil ) then
		if EXTRA_TAGS.DoAsReturn then
			return CONTENT_string .. added_string
		end
	end
	MithralThirdPerson_debug.draw_text( "RTP.enum_to_name: enumeration: " .. tostring( string.reverse( CONTENT_bin ) ) .. " " .. added_string , EXTRA_TAGS.headerColorTag )
	MithralThirdPerson_debug.draw_text( "	string:" .. CONTENT_string )

end

function MithralThirdPerson_debug.draw_line ( vec1, vec2, colour, timeout, ignorez )
	vec1 = vec1 or Vector(0, 0, 0)
	vec2 = vec2 or Vector(0, 0, 0)
	colour = colour or 2
	ignorez = ignorez or false

	debugoverlay.Line( vec1, vec2, timeout, debugDraw.textcolour[colour], ignorez )
end

function MithralThirdPerson_debug.bench_measure( key_string, text_colour, include_in_total, should_log_in_console, log_every_frame )
	local time = SysTime()

	if MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string] == nil then

		local caller_info = debug.getinfo( 2 )

		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string] = {
			linedefined = caller_info.linedefined,
			lastlinedefined = caller_info.lastlinedefined,
			source = caller_info.source,
			current_frame = 0,
			name = caller_info.source .. ":" .. key_string,
			time1 = 0,
			time2 = 0,
			run_count = 0,
			with_namer = true,
			text_colour = text_colour or 1,
			include_in_total = include_in_total or true,
			should_log_in_console = should_log_in_console or false,
			log_every_frame = log_every_frame or false,
			draw_string = "",
		}
	end

	if MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].current_frame ~= FrameNumber() then
		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].current_frame = FrameNumber()
		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].run_count = 0
		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time1 = 0
		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time2 = 0
	end

	if MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].run_count % 2 == 0 then
		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time1 = MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time1 + ( time - MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time1 )
	else
		MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time2 = MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time2 + ( time - MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].time2 )
	end

	--PrintTable( MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string] )
	--print( "------" )
	MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].run_count = MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key_string].run_count + 1
end

function MithralThirdPerson_debug.bench_draw_overlay()
	if MithralThirdPerson_debug.IsDebugEnabled() and GetConVar( "developer" ):GetBool() then

		local margin = 0.14
		local totals = 0
		local total_max = 0
		local trunc = 5
		--local string_trunc = 5
		local sample_rate = MithralThirdPerson_DEBUG_DRAW.bench.sample_rate
		local itterator = 0
		local updateTime = false
		local string_average = ""
		local string_max = ""
		local draw_table = {}

		MithralThirdPerson_debug.draw_text( "benchie the fiendly script measurer():", 4, 1 - ( margin + table.Count( MithralThirdPerson_DEBUG_DRAW.bench.tablebench ) * 0.01 ) + 0.01 )

		-- Nice that lua tables start at 1
		MithralThirdPerson_DEBUG_DRAW.bench.smooth.nextUpdate = MithralThirdPerson_DEBUG_DRAW.bench.smooth.nextUpdate or 0

		if MithralThirdPerson_DEBUG_DRAW.bench.smooth.nextUpdate < FrameNumber() then
			MithralThirdPerson_DEBUG_DRAW.bench.sample_rate = math.Truncate( 1 / RealFrameTime() )
			if MithralThirdPerson_DEBUG_DRAW.bench.sample_rate > 1000 then
				MithralThirdPerson_DEBUG_DRAW.bench.sample_rate = 400
			end
			MithralThirdPerson_DEBUG_DRAW.bench.smooth.nextUpdate =  FrameNumber() + MithralThirdPerson_DEBUG_DRAW.bench.sample_rate
			updateTime = true
		end

		for key, value in pairs( MithralThirdPerson_DEBUG_DRAW.bench.tablebench ) do

			itterator = itterator + 1

			MithralThirdPerson_DEBUG_DRAW.bench.smooth.average[key] = MithralThirdPerson_DEBUG_DRAW.bench.smooth.average[key] or 0
			MithralThirdPerson_DEBUG_DRAW.bench.accumulator.average[key] = MithralThirdPerson_DEBUG_DRAW.bench.accumulator.average[key] or 0
			MithralThirdPerson_DEBUG_DRAW.bench.accumulator.max[key] = MithralThirdPerson_DEBUG_DRAW.bench.accumulator.max[key] or 0
			MithralThirdPerson_DEBUG_DRAW.bench.smooth.max[key] = MithralThirdPerson_DEBUG_DRAW.bench.smooth.max[key] or 0

			MithralThirdPerson_DEBUG_DRAW.bench.accumulator.average[key] = MithralThirdPerson_DEBUG_DRAW.bench.accumulator.average[key] + ( value.time2 - value.time1 )

			local run_count = tostring( value.run_count / 2 )

			for i=1, 2 do
				if run_count[i] == "" then
					run_count = run_count .. " "
				end
			end


			if MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].log_every_frame then
				MsgC( MithralThirdPerson_debug.GetColour( MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].text_colour ), (value.time2 - value.time1) * 1000 .. "ms R:" .. run_count .. " " .. value.name .. "\n" )
			end

			if updateTime then
				MithralThirdPerson_DEBUG_DRAW.bench.smooth.average[key] = MithralThirdPerson_DEBUG_DRAW.bench.accumulator.average[key] / sample_rate
				MithralThirdPerson_DEBUG_DRAW.bench.smooth.max[key] = MithralThirdPerson_DEBUG_DRAW.bench.accumulator.max[key]
				MithralThirdPerson_DEBUG_DRAW.bench.accumulator.average[key] = 0
				MithralThirdPerson_DEBUG_DRAW.bench.accumulator.max[key] = 0
				value.time1 = 0
				value.time2 = 0
			end

			if MithralThirdPerson_DEBUG_DRAW.bench.accumulator.max[key] < ( value.time2 - value.time1 ) then
				MithralThirdPerson_DEBUG_DRAW.bench.accumulator.max[key] = ( value.time2 - value.time1  )
			end


			local avg_ms = tostring( math.Truncate( MithralThirdPerson_DEBUG_DRAW.bench.smooth.average[key] * 1000, trunc ) )
			local max_ms = tostring( math.Truncate( MithralThirdPerson_DEBUG_DRAW.bench.smooth.max[key] * 1000, trunc ) )

			for i=1, trunc do
				if max_ms[i+2] == "" then
					max_ms = max_ms .. "0"
				end
				if avg_ms[i+2] == "" then
					avg_ms = avg_ms .. "0"
				end
			end

			if updateTime then
				MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].draw_string = " avg: " .. avg_ms .. "ms max: " .. max_ms .. "ms R:" .. run_count .. " " .. value.name

				if MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].should_log_in_console then
					MsgC( MithralThirdPerson_debug.GetColour( MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].text_colour ), MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].draw_string .. "\n" )
				end
			end

			table.insert( draw_table, {
				string = MithralThirdPerson_DEBUG_DRAW.bench.tablebench[key].draw_string,
				t_avg = MithralThirdPerson_DEBUG_DRAW.bench.smooth.average[key],
				t_max = MithralThirdPerson_DEBUG_DRAW.bench.smooth.max[key],
				c_index = value.text_colour
			} )


			if value.include_in_total then
				totals = totals + MithralThirdPerson_DEBUG_DRAW.bench.smooth.average[key]
				total_max = total_max + MithralThirdPerson_DEBUG_DRAW.bench.smooth.max[key]
			end

		end

		--PrintTable( draw_table )
		table.SortByMember( draw_table, "t_avg", false )
		--PrintTable( draw_table )

		for i=1, table.Count( draw_table ) do
			MithralThirdPerson_debug.draw_text( draw_table[i].string, draw_table[i].c_index, 1 - ( margin + ( table.Count( draw_table ) - i ) * 0.01 ) + 0.01 )
		end

		local total_time = tostring( math.Truncate( ( totals ) * 1000, trunc ) )
		local total_time_max = tostring( math.Truncate( ( total_max ) * 1000, trunc ) )

		for i=1, trunc do
			if total_time[i+2] == "" then
				total_time = total_time .. "0"
			end
			if total_time_max[i+2] == "" then
				total_time_max = total_time_max .. "0"
			end
		end

		MithralThirdPerson_debug.draw_text( " avg: " .. total_time .. "ms max: " .. total_time_max .. "ms UPS: " .. sample_rate, 3, 1 - margin + 0.02 )



	end
end



if debugDraw.debug_build then

	local function instance_text_dump()
		print( "Dumping text of instance " .. debugDraw.instance_number .. ":" )

		for i, value in pairs( debugDraw.textStore ) do
			MsgC( debugDraw.textcolour[value.colour], tostring( value.text .. "\n" ) )
		end
		if debugDraw.current_frame == 0 then
			MsgC( debugDraw.textcolour[1], tostring( "No calls were made for this instance.\n" ) )
		else
			MsgC( debugDraw.textcolour[1], tostring( "Last run on frame: " .. debugDraw.current_frame .. "\n" ) )
		end
	end

	local function total_instance_text_dump()
		--debugDraw.printout = true
		for i=1, debugDraw.instance_number do
			RunConsoleCommand( "debug_mtp_dump_instance_text_" .. i )
		end
	end

	concommand.Remove( "debug_mtp_dump_instance_text_" .. debugDraw.instance_number )
	concommand.Add( "debug_mtp_dump_instance_text_" .. debugDraw.instance_number , function () instance_text_dump() end )

	concommand.Remove( "debug_mtp_dump_text" )
	concommand.Add( "debug_mtp_dump_text", function () total_instance_text_dump() end )
else
	-- Stub out everything since it's not needed.
	for key, value in pairs( MithralThirdPerson_debug ) do
		if isfunction( value ) then
			debugDraw[key] = function()end
		end
	end

	function MithralThirdPerson_debug.IsDebugEnabled()
		return debugDraw.debug_build
	end
end

return MithralThirdPerson_debug