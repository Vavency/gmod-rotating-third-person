local RTPCMD = {}

local AddKeyDelay_t = {
	KeyDownTable = {false},
	FrameCheck = {0},
	QueueFrame = {0},
	RemoveFrame = {0},
	TickCheck = {0},
	QueueTick = {0},
	RemoveTick = {0},
}

for i=1, 30, 1 do
	AddKeyDelay_t.KeyDownTable[ bit.lshift( 1, i ) ] = false
	AddKeyDelay_t.FrameCheck[ bit.lshift( 1, i ) ] = 0
	AddKeyDelay_t.QueueFrame[ bit.lshift( 1, i ) ] = 0
	AddKeyDelay_t.RemoveFrame[ bit.lshift( 1, i ) ] = 0
	AddKeyDelay_t.TickCheck[ bit.lshift( 1, i ) ] = 0
	AddKeyDelay_t.QueueTick[ bit.lshift( 1, i ) ] = 0
	AddKeyDelay_t.RemoveTick[ bit.lshift( 1, i ) ] = 0
end

-- Debug
local MithralThirdPerson_debug = include( "includes/mithtal_third_person_debug.lua" )

function RTPCMD.AddKeyDelayFrame( enum, ucmd )
	--[[
	MithralThirdPerson_debug.draw_text( "AddKeyDelay: enum: " .. tostring( MithralThirdPerson_debug.enum_to_name( enum, MithralThirdPerson_debug.GetKeyEnumStringTable(), { DoAsReturn = true } ) ), 2)
	MithralThirdPerson_debug.draw_text( "	KeyDown: " .. tostring( ucmd:KeyDown( enum ) ))
	MithralThirdPerson_debug.enum_to_name( ucmd:GetButtons(), MithralThirdPerson_debug.GetKeyEnumStringTable(), { headerColorTag = 4 } )
	--]]

	-- I theory theres a bug it's sister function had, in practice I couldn't trigger it. Maybe in some specific situations but I am not losing sleep over this.

	if AddKeyDelay_t.RemoveFrame[ enum ] == FrameNumber() then
		AddKeyDelay_t.QueueFrame[ enum ] = 0
		AddKeyDelay_t.QueueTick[ enum ] = 0
		AddKeyDelay_t.RemoveFrame[ enum ] = 0
		AddKeyDelay_t.FrameCheck[ enum ] = 0
		AddKeyDelay_t.TickCheck[ enum ] = 0
		AddKeyDelay_t.RemoveTick[ enum ] = 0
		return
	end

	if  AddKeyDelay_t.FrameCheck[ enum ] == FrameNumber() then
		ucmd:RemoveKey( enum )
		return
	end

	if ucmd:KeyDown( enum ) then
		
		if not AddKeyDelay_t.KeyDownTable[ enum ] then
			AddKeyDelay_t.FrameCheck[ enum ] = FrameNumber()
			AddKeyDelay_t.TickCheck[ enum ] = engine.TickCount()
			ucmd:RemoveKey( enum )
		end
		AddKeyDelay_t.QueueFrame[ enum ] = FrameNumber() + 1
		AddKeyDelay_t.QueueTick[ enum ] = engine.TickCount() + 1
		AddKeyDelay_t.KeyDownTable[ enum ] = true
		return
	else
		AddKeyDelay_t.KeyDownTable[ enum ] = false
	end

	if AddKeyDelay_t.QueueFrame[ enum ] == FrameNumber() then
		AddKeyDelay_t.RemoveFrame[ enum ] = FrameNumber() + 1
		AddKeyDelay_t.RemoveTick[ enum ] = engine.TickCount() + 1
		RTPCMD.AddKeysToCMD( enum, ucmd )
	end

	--[[
	MithralThirdPerson_debug.draw_text( "	KeyDown: " .. tostring( ucmd:KeyDown( enum ) ), 3)
	MithralThirdPerson_debug.draw_text( "" )
	--]]

end


function RTPCMD.AddKeyDelayTick( enum, ucmd )
	--[[
	MithralThirdPerson_debug.draw_text( "AddKeyDelay: enum: " .. tostring( MithralThirdPerson_debug.enum_to_name( enum, MithralThirdPerson_debug.GetKeyEnumStringTable(), { DoAsReturn = true } ) ), 2)
	MithralThirdPerson_debug.draw_text( "	KeyDown: " .. tostring( ucmd:KeyDown( enum ) ))
	--MithralThirdPerson_debug.enum_to_name( ucmd:GetButtons(), MithralThirdPerson_debug.GetKeyEnumStringTable(), { headerColorTag = 4 } )
	--]]

	--DebugInfo( 1, engine.TickCount() )

	if AddKeyDelay_t.RemoveTick[ enum ] == engine.TickCount() then
		AddKeyDelay_t.QueueFrame[ enum ] = 0
		AddKeyDelay_t.QueueTick[ enum ] = 0
		AddKeyDelay_t.RemoveFrame[ enum ] = 0
		AddKeyDelay_t.FrameCheck[ enum ] = 0
		AddKeyDelay_t.TickCheck[ enum ] = 0
		AddKeyDelay_t.RemoveTick[ enum ] = 0
		return
	end

	if AddKeyDelay_t.QueueTick[ enum ] == engine.TickCount() then
		AddKeyDelay_t.RemoveFrame[ enum ] = FrameNumber() + 1
		AddKeyDelay_t.RemoveTick[ enum ] = engine.TickCount() + 1
		if not ucmd:KeyDown( enum ) and AddKeyDelay_t.KeyDownTable[ enum ] then
			RTPCMD.AddKeysToCMD( enum, ucmd )
		end
		return
	end

	if  AddKeyDelay_t.TickCheck[ enum ] == engine.TickCount() then
		ucmd:RemoveKey( enum )
		return
	end

	if ucmd:KeyDown( enum ) then
		
		if not AddKeyDelay_t.KeyDownTable[ enum ] then
			AddKeyDelay_t.FrameCheck[ enum ] = FrameNumber()
			AddKeyDelay_t.TickCheck[ enum ] = engine.TickCount()
			ucmd:RemoveKey( enum )
		end
		AddKeyDelay_t.QueueFrame[ enum ] = FrameNumber() + 1
		AddKeyDelay_t.QueueTick[ enum ] = engine.TickCount() + 1
		AddKeyDelay_t.KeyDownTable[ enum ] = true
	else
		AddKeyDelay_t.KeyDownTable[ enum ] = false
	end

	--[[
	MithralThirdPerson_debug.draw_text( "	KeyDown: " .. tostring( ucmd:KeyDown( enum ) ), 3)
	MithralThirdPerson_debug.draw_text( "" )
	--]]

end


function RTPCMD.GetDelayedKey( enum )
	return AddKeyDelay_t.KeyDownTable[ enum ]
end


function RTPCMD.AddKeysToCMD ( enum, ucmd )
	ucmd:SetButtons( bit.bor( enum, ucmd:GetButtons() ) )
end

return RTPCMD