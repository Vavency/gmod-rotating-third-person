# Mithral Third Person Rotating Camera
This is reproduction of the camera control from the Haydee game for Garry's Mod.

[Original addon Steam page](https://steamcommunity.com/sharedfiles/filedetails/?id=1620191091)

# Developer setup (Linux only)
Everything here is done with the terminal. If you feel that GUI file manager would server you better be my guest.

Navigate to Garry's Mod `addons` directory. If you changed the Garry's Mod install directory then locate it yourself.

```
cd ~/.local/share/Steam/steamapps/common/GarrysMod/garrysmod/addons/
```

Then clone the repository:

```
git clone --recursive https://gitlab.com/Vavency/gmod-rotating-third-person.git
```
After (re)start Garry's mod.

# Packaging the addon (Linux only)

Go to `dist` directory and run `package.sh`.

If the script fails with `Couldn't find 'gmad'` you're using non-default install location for Garry's mod then you'll need to locate it yourself.

```
path/to/GarrysMod/bin/linux64/gmad create -folder "gmad_pack" -out "rotaiting_third_person.gma"
```