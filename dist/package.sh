#!/bin/bash

mkdir -p "gmad_pack/lua/autorun"
mkdir -p "gmad_pack/lua/includes"
mkdir -p "gmad_pack/resource/localization/en"
mkdir -p "gmad_pack/lua/includes/modules"
mkdir -p "gmad_pack/materials/icon32"
mkdir -p "gmad_pack/materials/icon64"
mkdir -p "gmad_pack/materials/icon128"
mkdir -p "gmad_pack/materials/icon256"
mkdir -p "gmad_pack/materials/icon512"
mkdir -p "gmad_pack/materials/icon1024"
cp -rl "../materials/icon32" "gmad_pack/materials/"
cp -rl "../materials/icon64" "gmad_pack/materials/"
cp -rl "../materials/icon128" "gmad_pack/materials/"
cp -rl "../materials/icon256" "gmad_pack/materials/"
cp -rl "../materials/icon512" "gmad_pack/materials/"
cp -rl "../materials/icon1024" "gmad_pack/materials/"
cp -rl "../materials/icon" "gmad_pack/materials/"
cp -rl "../resource/localization/en" "gmad_pack/resource/localization/"
ln "../addon.json" "gmad_pack/addon.json"

cp "../lua/autorun/mithtal_third_person.lua" "gmad_pack/lua/autorun/mithral_third_person.lua"
cp "../lua/includes/mithral_third_person_config.lua" "gmad_pack/lua/includes/mithral_third_person_config.lua"

cp "../lua/includes/mithtal_third_person_editor.lua" "gmad_pack/lua/includes/mithtal_third_person_editor.lua"
#cp "../lua/includes/mithtal_third_person_revision.lua" "gmad_pack/lua/includes/mithtal_third_person_revision.lua"
cp "../lua/includes/mithtal_third_person_debug.lua" "gmad_pack/lua/includes/mithtal_third_person_debug.lua"
cp "../lua/includes/mithtal_third_person_createmove.lua" "gmad_pack/lua/includes/mithtal_third_person_createmove.lua"
cp "../lua/includes/mithtal_third_person_renderer.lua" "gmad_pack/lua/includes/mithtal_third_person_renderer.lua"
cp "../lua/includes/mithtal_third_person_think.lua" "gmad_pack/lua/includes/mithtal_third_person_think.lua"


echo return\ \'Revision\ $(git log | grep commit' ' | grep -v "This reverts" | wc -l )\ $(git describe --abbrev=0)\' > gmad_pack/lua/includes/mithtal_third_person_revision.lua
sed -i -e 's/debugDraw.debug_build = true/debugDraw.debug_build = false/g' "gmad_pack/lua/includes/mithtal_third_person_debug.lua"

if [ -f "mithral_third_person.gma" ]
then
	echo "Found 'mithral_third_person.gma' removing."
	rm "mithral_third_person.gma"
fi

if hash $HOME/.local/share/Steam/steamapps/common/GarrysMod/bin/linux64/gmad
then
	$HOME/.local/share/Steam/steamapps/common/GarrysMod/bin/linux64/gmad create -folder  "gmad_pack" -out "mithral_third_person.gma"
else
	echo "Couldn't find 'gmad'"
fi
